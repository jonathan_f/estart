/**
 * telus.ui.collapsible.js
 * collpasible panel
 *
 *	wraps content in a collapsible panel
 *
 *	options:
 *		direction - "down" (default), "up", "left", "right"
 *		title - any string
 *		headerClass - user-specified class for header
 *		styleClass - user-specified class to wrap content in
 *		wrapperStyle - direct css style for the wrapper (not style class)
 *		idPrefix - 
 *
 *	example:  
 *		$("#content_to_be_wrapped").collapsible({title:"My Title", direction: "left"});
	
 */
	
	var panelId = 0;
	
	function getNextPanelId() {
		return ++panelId;
	}
	

	$.widget("telus.collapsible", {		
		// default settings / options
		options: {			
			direction: "down",
			title: "",
			headerClass: "",
			styleClass: " ",	// user-specified class to wrap body in
			wrapperStyle: " "
			// ,idPrefix: ""	
			// callbacks
		},
		
		/* constructor */
		_create: function() {
			
			this._width = this.element.width();
			this._height = this.element.height();
			
			// alias
			var o = this.options;
			var isVertical = o.direction === "down" || o.direction === "up";
			
			
			// create container
			this.element.wrap('<div id="panel' + getNextPanelId() + '" class="widget-wrapper '+ o.styleClass +'" style="'+ o.wrapperStyle +'">');
			
			// wrap body			
			this.element.addClass("widget-content");
			this.element.addClass(o.styleClass);
			
			// create header			
			this.header = $("<div>", {"class": "widget-header " + o.styleClass});
			this.icon = $("<div>", {"class": "ui-icon ui-icon-triangle-1-n", "style": "float:right"});
					
			
			// position the header and icon
			if (o.direction === "right") {
				this.icon.css("float", "left");
				this.icon.removeClass("ui-icon-triangle-1-n").addClass("ui-icon-triangle-1-e");	// set icon 
				this.header.css("float", "right");
				this.element.css("float", "right");
			}			
			else if (o.direction === "left") {
				this.icon.removeClass("ui-icon-triangle-1-n").addClass("ui-icon-triangle-1-w");	// set icon 
			}
			
			
			// add title
			// set header width
		
			this.header.html("<div style='float:left'>" + o.title + "</div>").append(this.icon);
	
			
			/*
			// rotate text for vertical header
			if (!isVertical) {
				
				// handle bar
				this.header.addClass("handle");
				//this.header.height(this.element.height());
				this.header.css("marginTop", (this.element.height() / 2 - 25));
				
				// this.header.addClass("verticalText");	// no title for horizontally collapsible panel
			}*/
			
			
			// insert header before content
			this.element.before(this.header);
			//this.header.width(this.element.outerWidth() - 4 - this.element.css("border-left-width").replace(/[^-\d\.]/g, '') - this.element.css("border-right-width").replace(/[^-\d\.]/g, ''));
			this.header.width(this.element.outerWidth() - 6);

		
			var that = this;	// to allow access of "this" inside the function
			this.header.bind("click.collapsible", function() {
				
				/* hide/show the content */
				
				// blind effect for vertical show/hide
				if (isVertical) {					
					that.element.toggleClass("hidden");
					that.element.animate({height: 'toggle'});
					
				} else {
					// simply hide/show element for horizontal ones because 
					// sliding might flicker the screen 
					that.element.toggle();
				}
				
				
				/* header animation */
				
				if (!isVertical) {
					(that.header.width() != that.icon.width() + 2)? 
							that.header.animate({'width': that.icon.width() + 2}) : 
							that.header.animate({'width': that._width});
					/*// move handle to right
					that.header.animate({'marginLeft': parseInt(that.header.css('marginLeft'), 10) == 0? 
	        				that.element.parent().outerWidth()-that.header.outerWidth(): 0});	*/
				}

				
				/* change icon */
				if (isVertical)  {
					that.icon.toggleClass("ui-icon-triangle-1-n");
					that.icon.toggleClass("ui-icon-triangle-1-s");
				}
				else {
					that.icon.toggleClass("ui-icon-triangle-1-w");
					that.icon.toggleClass("ui-icon-triangle-1-e");
				}
			});

		},
	
		/* undo constructor */
		_destroy: function() {	
			
			// remove header
			this.element.prev().remove();
			
			// remove wrapper
			this.element.unwrap();
			
			// remove css
			this.element.removeClass("widget-content " + this.options.styleClass);
		}	
	});

