/**
 * telus.ui.tabs.js
 * 
 * extends the JQuery UI Tabs (jquery.ui.tabs)
 * http://jqueryui.com/demos/tabs/
 * 
 * Tabs added would contain a close button and popout button
 * Newly added tab is automatically selected
 */

// @TODO: how to remove "$tabs" ... can't use this
// @TODO: make options "closable" and "popable" 

$.widget("fn.tabs", $.ui.tabs, {
	options: {
		// with close and popout icons
		tabTemplate : "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close'>Remove Tab</span><span class='ui-icon ui-icon-extlink'>Pop Out</span></li>"
		// select (display) newly added tab immediately
		,add : function(event, ui) {
			$tabs.tabs('select', '#' + ui.panel.id);
		}
	}
/*,	add: function(event, ui) { 
	$tabs.tabs('select', '#' + ui.id); 
	console.log(this);
	console.log("add called"); }*/
});


/* close tab */
// attach the close tab function to the close button
$( "span.ui-icon-close" ).live( "click", function() {
	var index = $( "li", $tabs ).index( $( this ).parent() );
	$tabs.tabs( "remove", index );
});


/* popout and close tab */
// attach the popout and close tab function to the popout button
$( "span.ui-icon-extlink" ).live( "click", function() {
	var index = $( "li", $tabs ).index( $( this ).parent() );
	
	// open url in new window
	var title = $(this).parent().children("a").html();
	var url = $("div:eq(" + index + ") > iframe", $tabs).attr('src');

	// popout a full size window
	popoutWindow = window.open(url + "?name=" + title, "popoutWindow", "height=1024, width=1280, toolbar=0, location=0, menubar=0" );

	// close tab
	$tabs.tabs( "remove", index );
	
	return popoutWindow;
	
});


/**
 * function addTab(message, settings)
 * Add a tab to the container. Select the tab if tab exists.
 * 
 * opts
 * 	id - the tab id will be used for identifying duplicate tabs. (ie customerId)
 * 	title - title of tab
 * 	url - if provided, the tab content will be an iframe with source set to this url
 * 	content - if url is not provided, tab content will be set to this
 */

var tab_counter = 0;
function addTab(message, settings) {
	// if id exists, select tab instead
	if ( $("#tabs-" + message.id).length) {		
		$tabs.tabs("select", "tabs-" + message.id);
	}
	
	// else add tab
	else {
		
		// initialize tab_counter
		if (!tab_counter) {
			tab_counter = $tabs.tabs("length");		
		}	
		++tab_counter;
		
		// use tab_counter as id if id not provided by user
		if (!message.id) {message.id = tab_counter;}
		
		
		// tab title			
		var tab_title = message.title || "Tab " + message.id;
		$tabs.tabs( "add", "#tabs-" + message.id, tab_title );
		
		if (message.url !== "undefined") {			
			// use iframe
			message.content = "<iframe frameborder='0' src='" + message.url + "' style='overflow-x:hidden;' scrolling='no'></iframe>";
		}
			
		// tab content
		$("#tabs-"+message.id).html(message.content);
	}
}
