function showLoader() {
	$(document).ready(function() {
		$("#loading").dialog({
			modal : true,
			dialogClass : 'loading',
			minWidth : 0,
			minHeight : 0,
			width : 220,
			height : 30,
			draggable : false,
			resizable : false
		});
	});
}

function hideLoader() {
	$(document).ready(function() {
		$("#loading").dialog('close');
	});
}

function hideTopNavItem() {
	// console.log("calling hide all");
	$('.nav_top_hider').parent().removeClass('active');
	$('.nav_top_hideable').slideUp(50);
}

$(document).ready(function() {
	$('.nav_top_hider').mouseenter(function(event) {
		var parent = $(this).parent();
		if (parent.hasClass('active')) {
			parent.removeClass('active');
			$('.nav_top_hideable', parent).slideUp(50);
		} else {
			hideTopNavItem();
			parent.addClass('active');
			$('.nav_top_hideable', parent).slideDown(50);
		}
	
		$('.nav_top_tabs').mouseleave(function() {
			hideTopNavItem();
		});
	});
	
	

	
});
