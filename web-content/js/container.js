/**
 * Container constructor
 * 
 * facilitates messaging between iframes and the container.
 * 
 * User should pass in a list of allowed domains of which the container is allowed to communicate with when initializing.
 * 
 * sample initialization
 * 
	var $Container = Container({
			origins: [ "http://telus.com", "http://domain1.com:" ]});	
 */
function Container(opts) {
	
	// default settings
	//		- set up acceptable origins
	var settings = {origins: "http://telus.com", popupId: "#container-popup"};
   
	// combine default settings with user options passed in parameter
	if ( opts ) { 
        $.extend( settings, opts );
      }

	/**
	 * receiveMessage
	 * 	- receive a message from an iframe
	 * 	- event.data is a key-value lists of arguments
	 *		- the "func" argument in "event.data" contains the name of the target function
	 * 		- the rest of the arguments are parameters to the function identified by "func"
	 */
	function receiveMessage(event) {
			
		// verify origin
		if ($.inArray(event.origin,opts.origins) < 0) {			
			if (console.log()) {
				console.log(event.origin + " is not in the origin list (" + opts.origins + ").");
			}
		}
		
		var message = $.parseJSON(event.data); // event.data is a json string
		/*
		 * the function name is stored in message.func the function should take
		 * in (message, settings) as arguments dispatch the function
		 */
		eval(message.func + '(message, settings)');	
	}
			
	/**
	 * adds event listener to handle the messaging for non-IE and IE
	 */
	if (window.addEventListener){           
		window.addEventListener("message", receiveMessage, false);
	} else {
		// IE
	    window.attachEvent("onmessage", receiveMessage);
	} 		

}

/* creates a popup dialog */
// keep container functions at a global scope so that new functions can be added (in a separate files) by developers as needed
// message and settings are key-value lists of arguments to the function 
function popup(message, settings) {
	// create popup with event.data as url
	$(settings.popupId).dialog({
		autoOpen : false,
		modal : message.modal? message.modal : true,
		height : message.height? message.height :500,
		width : message.width? message.width : 500
	});
	$(settings.popupId).html('<iframe src="' + message.url + '" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="auto" />').dialog("open");
	
}