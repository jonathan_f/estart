$(document).ready(function(){
	$('.accordion').click(function() {
		if ($(this).next().css('display') == 'none') // show
		{
			$(this).css('background', 'url(' + ctx + '/img/li_arrow_down.gif) no-repeat 0px 8px');
		}
		else // hide
		{
			$(this).css('background', 'url(' + ctx + '/img/li_arrow.gif) no-repeat 2px 6px');
		}
		$(this).next().toggle('medium');
		return false;
	}).next().hide();
});