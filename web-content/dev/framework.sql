SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "-07:00";

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  PRIMARY KEY (`username`, `authority`),
  UNIQUE KEY `ix_auth_username` (`username`,`authority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_authorities`
--

CREATE TABLE IF NOT EXISTS `group_authorities` (
  `group_id` bigint(20) NOT NULL,
  `authority` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`, `authority`),
  KEY `fk_group_authorities_group` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `EventLogID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EventDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EventExecutionStep` text NOT NULL,
  `EventDescription` text NOT NULL,
  `EventSource` varchar(30) DEFAULT NULL,
  `EventCode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`EventLogID`),
  UNIQUE KEY `Event Log ID_UNIQUE` (`EventLogID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `password` varchar(50) DEFAULT '',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for table `authorities`
--
ALTER TABLE `authorities`
  ADD CONSTRAINT `fk_auth_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
  
-- --------------------------------------------------------

--
-- Table structure for table `employee records`
--

CREATE TABLE IF NOT EXISTS `employee_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` varchar(20) NOT NULL,
  `numericalPID` varchar(20) DEFAULT NULL,
  `type` int(4) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `manager_numericalPID` varchar(20) DEFAULT NULL,
  `manager_email` varchar(45) DEFAULT NULL,
  `create_date` date NOT NULL,
  `hire_date` date NOT NULL,
  `card_sent_date` date DEFAULT NULL,
  `complete_date` date DEFAULT NULL,
  `hire_type` varchar(10) DEFAULT 'Normal',
  `last_updated_by` varchar(50) NOT NULL,
  `last_updated_at` datetime NOT NULL,
  `picture_ready_date` date DEFAULT NULL,
  `ldap_ready_date` date DEFAULT NULL,
  `cms_ready_date` date DEFAULT NULL,
  `idm_ready_date` date DEFAULT NULL,
  `hr_ehire_date` date DEFAULT NULL,
  `offer_signed_date` date DEFAULT NULL,
  `shipping_courier` int(11) DEFAULT -1,
  `shipping_tracking_number` varchar(45) DEFAULT NULL,
  `ready_to_provision_date` date DEFAULT NULL,
  `email_confirmation_sent` bit(1) DEFAULT b'0',
  `picture_received_date` date DEFAULT NULL,
  `vpn_ready_date` date DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL
  PRIMARY KEY (`id`),
  KEY `status_ref` (`status`),
  CONSTRAINT `status_ref` FOREIGN KEY (`status`) REFERENCES `employee_records_status_ref` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1


CREATE TABLE IF NOT EXISTS `employee_records_status_ref` (
  `id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` bit NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (1, 'New', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (2, 'Pending', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (3, 'Ready to Provision', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (4, 'Assigned', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (5, 'Card Sent', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (6, 'Completed', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (7, 'Quit Before Start', 0);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (8, 'Picture Received', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (9, 'Picture Ready', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (10, 'LDAP Ready', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (11, 'CMS Ready', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (12, 'IdM Ready', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (13, 'Back Dated', 0);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (14, 'Reported Late', 0);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (15, 'Escalation', 0);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (16, 'CATS Linked', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (17, 'LDAP Not Required', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (18, 'CMS Not Required', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (19, 'IdM Not Required', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (20, 'Card Received', 1);
INSERT INTO `employee_records_status_ref` (`id`, `name`, `type`) VALUES (21, 'Ship To Ready', 1);

CREATE TABLE IF NOT EXISTS `employee_records_status_history` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `tid` varchar(20) NOT NULL,
  `status` int(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `employee_records_status_history`
  ADD CONSTRAINT `fk_status` FOREIGN KEY (`status`) REFERENCES `employee_records_status_ref` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
CREATE  TABLE `employee_records_comments_type_ref` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
  
INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (1, 'User Input');

INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (2, 'HR Escalation');

INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (3, 'Manager Escalation');

INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (4, 'Late-Org-Management');

INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (5, 'Late-Finance');

INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (6, 'Late-Recruitment');

INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (7, 'Late-Business');

INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (8, 'Late-Security Customer Care');
INSERT INTO `employee_records_comments_type_ref` (`id`, `name`) VALUES (9, 'Late-Technical Issue');

CREATE TABLE IF NOT EXISTS `employee_records_comments` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `tid` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `type` int(4) NOT NULL,
  `content` varchar(500) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `employee_records_comments`
  ADD CONSTRAINT `fk_type` FOREIGN KEY (`type`) REFERENCES `employee_records_comments_type_ref` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  

CREATE  TABLE `eStart`.`employee_type_ref` (
  `id` INT(4) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `employee_type_ref` (`id`, `name`) VALUES (1, 'Regular Full Time');

INSERT INTO `employee_type_ref` (`id`, `name`) VALUES (2, 'Contractor - Regular');

INSERT INTO `employee_type_ref` (`id`, `name`) VALUES (3, 'Contractor - Building Access Only');

INSERT INTO `employee_type_ref` (`id`, `name`) VALUES (4, 'Contractor - System Access Only');

CREATE  TABLE `cats_employee_ref` (
  `cats_employee_ref_ID` INT NOT NULL AUTO_INCREMENT,
  `xid` VARCHAR(20) NOT NULL,
  `cat_id` INT NOT NULL,
  `birthday` DATE NOT NULL,
  `full_name` VARCHAR(50) NULL,
  `manager_full_name` VARCHAR(50) NULL,
  `applicant_city` VARCHAR(50) NULL,
  `applicant_province` VARCHAR(80) NULL,
  `company_name` VARCHAR(80) NULL,
  PRIMARY KEY (`xid`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE  TABLE `card_shipping_courier_ref` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) );
  
INSERT INTO `card_shipping_courier_ref` (`id`, `name`) VALUES (1, 'Purolator');

INSERT INTO `card_shipping_courier_ref` (`id`, `name`) VALUES (2, 'Dynamex');

INSERT INTO `card_shipping_courier_ref` (`id`, `name`) VALUES (3, 'Fedex');
  
CREATE  TABLE `mailroom_location_ref` (
  `id` INT NOT NULL ,
  `location` VARCHAR(60) NOT NULL ,
  PRIMARY KEY (`id`) );
  
INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (0, 'BC HUB 3777 Kingsway; Burnaby');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (1, 'BC 555 Robson St/768 Seymour St; Vancouver');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (2, 'BC 3500 Gilmore Way; Burnaby');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (3, 'BC 4535/4519 Canada Way, Burnaby');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (4, 'AB CLGR HUB 411 1st Street SE; Calgary');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (5, 'AB 622 - 1st Street SW / 120 7th Avenue SW; Calgary');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (6, 'AB 3030 2nd Avenue SE / 2912 M�morial Dr. SE; Calgary');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (7, 'AB EDTN HUB 10020 - 100th Street; Edmonton');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (8, 'AB 10035 - 102nd Avenue; Edmonton');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (9, 'ON HUB - TELUS House Consilium Scarborough ');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (10, 'ON 25 York Street; Toronto');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (11, 'ON 215 Slater Street; Ottawa');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (12, 'QC HUB 8885, route Transcanadienne; Saint-Laurent');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (13, 'QC 8851, route Transcanadienne; Saint-Laurent');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (14, 'QC 630, boul. Ren�-L�vesque; Montr�al');

INSERT INTO `mailroom_location_ref` (`id`, `location`) VALUES (15, 'QC 300, rue St-Paul; Qu�bec');



CREATE  TABLE `employee_records_shipping_mailroom` (
  `id` VARCHAR(10) NOT NULL ,
  `location_id` INT NOT NULL ,
  `last_updated_by` varchar(50) NOT NULL,
  `last_updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) );
  
ALTER TABLE `employee_records_shipping_mailroom` 
  ADD CONSTRAINT `location`
  FOREIGN KEY (`location_id` )
  REFERENCES `mailroom_location_ref` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `location` (`location_id` ASC) ;
  
CREATE  TABLE `employee_records_shipping_courier` (
  `id` VARCHAR(10) NOT NULL ,
  `street` VARCHAR(100) NULL ,
  `floor` VARCHAR(30) NULL ,
  `city` VARCHAR(50) NULL ,
  `province` VARCHAR(50) NULL ,
  `postal_code` VARCHAR(10) NULL ,
  `phone` VARCHAR(20) NULL ,
  `cost_centre` VARCHAR(20) NULL ,
  `last_updated_by` varchar(50) NOT NULL,
  `last_updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`) );
