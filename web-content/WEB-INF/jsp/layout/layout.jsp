<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<!-- CSS -->


<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery.ui.tabs.css" />

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/container-layout.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/telus.ui.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/dataTable/demo_page.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/dataTable/demo_table.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/menu/style.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/button.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/dataTable/TableTools_JUI.css" />
	
	
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/blueprint/screen.css" />
<!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/blueprint/screen.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/layout.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/navigation.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/extras.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/styles.css" />


<style lang="text/css">
#menu {
	min-width: 216px;
	width: auto;
	display: block;
	float: left;
	padding-right: 5px;
}

#body {
	width: 82%;
	display: block;
	float: right;
}

#footer {
	padding-top: 5px;
}

#hr {
	border: 0;
	border-top: 1px solid #000000;
	height: 0;
	background: #000000;
}
</style>

<!-- Javascript -->
<script type="text/javascript">
	var ctx = "${pageContext.request.contextPath}";
</script>

<script src="${pageContext.request.contextPath}/js/jquery-1.6.2.min.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery-ui-1.8.16.custom.min.js"></script>
<script src="${pageContext.request.contextPath}/js/json2.js"></script>
<script src="${pageContext.request.contextPath}/js/container.js"></script>
<script
	src="${pageContext.request.contextPath}/js/telus.ui.collapsible.js"></script>
<script src="${pageContext.request.contextPath}/js/telus.ui.tabs.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/js/TableTools.js"></script>
<script src="${pageContext.request.contextPath}/js/ZeroClipboard.js"></script>
<script
	src="${pageContext.request.contextPath}/js/ColReorderWithResize.js"></script>
<script src="${pageContext.request.contextPath}/js/date.format.js"></script>
<script src="${pageContext.request.contextPath}/js/menu.js"></script>

<script src="${pageContext.request.contextPath}/js/jquery.jeditable.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.dataTables.editable.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script src="https://saltspring.tsl.telus.com/habitat_bar/habitat_bar.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/layout.js"></script>

<script src="${pageContext.request.contextPath}/js/SDDISpringFramework.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//injectBar({replace_element_with_id: "habitat_bar"});
	});
</script>
</head>

<body class="container">
	
	<!-- Habitat Purple Bar -->
	<div id="habitat_bar"></div>
	<!--<hr class="space" />-->
	
	<!-- Header -->
	<header> 
		<tiles:insertAttribute name="header" />
	</header>
	<!--<hr class="space" />-->

	<!-- Green Nav -->
	<div class="nav_bar">
		<tiles:insertAttribute name="menu" />
	</div>
	
	<div class="content span-18 last">
		<hr class="space" />
		<tiles:insertAttribute name="body" />
		<hr class="space" />
	</div>
	<footer>
		<tiles:insertAttribute name="footer" />
	</footer>
</body>

</html>