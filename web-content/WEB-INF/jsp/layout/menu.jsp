<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<ul class="nav_top_tabs">
	<li><div class="nav_top_hider">
		<a href="${pageContext.request.contextPath}/">Team Members</a>
	</div></li>
	<li><div class="nav_top_hider">
		<a href="${pageContext.request.contextPath}/contractors">Contractors</a>
	</div></li>
	<sec:authorize access="hasRole('admin')">
		<li><div class="nav_top_hider">
				<a href="${pageContext.request.contextPath}/users"><spring:message code="app.nav.header1" /></a>
			</div>
			<ul class="nav_top_hideable">
				<li><a href="${pageContext.request.contextPath}/users"><spring:message
							code="app.nav.header1.content1" /></a></li>
				<li><a href="${pageContext.request.contextPath}/logs"><spring:message
							code="app.nav.header1.content2" /></a></li>			
			</ul></li>
	</sec:authorize>
	<sec:authorize access="hasRole('navigator') or hasRole('admin') or hasRole('report')">
		<li><div class="nav_top_hider">
			<a href="${pageContext.request.contextPath}/report">Reports</a>
		</div></li>
	</sec:authorize>
</ul>

