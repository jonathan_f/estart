<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<script>
var rootPath = '${pageContext.request.contextPath}';
var userName = '${UserName}';
var autoRefreshPeriod = 600000;//10mins = 10 * 60 sec * 1000 (ms)
var timer;
var oTable;

var tab_default = 0;
var tab_worklog = 1;
var tab_cats = 2;

var mailroom_locations = [];

$(document).ready(function() {
	//initiate auto refresh if setted
	var autoRefresh = getRequestParam('auto_refresh');
	if (autoRefresh == true) {
		$("#auto_refresh").prop('checked', true);
		startRefreshTimer();
	}

	//prepare table data
	var data = [];
	var icons = '';
<c:forEach items="${TeamMemberList}" var="user">
	<c:choose>
	<c:when test="${user.isVPNRequired == true && user.vpnReadyDate == null}">
		icons += '<div class="ui-state-default ui-corner-all list_icon text_icon right_space_20" title="VPN required"><span class="text_icon_text">V</span></div>';
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
	<c:choose>
	    <c:when test="${user.isLate == true}">
	    icons += '<div class="ui-state-default ui-corner-all list_icon" title="Late"><span class="ui-icon ui-icon-alert"></span></div>';
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
	<c:choose>
	    <c:when test="${user.isBackTracked == true}">
	    icons += '<div class="ui-state-default ui-corner-all list_icon right_space_20" title="Back Dated"><span class="ui-icon ui-icon-circle-triangle-w"></span></div>';
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
	<c:choose>
    <c:when test="${user.type != 3 && user.idmReadyDate == null}">
    	icons += '<div class="ui-state-default ui-corner-all list_icon text_icon right_space_20" title="lenel not ready"><span class="ui-icon ui-icon-carat-1-sw"></span></div>';
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
	<c:choose>
    <c:when test="${user.type != 3 && user.ldapReadyDate == null}">
    	icons +=  '<div class="ui-state-default ui-corner-all list_icon text_icon right_space_20" title="LDAP not ready"><span class="ui-icon ui-icon-person"></span></div>';
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
	
	<c:choose>
	<c:when test="${user.type != 4 && user.pictureReceivedDate == null}">
	icons += '<div id="${user.tid}_picture_received_icon" class="ui-state-default ui-corner-all list_icon text_icon right_space_20" title="Picture Received Date missing"><span class="text_icon_text">P</span></div>';
	    </c:when>
	    <c:otherwise>
	    </c:otherwise>
	</c:choose>
	<c:choose>
	<c:when test="${user.type != 4 && user.pictureReadyDate == null}">
	icons += '<div class="ui-state-default ui-corner-all list_icon text_icon right_space_20" title="Picture not ready"><span class="ui-icon ui-icon-image"></span></div>';
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>
	<c:choose>
	    <c:when test="${user.type == 3}">
	    icons += '<div class="ui-state-default ui-corner-all list_icon text_icon right_space_20" title="Building only Contractor"><span class="text_icon_text">B</span></div>';
		</c:when>
		<c:otherwise>
	    </c:otherwise>
	</c:choose>
	<c:choose>
    	<c:when test="${user.type == 4}">
    	icons += '<div class="ui-state-default ui-corner-all list_icon text_icon right_space_20" title="System only Contractor"><span class="text_icon_text">S</span></div>';
		</c:when>
		<c:otherwise>
	    </c:otherwise>
	</c:choose>
	<c:choose>
	    <c:when test="${user.catsID == '-1'}">
	    icons += '<div class="ui-state-default ui-corner-all list_icon right_space_20" title="CATS not linked"><span class="ui-icon ui-icon-link"></span></div>';
		</c:when>
		<c:otherwise>
	    </c:otherwise>
	</c:choose>
	
	data.push([ '<img src="${pageContext.request.contextPath}/img/icons/details_open.png">', 
	        	"${user.tid}", "${user.fullName}", "${user.hireDate}", "${user.hireType}",
	        	"${user.updatedBy}",
	        	"${user.statusName}", 
	        	"${user.completeDate}",
	        	icons, 
	        	'<div id="${user.tid}_details"><center><img src="${pageContext.request.contextPath}/img/ajax-loader-grey.gif" /></center></div>'
				]);
	icons = '';
</c:forEach>
	oTable = $('#dataTable').dataTable({
		"aaData": data,
		"aoColumns": [
						{ "sTitle": "", "sWidth": "20px" },
						{ "sTitle": "ID",  "sWidth": "80px"  },
						{ "sTitle": "Name", "sWidth": "160px" },
						{ "sTitle": "Hire Date", "sWidth": "80px"  },
						{ "sTitle": "Hire Type", "sWidth": "70px"  },
						{ "sTitle": "Updated By", "sWidth": "120px"  },
						{ "sTitle": "Status"},
						{ "sTitle": "Complete Date", "sWidth": "100px" },
						{ "sTitle": "", "sWidth": "160px" },
						{ "sTitle": "details" }
		      		],
		"iDisplayLength": 1000,
		sPaginationType : "full_numbers",
		"aoColumnDefs": [
		     			{ "bSearchable": false, "bSortable": false, "aTargets": [ 0 ] },
		     			{ "bSearchable": false, "bSortable": false, "aTargets": [ 8 ] },
		     			{ "bSearchable": false,"bVisible": false,"bSortable": false, "aTargets": [ 9 ] }
		     		],
		oLanguage : {
			sProcessing : "<spring:message code="app.datatable.text1" />",
			sLengthMenu : "<spring:message code="app.datatable.text2" />",
			sZeroRecords : "<spring:message code="app.datatable.text3" />",
			sInfo : "<spring:message code="app.datatable.text4" />",
			sInfoEmpty : "<spring:message code="app.datatable.text5" />",
			sInfoFiltered : "<spring:message code="app.datatable.text6" />",
			sInfoPostFix : "<spring:message code="app.datatable.text7" />",
			sSearch : "<spring:message code="app.datatable.text8" />",
			sUrl : "<spring:message code="app.datatable.text9" />",
			oPaginate : {
				sFirst : "<spring:message code="app.datatable.text10" />",
				sPrevious : "<spring:message code="app.datatable.text11" />",
				sNext : "<spring:message code="app.datatable.text12" />",
				sLast : "<spring:message code="app.datatable.text13" />",
			}
		}
	});
	
	/* Add event listener for opening and closing details      * Note that the indicator for showing which row is open is not controlled by DataTables,      * rather it is done here      */
	$('#dataTable tbody td img').live('click',function() {
		//var nTr = $(this).parents('tr')[0];
		var nTr = this.parentNode.parentNode;
		if ( this.src.match('details_close') ){ /* This row is already open - close it */
			this.src = "${pageContext.request.contextPath}/img/icons/details_open.png";
			oTable.fnClose(nTr);
		} else { /* Open this row */
			this.src = "${pageContext.request.contextPath}/img/icons/details_close.png";
			var id = nTr.children[1].innerHTML;
			var rowIndex = oTable.fnGetPosition(nTr);
			$.getJSON( rootPath+"/team_member/"+id, function (json) {
				generateTeamMemberDetails(json, rowIndex, tab_default);
			});
			oTable.fnOpen(nTr,fnFormatDetails(oTable,nTr),'details');
		}
	});

	//initialize report button
	$("#excelReportButton").button({
		icons : {
			primary : "ui-icon-print"
		}
	});
	$("#excelCardSentTodayButton").button({
		icons : {
			primary : "ui-icon-print"
		}
	});
	$("#excelAssignedToMeButton").button({
		icons : {
			primary : "ui-icon-print"
		}
	});
	

	//initialize auto refresh checkbox
	$("#auto_refresh").click (function ()
	{
		if ($(this).is (':checked')) {
			startRefreshTimer();
		} else {
			//stop refresh timer if started
			clearTimeout(timer);
		}
	});
	//initialize escalate dialog
	$( "#escalate_dialog" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Submit: function() {
				$.getJSON( rootPath+"/escalate/", 
						{tid:$('#escalate_tid').val(), type:$('#escalate_type').val()}, 
						function (json) {
					var rowIndex = $( "#escalate_tid_index" ).val();
					$( "#escalate_dialog" ).dialog( "close" );
					generateTeamMemberDetails(json, rowIndex, true);
				});
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});
	//initialize picture upload dialog
	$( "#upload_dialog" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Upload: function() {
				var fileName = $('#picture_upload_input').val();
				if (!validateFileExtensions(fileName) ) {
					$("#picture_upload_result").html(generateAlertMessage("Please select an (jpeg/jpg) image file to upload."));
					return;
				} else if (!validateFileSize(fileName)) {
					$("#picture_upload_result").html(generateAlertMessage("The size of selected file must under 1M."));
					return;
				} else { //display loading 
					$("#picture_upload_result").html('<center><img src="${pageContext.request.contextPath}/img/ajax-loader-grey.gif" /></center>');
				}
				$("#picture_upload_form").submit();
				setTimeout(function(){
					$.getJSON( rootPath+"/team_member/"+$('#picture_upload_tid').val(), function (json) {
						var rowIndex = $( "#picture_upload_tid_index" ).val();
						$( "#upload_dialog" ).dialog( "close" );
						generateTeamMemberDetails(json, rowIndex, true);
					});
					}, 2000);
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});
	//initialize card sent dialog
	$( "#ship_to_dialog" ).dialog({
		autoOpen: false,
		height: 450,
		width: 400,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Submit: function() {
				if ($('#ship_to_type').val() == 1 && ($('#ship_to_mailroom_location').val() == '' || $("#ship_to_mailroom_location_value").val() == '')) {
					//$("#ship_to_result").html(generateAlertMessage('courier Info is required!'));
					alert('Mailroom Location is required! Please select one from the dropdown.');
					return;
				}

				var courier_type = $('#ship_to_courier').val();
				//var courier_number = $('#ship_to_number').val();
				//courier_number = trimSpace(courier_number);
				
				if ($('#ship_to_type').val() == 1) {
					courier_type = 0;//default courier = mail room
				}
				$.getJSON( rootPath+"/shipto/", 
						{tid:$('#ship_to_tid').val(), 
						 type:$('#ship_to_type').val(),
						 mailroom_location:$("#ship_to_mailroom_location").val(),
						 mailroom_value:$("#ship_to_mailroom_location_value").val(),
						 street:$( "#ship_to_street" ).val(),
						 floor:$( "#ship_to_floor" ).val(),
						 city:$( "#ship_to_city" ).val(),
						 province:$( "#ship_to_province" ).val(),
						 postal_code:$( "#ship_to_postal_code" ).val(),
						 company:$( "#ship_to_company" ).val(),
						 phone:$( "#ship_to_phone" ).val(),
						 cost_centre:$( "#ship_to_cost_centre" ).val(),
						 courier:courier_type
						}, 
						function (json) {
						var rowIndex = $( "#ship_to_tid_index" ).val();
						//update row text
						
						
						$( "#ship_to_dialog" ).dialog( "close" );
						generateTeamMemberDetails(json, rowIndex, true);
					});
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});
	<c:forEach items="${MailroomLocations}" var="location" varStatus="locationIndex">
		mailroom_locations.push({value: "${locationIndex.index}", label: "${location}"});
	</c:forEach>
	$("#ship_to_mailroom_location").autocomplete({
		source: mailroom_locations,
		focus: function(event, ui) {
			// prevent autocomplete from updating the textbox
			event.preventDefault();
			// manually update the textbox
			$(this).val(ui.item.label);
		},
		select: function(event, ui) {
			// prevent autocomplete from updating the textbox
			event.preventDefault();
			// manually update the textbox and hidden field
			$(this).val(ui.item.label);
			$("#ship_to_mailroom_location_value").val(ui.item.value);
		}
	});
	$('#ship_to_type').change(function(){
		if ($('#ship_to_type').val() != 1) {
			$('#courier_div').show();
			$('#mailroom_div').hide();
		} else {
			$('#courier_div').hide();
			$('#mailroom_div').show();
		}
	});

	//initialize received dialog
	$( "#tracking_number_dialog" ).dialog({
		autoOpen: false,
		height: 300,
		width: 400,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Submit: function() {
				
				//if ($('#receive_date').val() == '') {
				//	alert('Card receive date is required!');
				//	return;
				//}
				//alert($('#card_sent_tid').val() + ' '+ $('#card_sent_tracking_number').val());
				$.getJSON( rootPath+"/tracking_number/", 
						{tid:$('#tracking_number_tid').val(), number:$('#tracking_number_tracking_number').val()}, 
						function (json) {
					var rowIndex = $( "#tracking_number_tid_index" ).val();
					//updateStatusText(rowIndex, "Card Sent");
					$( "#tracking_number_dialog" ).dialog( "close" );
					generateTeamMemberDetails(json, rowIndex, true);
				});
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});
	
	//initialize late reason dialog
	$( "#late_dialog" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Submit: function() {
				if ($('#late_ehire_date').val() =='' || $('#late_signed_date').val() == '') {
					//$("#late_result").html(generateAlertMessage('All fields are required!'));
					alert('All fields are required!');
					return;
				}
				//$( '#late_result').html('');
				
				$.getJSON( rootPath+"/late/", 
						{tid:$('#late_tid').val(), type:$('#late_type').val(), ehire_date:$('#late_ehire_date').val(), signed_date: $('#late_signed_date').val() }, 
						function (json) {
					var rowIndex = $( "#late_tid_index" ).val();
					//oTable.fnUpdate("Card Sent", rowIndex, 5);
					//oTable.fnUpdate(userName, rowIndex, 4);
					$( "#late_dialog" ).dialog( "close" );
					generateTeamMemberDetails(json, rowIndex, true);
					
				});
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});
	$('#late_ehire_date').datepicker();
	$('#late_signed_date').datepicker();


	//initialize force complete dialog
	$( "#complete_dialog" ).dialog({
		autoOpen: false,
		height: 300,
		width: 400,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Submit: function() {
				
				if ($('#complete_comment').val() == '') {
					alert('A reason for force complete is required!');
					//$("#complete_result").html(generateAlertMessage('A reason for force complete is required!'));
					return;
				}
				
				$.getJSON( rootPath+"/complete/", 
						{tid:$('#complete_tid').val(), comment:$('#complete_comment').val()}, 
						function (json) {
					var rowIndex = $( "#complete_tid_index" ).val();
					//alert(rowIndex +  " Completed");
					$('#complete_comment').val(json.employeeRecord.completeDate);
					updateStatusText(rowIndex, "Completed");
					//oTable.fnUpdate("Completed", rowIndex, 5);
					//oTable.fnUpdate(userName, rowIndex, 4);
					$( "#complete_dialog" ).dialog( "close" );
					generateTeamMemberDetails(json, rowIndex, true);
					
				});
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});

	//initialize received dialog
	$( "#receive_dialog" ).dialog({
		autoOpen: false,
		height: 300,
		width: 400,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Submit: function() {
				
				if ($('#receive_date').val() == '') {
					alert('Card receive date is required!');
					//$("#complete_result").html(generateAlertMessage('A reason for force complete is required!'));
					return;
				}
				
				$.getJSON( rootPath+"/receive/", 
						{tid:$('#receive_tid').val(), receive_date:$('#receive_date').val()}, 
						function (json) {
					var rowIndex = $( "#receive_tid_index" ).val();
					$("#"+ json.employeeRecord.tid + "_picture_received_icon").remove();
					$( "#receive_dialog" ).dialog( "close" );
					generateTeamMemberDetails(json, rowIndex, true);
					
				});
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});
	$('#receive_date').datepicker();


	$( "#hire_type_dialog" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Close: function() {
				$( this ).dialog( "close" );
			}, 
			Submit: function() {
				$.getJSON( rootPath+"/set_hire_type/", 
						{tid:$('#hire_type_tid').val(), type:$('#hire_type').val()}, 
						function (json) {
					var rowIndex = $( "#hire_type_tid_index" ).val();
					oTable.fnUpdate(json.employeeRecord.hireType, oTable.fnGetNodes(rowIndex), 4);
					$( "#hire_type_dialog" ).dialog( "close" );
					generateTeamMemberDetails(json, rowIndex, true);
				});
			}
		},
		show: { effect: "blind", duration: 300},
		hide: { effect: "explode", duration: 500}
	});
} );

/* ajax call to update record status at the back end*/
function updateStatus(rowIndex, status, url) {
	$.getJSON( url, function (json) {
		//update main table
		updateStatusText(rowIndex,status);
		//update details
		generateTeamMemberDetails(json, rowIndex, tab_worklog);
	});
}

/* helper function to update row content in the datatable*/
function updateStatusText(rowIndex, status) {
	var row = oTable.fnGetNodes(rowIndex);
	oTable.fnUpdate(status, row, 6);
	oTable.fnUpdate(userName, row, 5);
	if (status == 'Completed') {
		oTable.fnUpdate($('#complete_comment').val(), row, 7);//complete date field
		}
}

/* Formating function for row details */
function fnFormatDetails(table, nTr) {
	var aData = table.fnGetData(nTr);
	return aData[9];
}

function generateAlertMessage(text) {
	return '<span class="ui-icon ui-icon-alert" style="display: inline-block;"></span><span style="display: inline-block; overflow: hidden;">' + text + '</span>';
	
}

function startRefreshTimer(){
	timer = setTimeout("location.href='${pageContext.request.contextPath}/?auto_refresh=1'", autoRefreshPeriod);
}

function getRequestParam( name ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return "";
    else
        return results[1];
}

function validateFileSize(fileName) {
	var fileSize = 0;
	var maxSize = 1048576; //1M
	if (navigator.appName=="Microsoft Internet Explorer") {
		var fileSystem = new ActiveXObject("Scripting.FileSystemObject");
		fileSize = fileSystem.getFile(fileName).size;
	} else { //non IE
		fileSize = document.getElementById('picture_upload_input').files[0].size;
	}

	if (fileSize > maxSize) {
		return false;
	} else {
		return true;
	}
}

function validateFileExtensions(fileName) {
    
	 if (fileName == '') {
         // There is no file selected 
        return false;
     } else {
         var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
         //if (extension == "gif" || extension == "png" || extension == "bmp" || extension == "jpg" || extension == "jpeg") {
         if (extension == "jpg" || extension == "jpeg") {
             return true;//only support jpg
         }
         else {
             return false;
         }
     }
}

function trimSpace(value) {
	var s = value.replace(/\s+/g, '');
	s = s.replace(/[ ]{2,}/gi," ");
	s = s.replace(/\n /,"\n");
	return s;
}

function generateTeamMemberDetails(json, rowIndex, currentTab) {
	$('#' + json.SAPloginID + "_details").html(formatTeamMemberDetails(json, rowIndex));

	
	$('#'+ json.SAPloginID + '_escalate_hr').button({
		icons : {
			primary : "ui-icon-signal-diag"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#escalate_type" ).val('');
		$( "#escalate_tid" ).val(json.SAPloginID);
		$( "#escalate_tid_index" ).val(rowIndex);
		$( "#escalate_dialog" ).dialog( "open" );
	});
	/*
	$('#'+ json.SAPloginID + '_upload_picture').button({
		icons : {
			primary : "ui-icon-image"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#picture_upload_result" ).html('');//clear previous text
		$( "#picture_upload_input" ).val('');//clear previous file
		$( "#picture_upload_tid" ).val(json.SAPloginID);
		$( "#picture_upload_tid_index" ).val(rowIndex);
		$( "#upload_dialog" ).dialog( "open" );
	});*/

	$('#'+ json.SAPloginID + '_received').button({
		icons : {
			primary : "ui-icon-image"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#receive_date" ).val('');
		$( "#receive_tid" ).val(json.SAPloginID);
		$( "#receive_tid_index" ).val(rowIndex);
		$( "#receive_dialog" ).dialog( "open" );
	});
	
	$('#'+ json.SAPloginID + '_set_hire_type').button({
		icons : {
			primary : "ui-icon-pencil"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#hire_type_result" ).html('');//clear previous text
		$( "#hire_type" ).val('');//clear previous file
		$( "#hire_type_tid" ).val(json.SAPloginID);
		$( "#hire_type_tid_index" ).val(rowIndex);
		$( "#hire_type_dialog" ).dialog( "open" );
	});

	$('#'+ json.SAPloginID + '_assign').button({
		icons : {
			primary : "ui-icon-circle-arrow-e"
		}
	}).click(function() {
		var r = confirm("Assiging to " + userName + ".");
		if (r == true) {
			updateStatus(rowIndex, 'Assigned', rootPath+ '/assign/' +json.SAPloginID);
		}
		this.blur();//unfocus this button to fix a JQuery UI issue
	});

	$('#'+ json.SAPloginID + '_shipping_courier').button({
		icons : {
			primary : "ui-icon-print"
		}
	}).click(function() {
		var r = confirm("Confirm Generating shipping excel for " + json.PreferredFirstName + ' ' + json.LastName + ".");
		if (r == true) {
			location.href=rootPath+ '/shippingexcel/' +json.SAPloginID;
		}
		this.blur();//unfocus this button to fix a JQuery UI issue
	});

	$('#'+ json.SAPloginID + '_shipping_mailroom').button({
		icons : {
			primary : "ui-icon-print"
		}
	}).click(function() {
		var r = confirm("Confirm Generating MailRoom Label for " + json.PreferredFirstName + ' ' + json.LastName + ".");
		if (r == true) {
			location.href=rootPath+ '/mailroomlabel/' +json.SAPloginID;
		}
		this.blur();//unfocus this button to fix a JQuery UI issue
	});

	$('#'+ json.SAPloginID + '_tracking_number').button({
		icons : {
			primary : "ui-icon-mail-open"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#tracking_number_tracking_number" ).val('');
		$( "#tracking_number_tid" ).val(json.SAPloginID);
		$( "#tracking_number_tid_index" ).val(rowIndex);
		$( "#tracking_number_dialog" ).dialog( "open" );
	});

	$('#'+ json.SAPloginID + '_sent').button({
		icons : {
			primary : "ui-icon-mail-open"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		var r = confirm("Confirm changing record status to card sent.");
		if (r == true) {
			updateStatus(rowIndex, 'Card Sent', rootPath+ '/sent/' +json.SAPloginID);
		}
		this.blur();//unfocus this button to fix a JQuery UI issue
	});

	$('#'+ json.SAPloginID + '_shipto').button({
		icons : {
			primary : "ui-icon-mail-open"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#ship_to_type" ).val('');
		$('#mailroom_div').show();
		$( "#courier_div" ).hide();
		$( "#ship_to_mailroom_location" ).val('');
		$( "#ship_to_mailroom_location_value" ).val('');
		$( "#ship_to_street" ).val('');
		$( "#ship_to_floor" ).val('');
		$( "#ship_to_city" ).val('');
		$( "#ship_to_province" ).val('');
		$( "#ship_to_postal_code" ).val('');
		$( "#ship_to_company" ).val('');
		$( "#ship_to_phone" ).val('');
		$( "#ship_to_cost_centre" ).val('');
		
		$( "#ship_to_courier" ).val('');//clear previous file
		$( "#ship_to_number" ).val('');//clear previous file
		$( "#ship_to_tid" ).val(json.SAPloginID);
		$( "#ship_to_tid_index" ).val(rowIndex);
		$( "#ship_to_dialog" ).dialog( "open" );
	});
	
	$('#'+ json.SAPloginID + '_complete').button({
		icons : {
			primary : "ui-icon-circle-check"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#complete_comment" ).val('');
		$( "#complete_tid" ).val(json.SAPloginID);
		$( "#complete_tid_index" ).val(rowIndex);
		$( "#complete_dialog" ).dialog( "open" );
	});



	//late ui-icon-notice
	$('#'+ json.SAPloginID + '_late').button({
		icons : {
			primary : "ui-icon-alert"
		}
	}).click(function() {
		this.blur();//unfocus this button to fix a JQuery UI issue
		$( "#late_type" ).val('');
		$( "#late_tid" ).val(json.SAPloginID);
		$( "#late_tid_index" ).val(rowIndex);
		$( '#late_ehire_date').val('');
		$( '#late_signed_date').val('');
		$( '#late_result').html('');//clear previous text
		$( "#late_dialog" ).dialog( "open" );
	});

	$( "#"+ json.SAPloginID + "_tabs" ).tabs();
	if (currentTab == tab_worklog) {
		$( "#"+ json.SAPloginID + "_tabs" ).tabs('select', '#' +  json.SAPloginID + '_tabs_log');
	}  else if (currentTab == tab_cats) {
		$( "#"+ json.SAPloginID + "_tabs" ).tabs('select', '#' +  json.SAPloginID + '_tabs_cats');
	}
	
	$( '#'+ json.SAPloginID + '_comment_submit').button({
		icons : {
			primary : "ui-icon-circle-check"
		}
	}).click(function() {
		var content = $( '#'+ json.SAPloginID + '_comment_input').val();
		$.getJSON(  rootPath + '/comment/', { tid: json.SAPloginID, content: content }, function (json) {
			generateTeamMemberDetails(json, rowIndex, tab_worklog);
		});
	});

	//CATS profile link buttons
	<c:if test="${EmployeeType != 1}">
	var catsRecords = json.catsRecords;
	if(catsRecords != null) {
		for (var i = 0; i < catsRecords.length; i++) {
			var curRecord = catsRecords[i];
			$( '#'+ json.SAPloginID + '_cats_' + curRecord.catsID).button({
				icons : {
					primary : "ui-icon-link"
				}
			});
		}
	}
	</c:if>
}

function sendCATSIDLinkRequest(tid, catsID, rowIndex) {

	var r=confirm("This action will update the record in eSAM, please confirm to continue");
	if (r==true) {
	  	$.getJSON(  rootPath + '/link_cats/', { tid: tid, cats_id: catsID }, function (json) {
			generateTeamMemberDetails(json, rowIndex, tab_cats);
		});
	} 
}


/**/
function formatTeamMemberDetails(json, rowIndex) {
		var output = '';
		var line1_open = '<div class="label_row"><div class="label_text"><label>';
		var line1_close = ':</label></div>';
		var line2_open = '<div class="label_content"><span>';
		var line2_close = '</span></div></div>';

		var employeeType = '';
		if (json.employeeRecord.type == 1) {
			employeeType = 'Regular Team Member';
		} else if (json.employeeRecord.type == 2) {
			employeeType = 'Contractor';
		}if (json.employeeRecord.type == 3) {
			employeeType = 'Contractor - Building Access Only';
		}if (json.employeeRecord.type == 4) {
			employeeType = 'Contractor - System Access Only';
		}
		
		output = '<div id="'+ json.SAPloginID + '_tabs" style="float:left;width:762px;">'
			+ '<ul>'
			+ '<li><a href="#'+ json.SAPloginID + '_tabs_personal">Personal</a></li>'
			+ '<li><a href="#'+ json.SAPloginID + '_tabs_manager">Manager</a></li>';
			<c:if test="${EmployeeType != 1}">
		output += '<li><a href="#'+ json.SAPloginID + '_tabs_cats">CATS Profiles</a></li>';
			</c:if>
		output += '<li><a href="#'+ json.SAPloginID + '_tabs_other">Other</a></li>'
			+ '<li><a href="#'+ json.SAPloginID + '_tabs_log">Work Log</a></li>'
			+ '</ul>'
			+ '<div id="'+ json.SAPloginID + '_tabs_personal">'
				+ line1_open + 'Employee Type' + line1_close
				+ line2_open + employeeType + line2_close
				+ line1_open + 'Hire Type' + line1_close
				+ line2_open + json.employeeRecord.hireType + line2_close
				+ line1_open + 'Tags' + line1_close
				+ line2_open + json.employeeRecord.tags + line2_close
				+ line1_open + 'Personnel Number' + line1_close
				+ line2_open + json.PersonnelNumber + line2_close
				+ line1_open + 'SAP login ID' + line1_close
				+ line2_open + json.SAPloginID + line2_close
				+ line1_open + 'First Name' + line1_close
				+ line2_open + json.FirstName + line2_close
				+ line1_open + 'Last Name' + line1_close
				+ line2_open + json.LastName + line2_close
				+ line1_open + 'Preferred First Name' + line1_close
				+ line2_open + json.PreferredFirstName + line2_close
				+ line1_open + 'Communication Language' + line1_close
				+ line2_open + json.CommunicationLanguage + line2_close
				+ line1_open + 'TELUS Email Address' + line1_close
				+ line2_open + json.TELUSemailAddress + line2_close
				+ line1_open + 'Floor' + line1_close
				+ line2_open + json.Floor + line2_close
				+ line1_open + 'House No. Street' + line1_close
				+ line2_open + json.HouseNoStreet + line2_close
				+ line1_open + 'City' + line1_close
				+ line2_open + json.City + line2_close
				+ line1_open + 'Province' + line1_close
				+ line2_open + json.Province + line2_close
				+ line1_open + 'Postal Code' + line1_close
				+ line2_open + json.PostalCode + line2_close
				+ line1_open + 'Work Style' + line1_close
				+ line2_open + json.WorkStyle + line2_close
			+ '</div>'
			+ '<div id="'+ json.SAPloginID + '_tabs_manager">'
				+ line1_open + 'Manager ID' + line1_close
				+ line2_open + json.ManagerPersonnelNumber + line2_close
				+ line1_open + 'Manager First Name' + line1_close
				+ line2_open + json.ManagerFirstName + line2_close
				+ line1_open + 'Manager Last Name' + line1_close
				+ line2_open + json.ManagerLastName + line2_close
				+ line1_open + 'Manager Email' + line1_close
				+ line2_open + json.ManagerEmail + line2_close
				+ line1_open + 'Manager Phone Number' + line1_close
				+ line2_open + json.ManagerPhoneNumber + line2_close
			+ '</div>';
	
		    <c:if test="${EmployeeType != 1}">
		output += '<div id="'+ json.SAPloginID + '_tabs_cats">' + '<label>CATS Profiles</label><br />';
		if (json.catsID != "-1") { // cats linked
			output += '<div class="content_title list_item_green" style="clear:both;"><div class="content_title_text" style="float:left">'
			+ '<label>CATS ID : ' + json.catsID + ' [Linked]</label></div></div>';
		} else {
			var catsRecords = json.catsRecords;
			if (catsRecords != undefined && catsRecords != '') {
				for (var i = 0; i < catsRecords.length; i++) {
					var curRecord = catsRecords[i];
					//var icon = 'ui-icon ui-icon-closethick';
					var color = 'list_item_gray';
					var title = 'CATS ID : ' +  curRecord.catsID;
					
					output += '<div class="content_title ' + color + '" style="clear:both;"><div class="content_title_text" style="float:left">'
					+ '<label>' + title + '</label></div>';
					
					//select button
					output += '<div style="float:right;"><button id="' + json.SAPloginID + '_cats_' + curRecord.catsID + '" style="width:140px;" onclick="sendCATSIDLinkRequest(\''+ json.SAPloginID + '\', \'' + curRecord.catsID + '\', \'' + rowIndex + '\');"><span class="text">Link Profile</span></button></div>';
					
					output += '<div>' 
					+ line1_open + 'Full Name' + line1_close
					+ line2_open + curRecord.fullName + line2_close
					+ line1_open + 'Birthday' + line1_close
					+ line2_open + curRecord.birthday + line2_close
					+ line1_open + 'Manager Name' + line1_close
					+ line2_open + curRecord.managerName + line2_close
					+ line1_open + 'City' + line1_close
					+ line2_open + curRecord.applicantCity + line2_close
					+ line1_open + 'Province' + line1_close
					+ line2_open + curRecord.applicantProvince + line2_close
					+ line1_open + 'Company Name' + line1_close
					+ line2_open + curRecord.companyName + line2_close
					+ '</div>';
					output += '</div>';
				}
			} else {
				output += '<div class="content_title" style="clear:both;"><div class="content_title_text" style="float:left">'
				+ '<label>No CATS profile is found for this employee!</label></div></div>';
			}
		}
		output +='</div>';
		    </c:if>
		
		output += '<div id="'+ json.SAPloginID + '_tabs_other">'
				+ line1_open + 'TaleoID' + line1_close
				+ line2_open + json.TaleoID + line2_close
				+ line1_open + 'Hire/Rehire Date' + line1_close
				+ line2_open + json.HireRehireDate + line2_close
				+ line1_open + 'Service Anniversary Date' + line1_close
				+ line2_open + json.ServiceAnniversaryDate + line2_close
				+ line1_open + 'Employee Group' + line1_close
				+ line2_open + json.EmployeeGroup + line2_close
				+ line1_open + 'Employee Group Description' + line1_close
				+ line2_open + json.EmployeeGroupDescription + line2_close
				+ line1_open + 'Employee SubGroup' + line1_close
				+ line2_open + json.EmployeeSubGroup + line2_close
				+ line1_open + 'Employee Subgroup Description' + line1_close
				+ line2_open + json.EmployeeSubgroupDescription + line2_close
				+ line1_open + 'Personnel Subarea' + line1_close
				+ line2_open + json.PersonnelSubarea + line2_close
				+ line1_open + 'Personnel Subarea Description' + line1_close
				+ line2_open + json.PersonnelSubareaDescription + line2_close
				+ line1_open + 'Company Code' + line1_close
				+ line2_open + json.CompanyCode + line2_close
				+ line1_open + 'Position Code' + line1_close
				+ line2_open + json.PositionCode + line2_close
				+ line1_open + 'Position Description' + line1_close
				+ line2_open + json.PositionDescription + line2_close
				+ line1_open + 'Job Code' + line1_close
				+ line2_open + json.JobCode + line2_close
				+ line1_open + 'Job Description' + line1_close
				+ line2_open + json.JobDescription + line2_close
				+ line1_open + 'Organization Unit' + line1_close
				+ line2_open + json.OrganizationUnit + line2_close
				+ line1_open + 'Organization Unit Description' + line1_close
				+ line2_open + json.OrganizationUnitDescription + line2_close
				+ line1_open + 'Cost Center' + line1_close
				+ line2_open + json.CostCenter + line2_close
				+ line1_open + 'CLLI' + line1_close
				+ line2_open + json.CLLI + line2_close
				+ line1_open + 'CLLI description' + line1_close
				+ line2_open + json.CLLIdescription + line2_close
				+ line1_open + 'Business Identifier' + line1_close
				+ line2_open + json.BusinessIdentifier + line2_close
				+ line1_open + 'Org Hierarchy Code Description' + line1_close
				+ line2_open + json.OrgHierarchyCodeDescription + line2_close
			+ '</div>'
			+ '<div id="'+ json.SAPloginID + '_tabs_log">'
			+ '<div style="float:left; width:49%;">'
			//history for status starts here
				+ '<label>History</label><br />'
				+ '<ul id="sortable">';
			var history = json.history;
			for (var i = 0; i < history.length; i++) {
				var icon = 'ui-icon ui-icon-check';
				var color = 'list_item_green';
				if (history[i].isGoodType == false) {
					icon = 'ui-icon ui-icon-closethick';
					color = 'list_item_red';
				}
				if (history[i].active == false) {
					icon = 'ui-icon ui-icon-closethick';
					color = 'list_item_gray';
				}
				output += '<li class="' + color + '"><span class="' + icon + '"></span>'
				+ '<div style="width:140px;float:left;">' +  history[i].statusName + '</div>'
				+ '<div style="width:120px;float:left;">' +  history[i].createdBy + '</div>'
				+ '<div style="float:left;">' +  history[i].createdDate + '</div></li>';
			}
			output += '</ul>'
			//End of history
			+ '</div>'
			+ '<div class="divider"></div>'
			+ '<div style="float:right; width:49%;">'
			//comments start here
			+ '<label>Comments</label><br />'
			+ '<div id="'+ json.SAPloginID + '_comments">';
			
			var comments = json.comments;
			for (var i = 0; i < comments.length; i++) {
				color = 'list_item_green';
				if (comments[i].active == false) {
					color = 'list_item_gray';
				}
			    output += '<div class="comment">'
					+ '<div class="comment_title ' + color + '"><div class="comment_name">'+ comments[i].createdBy + '</div><div class="comment_date">' + comments[i].createdAt +  '</div></div>'
					+ '<textarea readonly>' + comments[i].comment + '</textarea>'
					+ '</div>';
			}
			output += '</div><br />'
			//end of comments
			

			<sec:authorize access="hasRole('admin') or hasRole('navigator') or hasRole('provisioner')">
			+ '<form id="'+ json.SAPloginID + '_comment" action="' + rootPath + '/comment/" method="post"><textarea id="'+ json.SAPloginID + '_comment_input" name="content" rows="6" cols="25"></textarea>'
			+ '<input type="hidden" name="tid" value="'+ json.SAPloginID + '" /></form>'
			+ '<button id="'+ json.SAPloginID + '_comment_submit" style="float:right" ><span class="text">Submit</span></button>'
			</sec:authorize>
			+ '</div>'
			+ '</div>'

			+ '</div>'
		
		+ '<div style="float:right;width:15%;">';

		var buttonGenerated = false;
		<sec:authorize access="hasRole('navigator') or hasRole('admin')">
		//output = output + '<button id="' + json.SAPloginID + '_b1" style="width:140px;"><span class="text">Verified</span></button>'
		//	+ '<button id="' + json.SAPloginID + '_b2" style="width:140px;"><span class="text">On Hold</span></button>';
		
		<c:choose>
		    <c:when test="${EmployeeType == 1}">
		    output = output + '<button id="' + json.SAPloginID + '_escalate_hr" style="width:140px;"><span class="text">Escalations</span></button>';
		    </c:when>
		    <c:otherwise>
		    output = output + '<button id="' + json.SAPloginID + '_escalate_hr" style="width:140px;"><span class="text">Notify Manager</span></button>';
		    </c:otherwise>
		</c:choose>
		
		output += '<button id="' + json.SAPloginID + '_late" style="width:140px;"><span class="text">Late Reason</span></button>';
		output += '<button id="' + json.SAPloginID + '_received" style="width:140px;"><span class="text">Picture Received</span></button>';
		output += '<button id="' + json.SAPloginID + '_assign" style="width:140px;"><span class="text">Assign to Me</span></button>';
		output += '<button id="' + json.SAPloginID + '_set_hire_type" style="width:140px;"><span class="text">Set Hire Type</span></button>';
		output += '<button id="' + json.SAPloginID + '_shipto" style="width:140px;"><span class="text">Ship To</span></button>';
		if (json.employeeRecord.courier > 0) {
			output += '<button id="' + json.SAPloginID + '_tracking_number" style="width:140px;"><span class="text">Courier Tracking</span></button>';
		}
		output += '<button id="' + json.SAPloginID + '_shipping_courier" style="width:140px;"><span class="text">Courier Form</span></button>';
		output += '<button id="' + json.SAPloginID + '_shipping_mailroom" style="width:140px;"><span class="text">MailRoom Label</span></button>';
		output += '<button id="' + json.SAPloginID + '_sent" style="width:140px;"><span class="text">Card Sent</span></button>';
		output += '<button id="' + json.SAPloginID + '_complete" style="width:140px;"><span class="text">Completed</span></button>';
			
			
		buttonGenerated = true;
		</sec:authorize>

		<sec:authorize access="hasRole('provisioner')">
		if (!buttonGenerated && (json.employeeRecord.status == 3 || json.employeeRecord.status == 4 || json.employeeRecord.status == 5)) {
			output += '<button id="' + json.SAPloginID + '_assign" style="width:140px;"><span class="text">Assign to Me</span></button>';
			if (json.employeeRecord.courier > 0) {
			output += '<button id="' + json.SAPloginID + '_tracking_number" style="width:140px;"><span class="text">Courier Tracking</span></button>';
			}
			output += '<button id="' + json.SAPloginID + '_shipping_courier" style="width:140px;"><span class="text">Courier Form</span></button>';
			output += '<button id="' + json.SAPloginID + '_shipping_mailroom" style="width:140px;"><span class="text">MailRoom Label</span></button>';
			output += '<button id="' + json.SAPloginID + '_sent" style="width:140px;"><span class="text">Card Sent</span></button>';
		
		}
		</sec:authorize>
		output = output +  '</div>';
		return output;
	}
</script>
<div>
	<c:choose>
	    <c:when test="${EmployeeType == 1}">
	   		<c:set var="employee_type_name" scope="request" value="Team Members"/>
	    </c:when>
	    <c:otherwise>
	    	<c:set var="employee_type_name" scope="request" value="Contractors"/>
	    </c:otherwise>
	</c:choose>
	<div style="float:left; width:300px;">
		<h2><b>List of ${employee_type_name}</b></h2>
		<label>From two weeks ago to future</label><br />
	</div>
	<div style="float:right; width:610px;">
		<button id="excelReportButton" style="width:200px;" onclick="location.href='${pageContext.request.contextPath}/excel'" />
			<span class="text">Generate Excel Reports</span>
		</button>
		<button id="excelCardSentTodayButton" style="width:200px;" onclick="location.href='${pageContext.request.contextPath}/excel_card_sent_today'" />
			<span class="text">Excel for Card Sent Today</span>
		</button>
		<button id="excelAssignedToMeButton" style="width:200px;" onclick="location.href='${pageContext.request.contextPath}/assigned_to_me_excel'" />
			<span class="text">Excel Assigned To Me</span>
		</button>
		<br/>
		<input type="checkbox" id="auto_refresh">Refresh/10 Minutes
	</div>
</div>
<style>  
#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%;}  
#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.0em; height: 18px; }
#sortable li span { position: absolute; margin-left: -1.3em; }  
textarea {
	width:350px;
    height:100px;
}
</style>
<div id="tableContainer" style="margin-left:auto; margin-right:auto; width: auto;clear:both;">
	<table id="dataTable" style="width:100%;"></table>
</div>
<div id="escalate_dialog" title="Escalations" style="display:none">
	<form id="escalate_form" method="post" method="post">
	  <!-- File input -->
		<div class="label_row">
			<div class="dialog_label_text"><label>Reason Type</label></div>
			<div class="dialog_label_content"><select id="escalate_type" name="type">
			  <option value="1">Picture Not Ready</option>
			  <option value="2">Picture Invalid</option>
			  <c:if test="${EmployeeType != 1}">
	   		  <option value="3">Access Requests</option>
	   		  <option value="4">Clearance Record</option>
	  		  </c:if>
			</select></div>
		</div>
	  	<input name="tid" id="escalate_tid" type="hidden" value="" />
	  	</div>
	</form>
	<input name="rowIndex" id="escalate_tid_index" type="hidden" value="" />
</div>
<div id="upload_dialog" title="Picture Upload" style="display:none">
	<form id="picture_upload_form" action="${pageContext.request.contextPath}/upload" method="post" enctype="multipart/form-data" method="post" target="uploadIframe">
	  <!-- File input -->    
	  <input name="file" id="picture_upload_input" type="file" value="" />
	  <input name="tid" id="picture_upload_tid" type="hidden" value="" />
	</form>
	<input name="rowIndex" id="picture_upload_tid_index" type="hidden" value="" />
	<iframe name="uploadIframe" style="display:none;"></iframe>
	<div id="picture_upload_result"></div>
</div>
<div id="ship_to_dialog" title="Card Delivery" style="display:none">
	<form id="ship_to_form" method="post" method="post">
	  <!-- File input -->
		<div class="label_row">
			<div class="dialog_label_text"><label>Destination Type</label></div>
			<div class="dialog_label_content"><select id="ship_to_type" name="type">
			  <option value="1">Mail Room</option>
			  <option value="2">Courier</option>
			</select></div>
		</div>
		<div id="mailroom_div">
			<div class="label_row">
				<div class="dialog_label_text"><label>Location:</label></div>
				<div class="dialog_label_content"><input id="ship_to_mailroom_location" name="mailroom_location" type="text" value="" /></div>
				<input name="tid" id="ship_to_mailroom_location_value" type="hidden" value="" />
			</div>
		</div>

		<div id="courier_div" style="display:none;">
			<div class="label_row">
				<div class="dialog_label_text"><label>Courier:</label></div>
				<div class="dialog_label_content"><select id="ship_to_courier" name="courier">
					<option value="1">Purolator</option>
					<option value="2">Dynamex</option>
					<option value="3">Fedex</option>
				</select></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>Company:</label></div>
				<div class="dialog_label_content"><input id="ship_to_company" name="company" type="text" value="" /></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>Floor:</label></div>
				<div class="dialog_label_content"><input id="ship_to_floor" name="floor" type="text" value="" /></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>Street:</label></div>
				<div class="dialog_label_content"><input id="ship_to_street" name="street" type="text" value="" /></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>City:</label></div>
				<div class="dialog_label_content"><input id="ship_to_city" name="city" type="text" value="" /></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>Province:</label></div>
				<div class="dialog_label_content"><input id="ship_to_province" name="province" type="text" value="" /></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>Postal Code:</label></div>
				<div class="dialog_label_content"><input id="ship_to_postal_code" name="postal_code" type="text" value="" /></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>Phone:</label></div>
				<div class="dialog_label_content"><input id="ship_to_phone" name="phone" type="text" value="" /></div>
			</div>
			<div class="label_row">
				<div class="dialog_label_text"><label>Cost Centre:</label></div>
				<div class="dialog_label_content"><input id="ship_to_cost_centre" name="cost_centre" type="text" value="" /></div>
			</div>
		</div>
	  	<input name="tid" id="ship_to_tid" type="hidden" value="" />
	  	</div>
	</form>
	<input name="rowIndex" id="ship_to_tid_index" type="hidden" value="" />
	<div id="ship_to_result"></div>
</div>
<div id="late_dialog" title="Late Reason" style="display:none">
	<form id="late_form" action="${pageContext.request.contextPath}/late" method="post" method="post">
	  <!-- File input -->
		<div class="label_row">
			<div class="dialog_label_text"><label>Reason Type</label></div>
			<div class="dialog_label_content"><select id="late_type" name="type">
			  <option value="4">Org-Management</option>
			  <option value="5">Finance</option>
			  <option value="6">Recruitment</option>
			  <option value="7">Business</option>
			  <option value="8">Security Customer Care</option>
			  <option value="9">Technical Issue</option>
			</select></div>
		</div>
		<div class="label_row">
			<div class="dialog_label_text"><label>eHire Date</label></div>
			<div class="dialog_label_content">
				<input id="late_ehire_date" name="ehire_date" type="text" value="" />
			</div>
		</div>
		<div class="label_row">
			<div class="dialog_label_text"><label>Offer Signed Date</label></div>
			<div class="dialog_label_content">
				<input id="late_signed_date" name="signed_date" type="text" value="" />
			</div>
		</div>
	  	<input name="tid" id="late_tid" type="hidden" value="" />
	  	</div>
	</form>
	<input name="rowIndex" id="late_tid_index" type="hidden" value="" />
	<div id="late_result"></div>
</div>
<div id="complete_dialog" title="Force Complete" style="display:none">
	<form id="complete_form" action="${pageContext.request.contextPath}/complete" method="post" method="post">
	  <!-- File input -->
		<div class="label_row">
			<div><label>Please explain your reason to force complete this record:</label></div>
			<div class="dialog_label_content">
				<textarea id="complete_comment" name="content" rows="6" cols="20"></textarea></div>
		</div>
	  	<input name="tid" id="complete_tid" type="hidden" value="" />
	  	</div>
	</form>
	<input name="rowIndex" id="complete_tid_index" type="hidden" value="" />
	<div id="complete_result"></div>
</div>
<div id="receive_dialog" title="Card Received" style="display:none">
	<form id="receive_form" action="${pageContext.request.contextPath}/receive" method="post" method="post">
	  <!-- File input -->
		<div class="label_row">
			<div><label>Please enter card received date:</label></div>
			<div class="dialog_label_content">
				<input id="receive_date" name="receive_date" type="text" value="" />
			</div>
	  	<input name="tid" id="receive_tid" type="hidden" value="" />
	  	</div>
	</form>
	<input name="rowIndex" id="receive_tid_index" type="hidden" value="" />
	<div id="receive_result"></div>
</div>
<div id="hire_type_dialog" title="Set Hire Type" style="display:none">
	<form id="hire_type_form" action="${pageContext.request.contextPath}/set_hire_type" method="post" method="post">
	  <!-- File input -->
		<div class="label_row">
			<div class="dialog_label_text"><label>Hire Type</label></div>
			<div class="dialog_label_content"><select id="hire_type" name="type">
			  <option value="1">Normal</option>
			  <option value="2">Rehire</option>
			  <option value="3">C-E</option>
			</select></div>
		</div>

	  	<input name="tid" id="hire_type_tid" type="hidden" value="" />
	  	</div>
	</form>
	<input name="rowIndex" id="hire_type_tid_index" type="hidden" value="" />
	<div id="hire_type_result"></div>
</div>
<div id="tracking_number_dialog" title="Card Sent" style="display:none">
	<form id="tracking_number_form" method="post" method="post">
	  <!-- File input -->
		<div class="label_row" >
			<div><label>Please enter Courier Tracking Number:</label></div>
			<div class="dialog_label_content">
				<input id="tracking_number_tracking_number" name="tracking_number" type="text" value="" />
			</div>
	  	<input name="tid" id="tracking_number_tid" type="hidden" value="" />
	  	</div>
	</form>
	<input name="rowIndex" id="tracking_number_tid_index" type="hidden" value="" />
	<div id="receive_result"></div>
</div>