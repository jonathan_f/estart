<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!-- Javascript for Datatable -->
<script type="text/javascript">
	var oTable;
	
	$(document).ready(function(e) {

		oTable = $('#dataTable').dataTable({
			oLanguage : {
				sProcessing : "<spring:message code="app.datatable.text1" />",
				sLengthMenu : "<spring:message code="app.datatable.text2" />",
				sZeroRecords : "<spring:message code="app.datatable.text3" />",
				sInfo : "<spring:message code="app.datatable.text4" />",
				sInfoEmpty : "<spring:message code="app.datatable.text5" />",
				sInfoFiltered : "<spring:message code="app.datatable.text6" />",
				sInfoPostFix : "<spring:message code="app.datatable.text7" />",
				sSearch : "<spring:message code="app.datatable.text8" />",
				sUrl : "<spring:message code="app.datatable.text9" />",
				oPaginate : {
					sFirst : "<spring:message code="app.datatable.text10" />",
					sPrevious : "<spring:message code="app.datatable.text11" />",
					sNext : "<spring:message code="app.datatable.text12" />",
					sLast : "<spring:message code="app.datatable.text13" />",
				}
			},
			bJQueryUI : true,
			sPaginationType : "full_numbers",
			bProcessing : true,
			bServerSide : true,
			sAjaxSource : "log/getLog",
			fnServerData: function ( sSource, aoData, fnCallback ) {
				 			$.getJSON( sSource, aoData, function (json) {
								fnCallback(json);
                 			});
						},
			fnInitComplete: function (oSetting, json) {
							$("div[class=popup]").dialog(
									{
									autoOpen: false,
									width: "600",
									height: "600",
									buttons: { "Ok": function() { $(this).dialog("close"); } } 
									}
							);
						},
			fnInfoCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				$("div[class=popup]").dialog(
						{
						autoOpen: false,
						width: "600",
						height: "600",
						buttons: { "Ok": function() { $(this).dialog("close"); } }
						}
				);
						},			
			aoColumnDefs : [ {
				aTargets : [ 0 ],
				bSearchable: false,
				mDataProp: "id"
			}, {
				aTargets : [ 1 ],
				mDataProp : "code"
			}, {
				aTargets : [ 2 ],
				mDataProp : function(source, type, val) {
					if (source.code == "Work Log") {
						return source.description;
					}
					var output = '<div id="'
					+ 'dialog' 
					+ source.id
					+ '" title="Details" class="popup" style="display:none">' 
					+ '<p>' 
					+ source.description
					+ '</p>'
					+ '</div>';
					
					output += '<a href="#" onclick=\''
					+ '$("#dialog' + source.id  +'").dialog("open")'
					+ '\'>Click for Details</a>'; 
					
					return output;
				}	
			}, {
				aTargets : [ 3 ],
				mDataProp : "executionStep"
			}, {
				aTargets : [ 4 ],
				mDataProp : "source"
			}, {
				aTargets : [ 5 ],
				mDataProp : "dateTime"
			} ]
		});
		oTable.fnSetFilteringDelay(500);
	});

</script>

<!-- DataTable -->
<table id="dataTable" style="width: 100%">
	<thead>
		<tr>
			<th><spring:message code="app.log.datatable.header1" /></th>
			<th><spring:message code="app.log.datatable.header2" /></th>
			<th><spring:message code="app.log.datatable.header3" /></th>
			<th><spring:message code="app.log.datatable.header4" /></th>
			<th><spring:message code="app.log.datatable.header5" /></th>
			<th><spring:message code="app.log.datatable.header6" /></th>
		</tr>
	</thead>
</table>

