<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script type="text/javascript">
	$(document).ready(function(e) {
		$("a[class=button]").button();
		$("#email").autocomplete({
			source: function( request, response) {
				$.ajax({
					url: "users/autocomplete",
					type: "POST",
					dataType: "json",
					data: {
						name_startsWith: request.term
					},
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
							label: item.firstName + ' ' + item.lastName + ' (' + item.emailAddress.toLowerCase() + ')',
								value: item.emailAddress.toLowerCase()
							};
						}));
					}
				})
			},
			minLength: 2,
			open: function() {
				$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
				$("#tid").bind("blur", function () {
                    if (typeof $(this).data("uiItem") === 'undefined') {
                        $(this).val("");
                    }
                });
			},
			close: function() {
				$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                $("#tid").unbind("blur");
                
			}
		});
		//bJQueryUI : true,
		var oTable = $('#dataTable').dataTable({
			sPaginationType : "full_numbers",
			oLanguage : {
				sProcessing : "<spring:message code="app.datatable.text1" />",
				sLengthMenu : "<spring:message code="app.datatable.text2" />",
				sZeroRecords : "<spring:message code="app.datatable.text3" />",
				sInfo : "<spring:message code="app.datatable.text4" />",
				sInfoEmpty : "<spring:message code="app.datatable.text5" />",
				sInfoFiltered : "<spring:message code="app.datatable.text6" />",
				sInfoPostFix : "<spring:message code="app.datatable.text7" />",
				sSearch : "<spring:message code="app.datatable.text8" />",
				sUrl : "<spring:message code="app.datatable.text9" />",
				oPaginate : {
					sFirst : "<spring:message code="app.datatable.text10" />",
					sPrevious : "<spring:message code="app.datatable.text11" />",
					sNext : "<spring:message code="app.datatable.text12" />",
					sLast : "<spring:message code="app.datatable.text13" />",
				}
			}
		}).makeEditable({
			sAddURL: "users/insert",
			sAddHttpMethod: "POST",
			sDeleteURL: "users/delete",
			sDeleteHttpMethod: "POST",
			sUpdateURL: 'users/update',
			sUpdateHttpMethod: "POST",
			oAddNewRowButtonOptions: {	label: "<spring:message code="app.user.datatable.add" />...",
				icons: {primary:'ui-icon-plus'} 
			},
			oDeleteRowButtonOptions: {	label: "<spring:message code="app.user.datatable.remove" />", 
				icons: {primary:'ui-icon-trash'}
			},
			oAddNewRowFormOptions: { 	
                title: 'Add a new user',
				show: "blind",
				hide: "explode",
                modal: false
			},
			sAddDeleteToolbarSelector: ".dataTables_length",
			aoColumns: [	null,
			            	{
							loadtext: 'loading...',
							type: 'select',
							onblur: 'cancel',
							submit: 'Ok',
							data: "{'true':'true','false':'false'}"
			            	},
			            	{}
			          	]

		});

	});

</script>
<div style="margin-left:auto; margin-right:auto; width: auto;">
<!-- Datatable -->
<table id="dataTable" style="width: 100%;">
	<thead>
		<tr>
			<th><spring:message code="app.user.datatable.header1" /></th>
			<th><spring:message code="app.user.datatable.header2" /></th>
			<th><spring:message code="app.user.datatable.header3" /></th>					
		</tr>
	</thead>
	<c:forEach items="${Users}" var="user">
		<tr id="${user.email}">
			<td><c:out value="${user.email}" /></td>
			<td><c:out value="${user.enabled}" /></td>
			<td><c:forEach items="${user.permissions}" var="permission"><c:out value="${fn:trim(permission)}" />;</c:forEach></td>
		</tr>
	</c:forEach>
</table>

<!-- Form for new user -->
<form id="formAddNewRow" action="#" title="Add a new user">
	<label for="email"><spring:message code="app.user.popup1" />:</label>
	<br /> 
	<input id="email" name="email" rel="0"/> 
	<br />
	<label for="enabled"><spring:message code="app.user.popup2" />:</label>
	<br />
	<select id="enabled" name="enabled" rel="1">
		<option value="true"><spring:message code="app.common.true" /></option>
		<option value="false"><spring:message code="app.common.false" /></option>
	</select>
	<br />
	<label for="permissions"><spring:message code="app.user.popup3" />:</label>
	<br />
	<input id="permissions" type="text" name="permissions" rel="2"/>
	<br />
	 
</form>
</div>
