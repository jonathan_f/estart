<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<script>
$(function() {
	var startDateString = "${startDate}";
	var endDateString = "${endDate}";
	var startDate = new Date(startDateString);
	var endDate = new Date(endDateString);

	$( "#start_date" ).datepicker({ maxDate: endDate, 
		onSelect: function(dateText) {
			$( "#end_date" ).datepicker("option", "minDate", new Date(dateText));
	  	}
	});
	$( "#start_date" ).datepicker( "setDate" , startDate );
	
	$( "#end_date" ).datepicker({minDate: startDate,
		onSelect: function(dateText) {
			$( "#start_date" ).datepicker("option", "maxDate", new Date(dateText));
	  	}
  	});
	$( "#end_date" ).datepicker( "setDate" , endDate );

	
	$('#select_submit').button({
		icons : {
			primary : "ui-icon-circle-triangle-s"
		}
	});

	$("#excelReportButton").button({
		icons : {
			primary : "ui-icon-print"
		}
	});
});
</script>
<div class="content-container">
<form id="select_form" action="${pageContext.request.contextPath}/report" method="post" method="post">
	<div class="label_row">
		<div class="label_datepicker"><label>Start Date:</label><input type="text" id="start_date" name="startDate" size="10" /></div>
		<div class="label_datepicker"><label>End Date:</label><input type="text" id="end_date" name="endDate" size="10" /></div>
		<div class="label_text">
			<button id="select_submit" onclick="$('#excelMode').val(0);$('#select_form').submit();" name="submit"><span class="text">Refresh</span></button>
			<button id="excelReportButton" onclick="$('#excelMode').val(1);$('#select_form').submit();" />
				<span class="text">Excel</span>
			</button>
			<input type="hidden" id="excelMode" name="excelMode" value="0" />
			</div>
	</div>
</form>
</div>
<div class="content-container">
	<div class="label_row list_item_green" style="height:30px;"><h3>SUMMARY</h3></div>
	<div class="label_row">
		<div class="label_cell">&nbsp;</div>
		<div class="label_cell"><label>Team Members</label></div>
		<div class="label_cell"><label>Contractors</label></div>
		<div class="label_cell"><label>Total</label></div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>With Navigator</label></div>
		<div class="label_cell">${AdminReport.teamMemberNavigator}</div>
		<div class="label_cell">${AdminReport.contractorNavigator}</div>
		<div class="label_cell">${AdminReport.navigator}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>With Provisioner</label></div>
		<div class="label_cell">${AdminReport.teamMemberProvisioner}</div>
		<div class="label_cell">${AdminReport.contractorProvisioner}</div>
		<div class="label_cell">${AdminReport.provisioner}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>In Progress</label></div>
		<div class="label_cell">${AdminReport.teamMemberInProcess}</div>
		<div class="label_cell">${AdminReport.contractorInProcess}</div>
		<div class="label_cell">${AdminReport.inProcess}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>Completed</label></div>
		<div class="label_cell">${AdminReport.teamMemberCompleted}</div>
		<div class="label_cell">${AdminReport.contractorCompleted}</div>
		<div class="label_cell">${AdminReport.completed}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>Late</label></div>
		<div class="label_cell">${AdminReport.teamMemberLate}</div>
		<div class="label_cell">${AdminReport.contractorLate}</div>
		<div class="label_cell">${AdminReport.late}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>Back Dated</label></div>
		<div class="label_cell">${AdminReport.teamMemberBackDated}</div>
		<div class="label_cell">${AdminReport.contractorBackDated}</div>
		<div class="label_cell">${AdminReport.backDated}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>Rehire</label></div>
		<div class="label_cell">${AdminReport.teamMemberRehire}</div>
		<div class="label_cell">${AdminReport.contractorRehire}</div>
		<div class="label_cell">${AdminReport.rehire}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>C-E</label></div>
		<div class="label_cell">${AdminReport.teamMemberC_E}</div>
		<div class="label_cell">N/A</div>
		<div class="label_cell">${AdminReport.teamMemberC_E}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>Fieldglass / Term</label></div>
		<div class="label_cell">N/A</div>
		<div class="label_cell">${AdminReport.contractorFieldglass}</div>
		<div class="label_cell">${AdminReport.contractorFieldglass}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>VPN</label></div>
		<div class="label_cell">N/A</div>
		<div class="label_cell">${AdminReport.contractorVPN}</div>
		<div class="label_cell">${AdminReport.contractorVPN}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>Contractor Registration</label></div>
		<div class="label_cell">N/A</div>
		<div class="label_cell">${AdminReport.contractorRegistration}</div>
		<div class="label_cell">${AdminReport.contractorRegistration}</div>
	</div>
	<div class="label_row">
		<div class="label_cell"><label>Total</label></div>
		<div class="label_cell">${AdminReport.teamMemberTotal}</div>
		<div class="label_cell">${AdminReport.contractorTotal}</div>
		<div class="label_cell">${AdminReport.total}</div>
	</div>
</div>
