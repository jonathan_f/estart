<%@page language="java" import="java.util.*" isErrorPage="true"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page isErrorPage="true"%>
<html>
<head>
<title>Oops!</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/telus.ui.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/container-layout.css"
	type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/telus.ui.collapsible.css"
	type="text/css" />

<script src="${pageContext.request.contextPath}/js/jquery-1.6.2.min.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery-ui-1.8.16.custom.min.js"
	type="text/javascript"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/telus.ui.collapsible.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var errorTicketNumber = "${errorTicketNumber}";
		if (errorTicketNumber == "") {
			$(".td-label-error").hide();
		}
	});

	
	
</script>

<style type="text/css">
th {
	background-color: #ECE7F0;
	font-size: 12px;
	text-align: left;
	color: #333;
}

.td-label {
	font-weight: bold;
}

.td-label-error {
	font-weight: bold;
}
</style>
</head>

<body>
	<h3>We're Sorry</h3>
	<p>
		The application has encountered an error. To help us diagnose and fix
		the problem, you can send us a crash report at <a
			href="mailto:dlCSOInnovation@telus.com">CSO Innovation </a> <br />
	</p>
	<p>
		This eSTART application is for Corprate Security team only. If you are looking for e.START tool, please use the go link go/e-start.
	</p>
	<form action="">
		<table width="650">
			<tr>
				<td class="td-label">URL:</td>
				<td>${pageContext.errorData.requestURI}</td>
			</tr>
			<tr>
				<td class="td-label">Error Code:</td>
				<td>${pageContext.errorData.statusCode}</td>
			</tr>
			<c:if test="${not empty errorTicketNumber}">
				<tr>
					<td class="td-label-error">Error Log Ticket Number:</td>
					<td>${errorTicketNumber}</td>
				</tr>
			</c:if>
		</table>
	</form>
	<div style="display:none;">
	<p class="error">An error occured: <c:out value="${exception}"/></p><br/>
	<c:out value="${exception.message}"/>
	<pre>
	<c:forEach var="stackTraceElem" items="${exception.stackTrace}">
	<c:out value="${stackTraceElem}"/><br/>
	</c:forEach>
	</pre>
	</div>
</body>
</html>
