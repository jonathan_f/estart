package com.telus.estart.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.ObjectNotFoundException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.telus.estart.controller.exception.FileUploadException;
import com.telus.estart.dao.DAOService;
import com.telus.estart.dao.interfaces.ICATSSqlmap;
import com.telus.estart.dao.interfaces.ILocalEmployeeRecordSqlmap;
import com.telus.estart.dao.interfaces.ITeamMemberSqlmap;
import com.telus.estart.domain.Address;
import com.telus.estart.domain.AdminReport;
import com.telus.estart.domain.EStartConstants;
import com.telus.estart.domain.EStartEmailContent;
import com.telus.estart.domain.LocalEmployeeRecord;
import com.telus.estart.domain.LocalEmployeeRecordComment;
import com.telus.estart.domain.LocalEmployeeRecordStatus;
import com.telus.estart.domain.MailroomLocation;
import com.telus.estart.domain.TeamMember;
import com.telus.framework.dbobject.ITeamMember;
import com.telus.sddi.jEMAClibrary.EmailQueue;
import com.telus.sddi.jEMAClibrary.eMACparameters;

@Controller
@RequestMapping({ "/" })
public class EStartMainController {

	
	private @Autowired HttpServletRequest request;

	private static final Log log = LogFactory.getLog(EStartMainController.class);
	private static final String TEAM_MEMBER_LIST_REPORT = "TeamMemberList";
	private static final String TEAM_MEMBER_LIST_FUTURE = "TeamMemberListFuture";
	private static final String TEAM_MEMBER_LIST_LASTWEEK = "TeamMemberListLastWeek";
	private static final String CONTRACTOR_LIST_LASTWEEK_TO_FUTURE = "ContractorListLastWeekToFuture";
	private static final String USER_NAME = "UserName";
	private static final String ADMIN_REPORT = "AdminReport";
	private static final String USER_SYSTEM = "System";
	private static final String EMPLOYEE_TYPE = "EmployeeType";
	private static final String MAILROOM_LOCATIONS = "MailroomLocations";
	private static final int FILE_UPLOAD_MAX_SIZE = 1048576;
	
	private static boolean eMacInitialized = false;
	// TODO: Change emailNotificationOn to true for production release, otherwise false for testing
	private static boolean emailNotificationOn = false;

	@RequestMapping
	public ModelAndView displayMainPage() throws ObjectNotFoundException,
			SQLException, ParseException {
		ModelAndView mv = new ModelAndView("index");
		List<String> incompleteRecordIDs = DAOService.getLocalEmployeeRecordSqlmap(request).getIncompleteEmployeeRecords();
		List<TeamMember> list = DAOService.getTeamMemberSqlMap(this.request)
				.getTeamMembersStartedLastWeek(incompleteRecordIDs);
		list.addAll(DAOService.getTeamMemberSqlMap(this.request)
				.getNewTeamMembers());
		//for the team members without local record, create with records with new status
		mv.addObject(TEAM_MEMBER_LIST_REPORT, getLocalRecords(list, EStartConstants.EMPLOYEE_TYPE_FULL_TIME));
		mv.addObject(MAILROOM_LOCATIONS, DAOService.getLocalEmployeeRecordSqlmap(request).getMailRoomLocations());
		//System.out.println(DAOService.getLocalEmployeeRecordSqlmap(request).getMailRoomLocations());
		
		mv.addObject(USER_NAME, getUserName(this.request));
		mv.addObject(EMPLOYEE_TYPE, EStartConstants.EMPLOYEE_TYPE_FULL_TIME);
		return mv;
	}
	
	@RequestMapping({ "/contractors" })
	public ModelAndView displayContractors() throws ObjectNotFoundException,
			SQLException, ParseException {
		ModelAndView mv = new ModelAndView("index");
		
		List<String> incompleteContractorIDs = DAOService.getLocalEmployeeRecordSqlmap(request).getIncompleteContractorRecords();
		//System.out.println("incompleteContractorIDs");
		List<TeamMember> list = DAOService.getTeamMemberSqlMap(this.request).getContractorsFromLastWeekToFuture(incompleteContractorIDs);
		//System.out.println("GetRecords From eSAM");
		generateSAPIDFromNumericalPID(list);
		//for the team members without local record, create with records with new status
		mv.addObject(TEAM_MEMBER_LIST_REPORT, getLocalRecords(list, EStartConstants.EMPLOYEE_TYPE_CONTRACTOR));
		mv.addObject(MAILROOM_LOCATIONS, DAOService.getLocalEmployeeRecordSqlmap(request).getMailRoomLocations());
		
		//System.out.println("Get/Update Records From local DB");
		mv.addObject(USER_NAME, getUserName(this.request));
		mv.addObject(EMPLOYEE_TYPE, EStartConstants.EMPLOYEE_TYPE_CONTRACTOR);
		return mv;
	}
	
	@RequestMapping({ "/report" })
	public ModelAndView displayAllReports(@RequestParam(value = "startDate", defaultValue = "") String startDateString,
			@RequestParam(value = "endDate", defaultValue = "") String endDateString,
			@RequestParam(value = "excelMode", defaultValue = "0") int excelMode) throws ObjectNotFoundException,
			SQLException, ParseException {
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date startDate, endDate;
		if (startDateString.equals("") || endDateString.equals("")) {
			Date today = new Date();
			endDate = today;
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_MONTH, -30);
			startDate = cal.getTime();//30 days ago
		} else {
			startDate = df.parse(startDateString);
			endDate = df.parse(endDateString);
		}
		
		List<LocalEmployeeRecord> list = DAOService.getLocalEmployeeRecordSqlmap(request).getEmployeeRecordsByDate(startDate, endDate);
		AdminReport aReport = new AdminReport(list);
		ModelAndView mv;
		
		if (excelMode == 1) {
			mv = new ModelAndView("AdminExcelReport");
			mv.addObject(TEAM_MEMBER_LIST_REPORT, list);
		} else {
			mv = new ModelAndView("report");
		}
		mv.addObject(ADMIN_REPORT, aReport);
		mv.addObject("startDate", df.format(startDate));
		mv.addObject("endDate", df.format(endDate));
		mv.addObject(USER_NAME, getUserName(this.request));
		return mv;
	}
	
	@RequestMapping({ "/excel" })
	public ModelAndView displayExcelReportView() throws ObjectNotFoundException,
			SQLException {
		ModelAndView mv = new ModelAndView("ExcelReport");
		List<String> incompleteRecordIDs = DAOService.getLocalEmployeeRecordSqlmap(request).getIncompleteEmployeeRecords();
		List<TeamMember> list1 = DAOService.getTeamMemberSqlMap(this.request).getTeamMembersStartedLastWeek(incompleteRecordIDs);
		mv.addObject(TEAM_MEMBER_LIST_LASTWEEK, list1);
		List<TeamMember> list2 = DAOService.getTeamMemberSqlMap(this.request).getNewTeamMembers();
		mv.addObject(TEAM_MEMBER_LIST_FUTURE, list2);
		
		List<String> incompleteContractorIDs = DAOService.getLocalEmployeeRecordSqlmap(request).getIncompleteContractorRecords();
		List<TeamMember> list3 = DAOService.getTeamMemberSqlMap(this.request).getContractorsFromLastWeekToFuture(incompleteContractorIDs);
		mv.addObject(CONTRACTOR_LIST_LASTWEEK_TO_FUTURE, list3);
		return mv;
	}
	
	@RequestMapping({ "/excel_card_sent_today" })
	public ModelAndView displayExcelCardSentTodayView() throws ObjectNotFoundException,
			SQLException {
		ModelAndView mv = new ModelAndView("ExcelCardSentTodayToMailRoomView");
		List<String> cardSentTodayRecordIDs = DAOService.getLocalEmployeeRecordSqlmap(request).getCardSentRecordsToday();
		ITeamMemberSqlmap tSqlMap = DAOService.getTeamMemberSqlMap(request);
		List<TeamMember> list = new LinkedList<TeamMember>();
		for (String s : cardSentTodayRecordIDs) {
			if (s.length() < 1) continue;
			if (s.charAt(0) == 'T') {
				list.add(tSqlMap.getTeamMember(s));
			} else if (s.charAt(0) == 'X') {
				TeamMember t = tSqlMap.getContractor(s.substring(1));
				t.setSAPloginID(s);
				list.add(t);
			}
		}
		
		mv.addObject("teamMemberList", list);
		return mv;
	}
	
	@RequestMapping({ "/assigned_to_me_excel" })
	public ModelAndView displayExcelAssignedToMeView() throws ObjectNotFoundException,
			SQLException, IOException {
		ModelAndView mv = new ModelAndView("ExcelAssignedToMeView");		
		List<String> assignedToMeRecordIDs = DAOService.getLocalEmployeeRecordSqlmap(request).getAssignedToMeRecordIDs(getUserName(request));
		ITeamMemberSqlmap tSqlMap = DAOService.getTeamMemberSqlMap(request);
		ILocalEmployeeRecordSqlmap localEmployeeSqlMap = DAOService.getLocalEmployeeRecordSqlmap(request);
		
		List<TeamMember> list = new LinkedList<TeamMember>();
		List<String> mailroomIDs = new LinkedList<String>();
		List<String> courierIDs = new LinkedList<String>();
		for (String s : assignedToMeRecordIDs) {
			if (s.length() < 1) continue;
			if (s.charAt(0) == 'T') {
				list.add(tSqlMap.getTeamMember(s));
			} else if (s.charAt(0) == 'X') {
				TeamMember t = tSqlMap.getContractor(s.substring(1));
				t.setSAPloginID(s);
				list.add(t);
			}
		}
		
		mv.addObject("assignedToMe", list);
		return mv;
	}
	
	@RequestMapping({ "/shippingexcel/{tid}" })
	public ModelAndView displayCourierShippingExcelFormView(@PathVariable String tid) throws ObjectNotFoundException,
			SQLException, IOException {
		ModelAndView mv = new ModelAndView("ShippingCourierExcel");
		LocalEmployeeRecord record = DAOService.getLocalEmployeeRecordSqlmap(request).getEmployeeRecord(tid);
		//System.out.println(record);
		TeamMember toTeamMember = null;
		if(record.getType() == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
			toTeamMember = DAOService.getTeamMemberSqlMap(this.request).getTeamMember(tid);
		} else {
			//System.out.println("get xid: " + record.getTid() + " id:" + record.getNumericalPID());
			toTeamMember = DAOService.getTeamMemberSqlMap(this.request).getContractor(record.getNumericalPID());
			toTeamMember.setSAPloginID(tid);//important! Contractors may not have a xid ready when queried
		}
		//retrive address enterred by agent
		toTeamMember.setAddress(DAOService.getLocalEmployeeRecordSqlmap(request).getShippingCourierAddressByID(tid));
		mv.addObject("toTeamMember", toTeamMember);
		
		// set from Team Member
		//X159155
		//"T023781"
		TeamMember fromTeamMember = DAOService.getTeamMemberSqlMap(this.request).getTeamMemberFromCED(getUserEmail(request));
		mv.addObject("fromTeamMember", fromTeamMember);
		
		return mv;
	}
	
	@RequestMapping({ "/mailroomlabel/{tid}" })
	public ModelAndView displayShippingMailRoomLabelView(@PathVariable String tid) throws ObjectNotFoundException,
			SQLException, IOException {
		ModelAndView mv = new ModelAndView("ShippingMailRoomLabel");
		LocalEmployeeRecord record = DAOService.getLocalEmployeeRecordSqlmap(request).getEmployeeRecord(tid);
		//System.out.println(record);
		TeamMember toTeamMember = null;
		if(record.getType() == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
			toTeamMember = DAOService.getTeamMemberSqlMap(this.request).getTeamMember(tid);
		} else {
			//System.out.println("get xid: " + record.getTid() + " id:" + record.getNumericalPID());
			toTeamMember = DAOService.getTeamMemberSqlMap(this.request).getContractor(record.getNumericalPID());
			toTeamMember.setSAPloginID(tid);//important! Contractors may not have a xid ready when queried
		}
		//retrive address enterred by agent
		toTeamMember.setHouseNoStreet(DAOService.getLocalEmployeeRecordSqlmap(request).getShippingMailRoomAddress(tid));
		mv.addObject("toTeamMember", toTeamMember);
		
		// set from Team Member
		TeamMember fromTeamMember = DAOService.getTeamMemberSqlMap(this.request).getTeamMemberFromCED(getUserEmail(request));
		mv.addObject("fromTeamMember", fromTeamMember);
		
		return mv;
	}
	
	@RequestMapping({ "/comment/" })
	public @ResponseBody String actionAddComment(@RequestParam("content") String content, @RequestParam("tid") String tid)
			throws ObjectNotFoundException, SQLException {
		saveComment(EStartConstants.COMMENT_USER_INPUT, tid, content);
		
		return getTeamMemberInfo(tid);
	}
	
	private void saveComment(int type, String tid, String content) throws ObjectNotFoundException, SQLException {
		LocalEmployeeRecordComment comment = new LocalEmployeeRecordComment();
		String currentUserID = getUserName(request);
		comment.setCreatedBy(currentUserID);
		comment.setTid(tid);
		comment.setType(type);
		comment.setComment(content);
		DAOService.getLocalEmployeeRecordSqlmap(request).addLocalEmployeeRecordComment(comment);
		log.info("Add new comment for " + tid + " by " + currentUserID);
	}
	
	
	
	@RequestMapping({ "/escalate" })
	public @ResponseBody String actionEscalate(@RequestParam("tid") String tid, @RequestParam("type") int type)
			throws ObjectNotFoundException, SQLException {
		//System.out.println("Escalate " + tid + " type: " + type);
		ILocalEmployeeRecordSqlmap e = DAOService.getLocalEmployeeRecordSqlmap(this.request);
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(tid,EStartConstants.STATUS_ESCALTED, getUserName(this.request)));
		LocalEmployeeRecord record = e.getEmployeeRecord(tid);
		
		if (record.getType() == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
			if (type == EStartConstants.ESCALATATION_EMPLOYEE_NO_PHOTO) {
				ITeamMemberSqlmap teamMemberSqlmap = DAOService.getTeamMemberSqlMap(request);
				TeamMember t = teamMemberSqlmap.getTeamMember(tid);
				saveComment(EStartConstants.COMMENT_ESCALATATION_HR, tid, "HR ESCALATION - Picture not submitted.");
				sendEmployeePhotoNotReceivedEmail(record.getManagerNumericalPID(), record.getFullName(), record.getTid(), t.getTaleoID());
			
			} else if (type == EStartConstants.ESCALATATION_EMPLOYEE_INVALID_PHOTO) {
				saveComment(EStartConstants.COMMENT_ESCALATATION_HR, tid, "HR ESCALATION - Picture does not match standards.");
				sendEmployeeResubmitPhotoEmail();
			}
		} else { //contactors
			if (type == EStartConstants.ESCALATATION_CONTRACTOR_NO_PHOTO) {
				saveComment(EStartConstants.COMMENT_ESCALATATION_MANAGER, tid, "Manager ESCALATION - Picture not submitted.");
				sendContractorPhotoNotReceivedEmail(record.getManagerNumericalPID(), record.getManagerEmail(), record.getFullName());
			
			} else if (type == EStartConstants.ESCALATATION_CONTRACTOR_INVALID_PHOTO) {
				saveComment(EStartConstants.COMMENT_ESCALATATION_MANAGER, tid, "Manager ESCALATION - Picture does not match standards.");
				sendContractorResubmitPhotoEmail(record.getManagerNumericalPID(), record.getManagerEmail(), record.getFullName());
			
			} else if (type == EStartConstants.ESCALATATION_CONTRACTOR_ACCESS_REQUEST) {
				saveComment(EStartConstants.COMMENT_ESCALATATION_MANAGER, tid, "Manager ESCALATION - missing access request.");
				sendContractorSubmitNewAccessRequestEmail(record.getManagerNumericalPID(), record.getManagerEmail(), record.getFullName(), record.geteHireDate());
			
			} else if (type == EStartConstants.ESCALATATION_CONTRACTOR_CLEARANCE_RECORD) {
				saveComment(EStartConstants.COMMENT_ESCALATATION_MANAGER, tid, "Manager ESCALATION - missing clearance record.");
				sendContractorSubmitClearanceRecordEmail(record.getManagerNumericalPID(), record.getManagerEmail(), record.getFullName());
			}
		}
		log.info("Add new escalation for " + tid + " by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/late" })
	public @ResponseBody String actionLateReason(@RequestParam("tid") String tid, @RequestParam("type") int type,  @RequestParam("ehire_date") String eHireDateString, @RequestParam("signed_date") String signedDateString)
			throws ObjectNotFoundException, SQLException, ParseException {
		//System.out.println("Late " + tid + " type: " + type + " eHire: " + eHireDateString + "Offer Signed: " + signedDateString);
		
		SimpleDateFormat dfInput = new SimpleDateFormat("MM/dd/yyyy");
		Date eHireDate = dfInput.parse(eHireDateString);
		Date signedDate = dfInput.parse(signedDateString);
		SimpleDateFormat dfOutput = new SimpleDateFormat("yyyy-MM-dd");
		
		LocalEmployeeRecord record = new LocalEmployeeRecord();
		record.seteHireDate(dfOutput.format(eHireDate));
		record.setOfferSignedDate(dfOutput.format(signedDate));
		record.setTid(tid);
		record.setUpdatedBy(getUserName(request));
		DAOService.getLocalEmployeeRecordSqlmap(request).editEmployeeRecord(record);
		//Add status to history
		ILocalEmployeeRecordSqlmap e = DAOService.getLocalEmployeeRecordSqlmap(this.request);
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(tid,EStartConstants.STATUS_REPORTED_LATE, getUserName(request)));
		//save to comments
		if (type == EStartConstants.COMMENT_LATE_ORG_MGM) {
			saveComment(EStartConstants.COMMENT_LATE_ORG_MGM, tid, "LATE REASON - Organization Management.");
		} else if (type == EStartConstants.COMMENT_LATE_FINANCE) {
			saveComment(EStartConstants.COMMENT_LATE_FINANCE, tid, "LATE REASON - Finance.");
		} else if (type == EStartConstants.COMMENT_LATE_RECRUITMENT) {
			saveComment(EStartConstants.COMMENT_LATE_RECRUITMENT, tid, "LATE REASON - Recruitment.");
		} else if (type == EStartConstants.COMMENT_LATE_BUSINESS) {
			saveComment(EStartConstants.COMMENT_LATE_BUSINESS, tid, "LATE REASON - Business.");
		} else if (type == EStartConstants.COMMENT_LATE_TECHNICAL_ISSUE) {
			saveComment(EStartConstants.COMMENT_LATE_TECHNICAL_ISSUE, tid, "LATE REASON - Technical Issue.");
		} 
		log.info("Add new late reason for " + tid + " by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/set_hire_type" })
	public @ResponseBody String actionSetHireType(@RequestParam("tid") String tid, @RequestParam("type") int type)
			throws ObjectNotFoundException, SQLException {
		//updateRecord(tid, EStartConstants.STATUS_ASSIGNED);
		String hireType = "Normal";
		if (type == 2) { //Rehire
			hireType = "Rehire";
		} else if (type == 3) {//
			hireType = "C-E";
		}
		String comment = "Set hire type to " + hireType;
		saveComment(EStartConstants.COMMENT_USER_INPUT, tid, comment);
		
		ILocalEmployeeRecordSqlmap e = DAOService
				.getLocalEmployeeRecordSqlmap(this.request);
		LocalEmployeeRecord record = new LocalEmployeeRecord();
		record.setTid(tid);
		record.setUpdatedBy(getUserName(request));
		record.setHireType(hireType);
		e.editEmployeeRecord(record);
		
		log.info("Setting hire type of " + tid + " to " + hireType + " by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/assign/{tid}" })
	public @ResponseBody String actionAssignRecord(@PathVariable String tid)
			throws ObjectNotFoundException, SQLException {
		updateRecord(tid, EStartConstants.STATUS_ASSIGNED);
		log.info("Assigning " + tid + " to " + getUserName(request) + " by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/shipto" })
	public @ResponseBody String actionEnterShipToInfo(@RequestParam("tid") String tid, @RequestParam("type") int type,
			@RequestParam(value = "courier") int courier,
			@RequestParam("mailroom_location") String mailRoomLocation, @RequestParam("mailroom_value") String mailRoomValue, 
			@RequestParam(value = "street") String street, @RequestParam(value = "floor") String floor, @RequestParam(value = "city") String city, 
			@RequestParam(value = "province") String province, @RequestParam(value = "postal_code") String postalCode,
			@RequestParam(value = "company") String company, @RequestParam(value = "phone") String phone, @RequestParam(value = "cost_centre") String costCentre)
			throws ObjectNotFoundException, SQLException {
		ILocalEmployeeRecordSqlmap e = DAOService.getLocalEmployeeRecordSqlmap(this.request);
		
		LocalEmployeeRecord recordToSave = new LocalEmployeeRecord();
		recordToSave.setTid(tid);
		//recordToSave.setStatus(EStartConstants.STATUS_SHIP_TO_READY);
		recordToSave.setUpdatedBy(getUserName(request));
		recordToSave.setCourier(courier);
		e.editEmployeeRecord(recordToSave);
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(tid,EStartConstants.STATUS_SHIP_TO_READY, getUserName(this.request)));
		
		String address = "";
		String comment = "";
		String courierName = "";
		if (courier != 0) {
			address = street + ", " + city + ", " + province + ", " + postalCode;
			if (floor != null && !"".equals(floor)) {
				address = floor + ", " + address;
			}
			if (company != null && !"".equals(company)) {
				address = company + ", " + address;
			}
			if (courier == EStartConstants.SHIPPING_COURIER_DYNAMEX) {
				courierName = "Dynamex";
			} else if (courier == EStartConstants.SHIPPING_COURIER_FEDEX) {
				courierName = "Fedex";
			} else if (courier == EStartConstants.SHIPPING_COURIER_PUROLATOR) {
				courierName = "Purolator";
			}
			e.addShippingCourierAddress(new Address(tid, floor, street, city, province, postalCode, company, phone, costCentre, getUserName(this.request)));
			
		} else {
			//Mailroom
			address = mailRoomLocation;
			e.addShippingMailRoomAddress(new MailroomLocation(tid, Integer.parseInt(mailRoomValue), getUserName(this.request)));
		}
		
		LocalEmployeeRecord record = e.getEmployeeRecord(tid);
		
		if(type == 1) { //mail room
			comment = "Shipped to Mailroom at " + address;
		} else { //courier
			comment = "Shipped to " + address + " via " + courierName;
			if (phone != null && !"".equals(phone)) {
				comment += ", phone number: " + phone;
			}
			if (costCentre != null && !"".equals(costCentre)) {
				comment += ", Cost Centre: " + costCentre;
			}
		}
		
		//System.out.println(comment);
		saveComment(EStartConstants.COMMENT_USER_INPUT, tid, comment);
		log.info(tid + " add Status Ship To Ready by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/tracking_number" })
	public @ResponseBody String actionEnterTrackingNumber(@RequestParam("tid") String tid, @RequestParam("number") String trackingNumber)
			throws ObjectNotFoundException, SQLException {
		//System.out.println(tid + " type: " + type + " address: " + address +" courier: " + courier + " tracking number: " + trackingNumber);
		//updateRecord(tid, EStartConstants.STATUS_CARD_SENT);
		ILocalEmployeeRecordSqlmap e = DAOService.getLocalEmployeeRecordSqlmap(this.request);
		
		LocalEmployeeRecord recordToSave = new LocalEmployeeRecord();
		recordToSave.setTid(tid);
		recordToSave.setUpdatedBy(getUserName(request));
		recordToSave.setTrackingNumber(trackingNumber);
		e.editEmployeeRecord(recordToSave);
		String comment  = "Courier tracking number : " + trackingNumber;
		saveComment(EStartConstants.COMMENT_USER_INPUT, tid, comment);
		
		log.info("Entering courier tracking number for " + tid + "  by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/sent/{tid}" })
	public @ResponseBody String actionSentRecord(@PathVariable("tid") String tid)
			throws ObjectNotFoundException, SQLException {
		//System.out.println(tid + " type: " + type + " address: " + address +" courier: " + courier + " tracking number: " + trackingNumber);
		//updateRecord(tid, EStartConstants.STATUS_CARD_SENT);
		ILocalEmployeeRecordSqlmap e = DAOService.getLocalEmployeeRecordSqlmap(this.request);
		
		LocalEmployeeRecord recordToSave = new LocalEmployeeRecord();
		recordToSave.setTid(tid);
		recordToSave.setStatus(EStartConstants.STATUS_CARD_SENT);
		recordToSave.setCardSentDate("notNull");
		recordToSave.setUpdatedBy(getUserName(request));

		e.editEmployeeRecord(recordToSave);
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(tid,EStartConstants.STATUS_CARD_SENT, getUserName(this.request)));
		
		log.info(tid + " change Status to Card Sent by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/link_cats" })
	public @ResponseBody String actionsLinkCATSID(@RequestParam("tid") String tid, @RequestParam("cats_id") String catsID)
			throws ObjectNotFoundException, SQLException {
		//System.out.println(tid + " type: " + catsID);
		ILocalEmployeeRecordSqlmap e = DAOService.getLocalEmployeeRecordSqlmap(this.request);
		//e.editLocalEmployeeRecordCATSID(new LocalEmployeeRecord(tid, catsID, getUserName(this.request)));
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(tid,EStartConstants.STATUS_CATS_LINKED, getUserName(this.request)));
		ITeamMemberSqlmap tSqlMap = DAOService.getTeamMemberSqlMap(request);
		tSqlMap.updateESamCATSID(tid.substring(1), catsID);//numerical id
		//update CATS DB with xid
		ICATSSqlmap catsSqlMap = DAOService.getCATSSqlmap(request);
		
		catsSqlMap.updateCATSEmployeeID(tid.substring(1), catsID);//numerical id
		log.info("Linking CATS profile for " + tid + " by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/complete" })
	public @ResponseBody String actionsCompleteRecord(@RequestParam("tid") String tid, @RequestParam("comment") String comment)
			throws ObjectNotFoundException, SQLException {
		updateRecord(tid, EStartConstants.STATUS_COMPLETED);
		saveComment(EStartConstants.COMMENT_USER_INPUT, tid, comment);
		log.info(tid + " change Status to Completed by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/receive" })
	public @ResponseBody String actionsCardReceived(@RequestParam("tid") String tid, @RequestParam("receive_date") String dateString)
			throws ObjectNotFoundException, SQLException, ParseException {
		//System.out.println("tid: " + tid + " receiveDate : " + date);
		SimpleDateFormat dfInput = new SimpleDateFormat("MM/dd/yyyy");
		Date receivedDate = dfInput.parse(dateString);
		SimpleDateFormat dfOutput = new SimpleDateFormat("yyyy-MM-dd");
		String date = dfOutput.format(receivedDate);
		
		ILocalEmployeeRecordSqlmap e = DAOService
				.getLocalEmployeeRecordSqlmap(this.request);
		LocalEmployeeRecord record = new LocalEmployeeRecord();
		record.setTid(tid);
		record.setPictureReceivedDate(date);
		e.editEmployeeRecord(record);
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(tid,EStartConstants.STATUS_PICTURE_UPLOADED, getUserName(this.request)));
		
		//updateRecord(tid, EStartConstants.STATUS_COMPLETED);
		String comment = "Picture Received on " + date;
		saveComment(EStartConstants.COMMENT_USER_INPUT, tid, comment);
		log.info(tid + " marked as Picture Received by " + getUserName(request));
		return getTeamMemberInfo(tid);
	}
	
	@RequestMapping({ "/upload" })
	public String  actionsUploadPicture(@RequestParam("file") MultipartFile mpf, @RequestParam("tid") String tid)
			throws ObjectNotFoundException, SQLException, ServletException, FileUploadException, IOException {
		//MultipartHttpServletRequest mRequest = new MultipartHttpServletRequest(request);
		System.out.println(mpf.getOriginalFilename());
		System.out.println("File Length: " + mpf.getSize());
		
		if (mpf.getSize() > FILE_UPLOAD_MAX_SIZE) {
			throw new FileUploadException("File size exceed 1M.");
		}
		System.out.println("ID: " + tid + " File Type: " + mpf.getContentType());
		
		//System.out.println("File Name: " + mpf.getOriginalFilename());
		mpf.getInputStream().read();
		DAOService.uploadPictureToIDMAdapter(request, tid.substring(1), mpf.getBytes());
		System.out.println("Picture In adapter");
		ILocalEmployeeRecordSqlmap e = DAOService
				.getLocalEmployeeRecordSqlmap(this.request);
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(tid,EStartConstants.STATUS_PICTURE_UPLOADED, getUserName(this.request)));
		
		log.info(tid + " picture uploaded by " + getUserName(request));
		return "nothing";
	}	


	
	@RequestMapping(value={ "/team_member/{id}" }, produces={"application/json; charset=UTF-8"})
	/**
	 * Return logs in JSON format
	 * 
	 * @param request
	 *            An instance of HttpSerlvetRequest
	 * @return Response code for JQuery Datatable Object
	 * @throws SQLException
	 */
	public @ResponseBody String getTeamMemberInfo(@PathVariable String id) throws ObjectNotFoundException, SQLException {
		LocalEmployeeRecord record = DAOService.getLocalEmployeeRecordSqlmap(request).getEmployeeRecord(id);
		//System.out.println(record);
		TeamMember t = null;
		if(record.getType() == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
			t = DAOService.getTeamMemberSqlMap(this.request).getTeamMember(id);
		} else {
			//record.getIsVPNRequired();
			//System.out.println("get xid: " + record.getTid() + " id:" + record.getNumericalPID());
			t = DAOService.getTeamMemberSqlMap(this.request).getContractor(record.getNumericalPID());
			t.setSAPloginID(id);//important! Contractors may not have a xid ready when queried
			if ("-1".equals(t.getCatsID())) { //CATS not linked
				//get potential CATS profile, using small xid [lowercase]
				t.setCatsRecords((DAOService.getLocalEmployeeRecordSqlmap(request).getCATSRecordsByID(id.toLowerCase())));
			}
		}
		t.setEmployeeRecord(record);
		t.setComments(DAOService.getLocalEmployeeRecordSqlmap(request).getLocalEmployeeRecordComments(id));
		t.setHistory(DAOService.getLocalEmployeeRecordSqlmap(request).getLocalEmployeeRecordStatusHistory(id));
		Gson gson = new Gson();
		return gson.toJson(t);
	}
	
	private void updateRecord(String id, int status) throws ObjectNotFoundException, SQLException {
		ILocalEmployeeRecordSqlmap e = DAOService
				.getLocalEmployeeRecordSqlmap(this.request);
		LocalEmployeeRecord record = new LocalEmployeeRecord(id,status, getUserName(this.request));
		if (status == 6) {//completed
			record.setCompleteDate("notNull");
		}
		e.editEmployeeRecord(record);
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(id,status, getUserName(this.request)));
	}
	
	private List<LocalEmployeeRecord> getLocalRecords(List<TeamMember> tlist, int employeeType) throws ObjectNotFoundException, SQLException, ParseException {
		Map<String, TeamMember> map = generateTeamMemberMap(tlist);
		ILocalEmployeeRecordSqlmap sqlMap = DAOService
				.getLocalEmployeeRecordSqlmap(this.request);
		//query all existing local records
		List<LocalEmployeeRecord> elist = sqlMap.getLocalEmployeeRecords(tlist);
		
		
			
		//set records for each team member, preparing to handle non-existing records
		for (LocalEmployeeRecord e : elist) {
			TeamMember t = (TeamMember) map.get(e.getTid());
			if (t != null) {
				t.setEmployeeRecord(e);
			}
			
			//send email confirmation if status = 6 completed and and emailConfirmation = false;
			/*
			if (e.getStatus() == EStartConstants.STATUS_COMPLETED && e.getEmailConfirmationSent() == false) {
				if (e.getType() == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
					sendEmployeeConfirmationOfDeliveryEmail(e.getManagerEmail(), e.getFullName());
				} else if (e.getType() != EStartConstants.EMPLOYEE_TYPE_CONTRACTOR_SYSTEMS_ACCESS_ONLY){
					//exclude building only contractors
					sendContractorConfirmationOfDeliveryEmail(e.getManagerEmail(), e.getFullName());
				}
				sqlMap.editEmployeeRecordEmailConfirmationSent(e.getTid());
			}
			*/
		}
		
		List<LocalEmployeeRecord> newRecordList = new LinkedList<LocalEmployeeRecord>();
		//create new records and set additional status for existing records
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		for( TeamMember t : map.values()) { //map.values = all new employee without duplicate records
			df = new SimpleDateFormat("yyyy-MM-dd");
			Date curDate = new Date();
			if(t.getHireRehireDate() == null) {
				//eSAM dev employee without hiredate
				//ignore
				elist.remove(t);
				continue;
			}
			Date hireDate = df.parse(t.getHireRehireDate());
			if (t.getEmployeeRecord() == null) {
				//System.out.println("Creating new record for " + t.getSAPloginID());
				createNewLocalRecord(newRecordList, t, employeeType, curDate, hireDate);
			} else {
				loadLocalRecord(elist, newRecordList, t, employeeType, curDate, hireDate);
			}
		}
		newRecordList.addAll(elist);//add all old record to the new list so that all new records are in the front
		
		//send out email notifications and change status to pending if the record is new
		for (LocalEmployeeRecord record : newRecordList) {
			if (record.getStatus() == EStartConstants.STATUS_NEW) {
				//send notifications
				if(record.getType() == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
					sendEmployeeSubmitNewAccessRequestEmail(record.getManagerNumericalPID(), record.getManagerEmail(), record.getFullName());
				} else {
					sendContractorSubmitNewAccessRequestEmail(record.getManagerNumericalPID(), record.getManagerEmail(), record.getFullName(), record.getHireDate());
				}
				
				//update status in the DB
				ILocalEmployeeRecordSqlmap e = DAOService
						.getLocalEmployeeRecordSqlmap(this.request);
				e.editEmployeeRecord(new LocalEmployeeRecord(record.getTid(),EStartConstants.STATUS_PENDING, USER_SYSTEM));
				//add status to history
				e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(record.getTid(),EStartConstants.STATUS_PENDING, USER_SYSTEM));
	
				record.setStatus(EStartConstants.STATUS_PENDING);
				record.setStatusName("Pending");
				record.setUpdatedBy(USER_SYSTEM);
				
				if (record.getType() == EStartConstants.EMPLOYEE_TYPE_CONTRACTOR_BUILDING_ACCESS_ONLY) {
					e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(record.getTid(),EStartConstants.STATUS_LDAP_NOT_REQUIRED, USER_SYSTEM));
					e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(record.getTid(),EStartConstants.STATUS_IDM_NOT_REQUIRED, USER_SYSTEM));
				}
			}
		}
		
		return newRecordList;
	}
	
	private void createNewLocalRecord(List<LocalEmployeeRecord> newRecordList, TeamMember t, int employeeType, Date curDate, Date hireDate) throws ObjectNotFoundException, SQLException {
		ILocalEmployeeRecordSqlmap e = DAOService.getLocalEmployeeRecordSqlmap(this.request);
		//create New Records If Not Exist
		int currentEmployeeType = 0;
		//Set employee type
		if(employeeType == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) { //regular full time
			currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_FULL_TIME;
		} else { //contractors
			if(t.getBuildingAccess() == true && t.getSystemsAccess() == false) {
				currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_CONTRACTOR_BUILDING_ACCESS_ONLY;
			} else if (t.getBuildingAccess() == false && t.getSystemsAccess() == true) {
				currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_CONTRACTOR_SYSTEMS_ACCESS_ONLY;
			} else {
				currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_CONTRACTOR;
			}
		}
		
		String fullname = "";
		
		if (t.getPreferredFirstName() == null || t.getPreferredFirstName().isEmpty()) {
			fullname = t.getFirstName() + ' ' + t.getLastName();
		} else {
			fullname = t.getPreferredFirstName() + ' ' + t.getLastName();
		}
		
		//set new status
		LocalEmployeeRecord record = new LocalEmployeeRecord(t.getSAPloginID(), t.getNumericalPID(),
				fullname, currentEmployeeType, EStartConstants.STATUS_NEW, "New",
				t.getHireRehireDate(), USER_SYSTEM);
		record.setManagerEmail(t.getManagerEmail());//for notifications
		record.setManagerNumericalPID(t.getManagerPersonnelNumber());
		record.setHireType("Normal");
		
		if (employeeType != EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
			//set CATS id for contractors
			record.setCatsID(t.getCatsID());
			record.setTags(t.getTags());
		}

		//create employee record in database
		e.addLocalEmployeeRecord(record);
		//add status to employee record status history in database
		if(curDate.after(hireDate)) {
			record.setBackTracked(true);
			record.setLate(true);
			
			e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(t.getSAPloginID(),
					EStartConstants.STATUS_BACK_DATED, USER_SYSTEM));
		}
		
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(t.getSAPloginID(),
				EStartConstants.STATUS_NEW, USER_SYSTEM));
		
		//add vpn required status in history
		if (t.getTags() != null && t.getTags().contains("vpn")) {
		e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(t
				.getSAPloginID(), EStartConstants.STATUS_VPN_REQUIRED, USER_SYSTEM));
		}
		
		newRecordList.add(record);
	}
	
	private void loadLocalRecord(List<LocalEmployeeRecord> elist,
			List<LocalEmployeeRecord> newRecordList, TeamMember t,
			int employeeType, Date curDate, Date hireDate)
			throws ObjectNotFoundException, SQLException, ParseException {
		ILocalEmployeeRecordSqlmap e = DAOService
				.getLocalEmployeeRecordSqlmap(this.request);
		
		LocalEmployeeRecord record = t.getEmployeeRecord();
		if (employeeType != EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
			//set CATS id for contractors
			record.setCatsID(t.getCatsID());
		}
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date createDate = df.parse(record.getCreateDate());
		Date localHireDate = df.parse(record.getHireDate());

		if (localHireDate.compareTo(hireDate) != 0) {
			// a potiential rehire is found, deactivate all previous records
			e.deactivatePreviousEmployeeRecord(record.getTid());
			elist.remove(record);

			// refresh & create a new record
			int currentEmployeeType = 0;
			// Set employee type
			if (employeeType == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) { 
				// regular full time
				currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_FULL_TIME;
			} else { // contractors
				if (t.getBuildingAccess() == true
						&& t.getSystemsAccess() == false) {
					currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_CONTRACTOR_BUILDING_ACCESS_ONLY;
				} else if (t.getBuildingAccess() == false
						&& t.getSystemsAccess() == true) {
					currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_CONTRACTOR_SYSTEMS_ACCESS_ONLY;
				} else {
					currentEmployeeType = EStartConstants.EMPLOYEE_TYPE_CONTRACTOR;
				}
			}
			// set new status
			record = new LocalEmployeeRecord(t.getSAPloginID(),
					t.getNumericalPID(), t.getPreferredFirstName() + ' '
							+ t.getLastName(), currentEmployeeType,
					EStartConstants.STATUS_NEW, "New", t.getHireRehireDate(),
					USER_SYSTEM);
			record.setManagerEmail(t.getManagerEmail());//for notifications
			newRecordList.add(record);

			// create employee record in database
			e.removeLocalEmployeeRecord(record.getTid());
			e.addLocalEmployeeRecord(record);
			
			// add status to employee record status history in database
			if (curDate.after(hireDate)) {
				record.setBackTracked(true);
				record.setLate(true);
				e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(t
						.getSAPloginID(), EStartConstants.STATUS_BACK_DATED,
						USER_SYSTEM));
			}

			e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(t
					.getSAPloginID(), EStartConstants.STATUS_NEW, USER_SYSTEM));
			//add vpn required status in history
			if (t.getTags() != null && t.getTags().contains("vpn")) {
			e.addLocalEmployeeRecordStatusToHistory(new LocalEmployeeRecordStatus(t
					.getSAPloginID(), EStartConstants.STATUS_VPN_REQUIRED, USER_SYSTEM));
			}
		}

		Date completeDate = null;
		if (record.getCompleteDate() != null) {
			completeDate = df.parse(record.getCompleteDate());
		}

		if (!createDate.before(hireDate)) {
			record.setBackTracked(true);
		}
		if ((completeDate == null && curDate.after(hireDate))
				|| (completeDate != null && completeDate.after(hireDate))) {
			record.setLate(true);
		}
		
	}

	private Map<String, TeamMember> generateTeamMemberMap(List<TeamMember> list) {
		Map<String, TeamMember> map = new HashMap<String, TeamMember>();
		for (TeamMember t : list) {
			map.put(t.getSAPloginID(), t);
		}
		return map;
	}

	private String getUserName(HttpServletRequest request) throws ObjectNotFoundException, SQLException {
		String uid = (String) request.getSession().getAttribute("customSSOFilter_email");
		ITeamMember tm = DAOService.getTeamMemberDao(request).getTeamMemberByEmail(uid);
		return tm.getPreferredFirstName() + " " + tm.getLastName();
	}
	
	private String getUserEmail(HttpServletRequest request) {
		String uid = (String) request.getSession().getAttribute("customSSOFilter_email");
		return uid;
	}
	
	private void generateSAPIDFromNumericalPID(List<TeamMember> list) {
		for (TeamMember t : list) {
			t.setSAPloginID(generateSAPIDFromNumericalPID(t.getNumericalPID()));
			//System.out.println("setTID: " + t.getSAPloginID());
		}
	}
	
	private String generateSAPIDFromNumericalPID(String numericalPID) {
		int length = numericalPID.length();
		String result = "X";
		while (length < 6) {
			result += '0';
			length++;
		}
		return result + numericalPID;
	}
	
	/**
	 * Employee Emails
	 */
	public void sendEmployeeSubmitNewAccessRequestEmail(String numericalID, String toEmail, String name) {
		String subjectEnglish = String.format(EStartEmailContent.EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_SUBJECT_EN, name);
		String subjectFrench = String.format(EStartEmailContent.EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_SUBJECT_FR, name);
		String emailContentsEnglish = String.format(EStartEmailContent.EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_BODY_EN, name);
		String emailContentsFrench = String.format(EStartEmailContent.EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_BODY_FR, name);
		sendEMacEmail(numericalID, toEmail, subjectEnglish, subjectFrench, emailContentsEnglish, emailContentsFrench);
		

	}
	
	public void sendEmployeeResubmitPhotoEmail() {
		String toEmail = "project.tulip@telus.com"; //HR email
		String subjectEnglish = String.format(EStartEmailContent.EMPLOYEE_RESUBMIT_PHOTO_SUBJECT_EN);
		String subjectFrench = String.format(EStartEmailContent.EMPLOYEE_RESUBMIT_PHOTO_SUBJECT_FR);
		String emailContentsEnglish = String.format(EStartEmailContent.EMPLOYEE_RESUBMIT_PHOTO_BODY_EN);
		String emailContentsFrench = String.format(EStartEmailContent.EMPLOYEE_RESUBMIT_PHOTO_BODY_FR);
		sendEMacEmail(null, toEmail, subjectEnglish, subjectFrench, emailContentsEnglish, emailContentsFrench);
	}
	
	public void sendEmployeePhotoNotReceivedEmail(String numericalID, String name, String tid, String taleoId) {
		String toEmail = "project.tulip@telus.com";//HR email
		String subjectEnglish = String.format(EStartEmailContent.EMPLOYEE_PHOTO_NOT_RECEIVED_SUBJECT_EN, name, tid, taleoId);
		String subjectFrench = String.format(EStartEmailContent.EMPLOYEE_PHOTO_NOT_RECEIVED_SUBJECT_FR, name, tid, taleoId);
		String emailContentsEnglish = String.format(EStartEmailContent.EMPLOYEE_PHOTO_NOT_RECEIVED_BODY_EN, name, tid, taleoId);
		String emailContentsFrench = String.format(EStartEmailContent.EMPLOYEE_PHOTO_NOT_RECEIVED_BODY_FR, name, tid, taleoId);
		sendEMacEmail(numericalID, toEmail, subjectEnglish, subjectFrench, emailContentsEnglish, emailContentsFrench);
	}

	/**
	 * Contractor Emails
	 */
	public void sendContractorSubmitNewAccessRequestEmail(String numericalID, String toEmail, String name, String hireDate) {
		String subjectEnglish = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_SUBJECT_EN, name);
		String subjectFrench = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_SUBJECT_FR, name);
		String emailContentsEnglish = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_BODY_EN, name, hireDate, name);
		String emailContentsFrench = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_BODY_FR, name, hireDate, name);
		sendEMacEmail(numericalID, toEmail, subjectEnglish, subjectFrench, emailContentsEnglish, emailContentsFrench);
	}
	

	public void sendContractorResubmitPhotoEmail(String numericalID, String toEmail, String name) {
		String subjectEnglish = String.format(EStartEmailContent.CONTRACTOR_RESUBMIT_PHOTO_SUBJECT_EN, name);
		String subjectFrench = String.format(EStartEmailContent.CONTRACTOR_RESUBMIT_PHOTO_SUBJECT_FR, name);
		String emailContentsEnglish = String.format(EStartEmailContent.CONTRACTOR_RESUBMIT_PHOTO_BODY_EN, name);
		String emailContentsFrench = String.format(EStartEmailContent.CONTRACTOR_RESUBMIT_PHOTO_BODY_FR, name);
		sendEMacEmail(numericalID, toEmail, subjectEnglish, subjectFrench, emailContentsEnglish, emailContentsFrench);
	}
	
	public void sendContractorPhotoNotReceivedEmail(String numericalID, String toEmail, String name) {
		String subjectEnglish = String.format(EStartEmailContent.CONTRACTOR_SUBMIT_PHOTO_SUBJECT_EN, name);
		String subjectFrench = String.format(EStartEmailContent.CONTRACTOR_SUBMIT_PHOTO_SUBJECT_FR, name);
		String emailContentsEnglish = String.format(EStartEmailContent.CONTRACTOR_SUBMIT_PHOTO_BODY_EN, name);
		String emailContentsFrench = String.format(EStartEmailContent.CONTRACTOR_SUBMIT_PHOTO_BODY_FR, name);
		sendEMacEmail(numericalID, toEmail, subjectEnglish, subjectFrench, emailContentsEnglish, emailContentsFrench);
	}
	
	public void sendContractorSubmitClearanceRecordEmail(String numericalID, String toEmail, String name) {
		String subjectEnglish = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_SUBJECT_EN, name);
		String subjectFrench = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_SUBJECT_FR, name);
		String emailContentsEnglish = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_BODY_EN);
		String emailContentsFrench = String.format(EStartEmailContent.CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_BODY_FR);
		sendEMacEmail(numericalID, toEmail, subjectEnglish, subjectFrench, emailContentsEnglish, emailContentsFrench);
	}
	
	private void sendEMacEmail(String numericalID, String toEmail, String subjectEnglish, String subjectFrench, String emailContentsEnglish, String emailContentsFrench){
		//System.out.println("sending email to " + numericalID + ", tomail " + toEmail + " subject:" + subjectEnglish);
		if (!emailNotificationOn) {
			return;
		}
		
		

		if (eMacInitialized == false) {
			eMacInitialized = true;
			eMACparameters.initialize("prod",true);
		}
		
		if (numericalID == null) {
			return;//don't know who to send the email to
		} else {
			int id = Integer.parseInt(numericalID);
			// Create exception
			//String toEmailT = "kobe.lin@telus.com,Cindy.Terpstra@TELUS.COM,KRYS.DOMINIK@TELUS.COM,MARLA.DASKIS@TELUS.COM,FRANCOIS.BEGIN@TELUS.COM";
			String ccEmail = null;
			if ("".equals(toEmail)) {
				toEmail = null;
			}
			//EmailQueue myExceptionEmail = new EmailQueue(805959,false,toEmail,ccEmail,null,subjectEnglish,subjectFrench,emailContentsEnglish,emailContentsFrench,"eSTART Web");
			EmailQueue myExceptionEmail = new EmailQueue(id,false,toEmail,ccEmail,null,subjectEnglish,subjectFrench,emailContentsEnglish,emailContentsFrench,"eSTART Web");
	
			// Queue exception
			myExceptionEmail.createException(); 
		}
		log.info("Sending email to " + numericalID + " with subject " + subjectEnglish);
	}
}