package com.telus.estart.controller.exception;

@SuppressWarnings("serial")
public class FileUploadException extends Exception {

	public FileUploadException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FileUploadException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public FileUploadException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public FileUploadException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
