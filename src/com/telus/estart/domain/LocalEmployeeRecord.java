package com.telus.estart.domain;

import java.io.Serializable;
import java.util.Date;

public class LocalEmployeeRecord implements Serializable {
	private static final long serialVersionUID = -2807264086756206771L;
	private String tid;
	private String numericalPID;// required for contractors
	private int type;
	private Integer status;
	private String statusName;
	private String fullName;
	private String createDate;
	private String hireDate;
	private String hireType;
	private String completeDate;
	private String idmReadyDate;
	private String ldapReadyDate;
	private String pictureReceivedDate;
	private String pictureReadyDate;
	private String vpnReadyDate;
	private String cardSentDate;
	private String eHireDate;
	private String offerSignedDate;
	private String updatedBy;
	private String catsID;
	private Integer courier;
	private String trackingNumber;
	private String managerEmail;
	private String managerNumericalPID;
	private String tags;
	private boolean emailConfirmationSent = false;
	private boolean isBackDated = false;
	private boolean isLate = false;
	private boolean lateReasonOrgManagement = false;
	private boolean lateReasonFinance = false;
	private boolean lateReasonRecruitment = false;
	private boolean lateReasonBusiness = false;
	private boolean lateReasonSCC = false;
	private boolean lateReasonTechnical = false;

	public LocalEmployeeRecord() {
	}

	public LocalEmployeeRecord(String tid, String numericalPID,
			String fullName, int type, int status, String statusName,
			String hireDate, String updatedBy) {
		super();
		this.tid = tid;
		this.numericalPID = numericalPID;
		this.fullName = fullName;
		this.type = type;
		this.status = status;
		this.statusName = statusName;
		this.hireDate = hireDate;
		this.updatedBy = updatedBy;
	}

	// constructor for status update action
	public LocalEmployeeRecord(String tid, int status, String updatedBy) {
		super();
		this.tid = tid;
		this.status = status;
		this.updatedBy = updatedBy;
	}

	// constructor for creating new records
	public LocalEmployeeRecord(String tid, int status, int type,
			String hireDate, String upStringdBy) {
		super();
		this.tid = tid;
		this.status = status;
		this.type = type;
		this.hireDate = hireDate;
		this.updatedBy = updatedBy;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}

	public String getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getNumericalPID() {
		return numericalPID;
	}

	public void setNumericalPID(String numericalPID) {
		this.numericalPID = numericalPID;
	}

	public String geteHireDate() {
		return eHireDate;
	}

	public void seteHireDate(String eHireDate) {
		this.eHireDate = eHireDate;
	}

	public String getOfferSignedDate() {
		return offerSignedDate;
	}

	public void setOfferSignedDate(String offerSignedDate) {
		this.offerSignedDate = offerSignedDate;
	}

	public boolean getIsBackTracked() {
		return isBackDated;
	}

	public void setBackTracked(boolean isBackTracked) {
		this.isBackDated = isBackTracked;
	}

	public boolean getIsLate() {
		return isLate;
	}

	public void setLate(boolean isLate) {
		this.isLate = isLate;
	}
	

	public boolean isLateReasonOrgManagement() {
		return lateReasonOrgManagement;
	}

	public void setLateReasonOrgManagement(boolean lateReasonOrgManagement) {
		this.lateReasonOrgManagement = lateReasonOrgManagement;
	}

	public boolean isLateReasonFinance() {
		return lateReasonFinance;
	}

	public void setLateReasonFinance(boolean lateReasonFinance) {
		this.lateReasonFinance = lateReasonFinance;
	}

	public boolean isLateReasonRecruitment() {
		return lateReasonRecruitment;
	}

	public void setLateReasonRecruitment(boolean lateReasonRecruitment) {
		this.lateReasonRecruitment = lateReasonRecruitment;
	}

	public boolean isLateReasonBusiness() {
		return lateReasonBusiness;
	}

	public void setLateReasonBusiness(boolean lateReasonBusiness) {
		this.lateReasonBusiness = lateReasonBusiness;
	}
	
	public boolean isLateReasonSCC() {
		return lateReasonSCC;
	}

	public void setLateReasonSCC(boolean lateReasonSCC) {
		this.lateReasonSCC = lateReasonSCC;
	}
	
	public boolean isLateReasonTechnical() {
		return lateReasonTechnical;
	}

	public void setLateReasonTechnical(boolean lateReasonTechnical) {
		this.lateReasonTechnical = lateReasonTechnical;
	}
	

	public String getCatsID() {
		return catsID;
	}

	public void setCatsID(String catsID) {
		this.catsID = catsID;
	}

	public Integer getCourier() {
		return courier;
	}

	public void setCourier(int courier) {
		this.courier = courier;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}
	
	public boolean getEmailConfirmationSent() {
		return emailConfirmationSent;
	}

	public void setEmailConfirmationSent(boolean emailConfirmationSent) {
		this.emailConfirmationSent = emailConfirmationSent;
	}

	public String getManagerNumericalPID() {
		return managerNumericalPID;
	}

	public void setManagerNumericalPID(String managerNumericalPID) {
		if (managerNumericalPID == null || "".equals(managerNumericalPID)) {
			this.managerNumericalPID = null;
		} else {
			//System.out.println("Manager ID" + managerNumericalPID);
			int tmp = Integer.parseInt(managerNumericalPID);
			//System.out.println(tmp);
			this.managerNumericalPID = ""+tmp;
		}
	}

	public String getHireType() {
		return hireType;
	}

	public void setHireType(String hireType) {
		this.hireType = hireType;
	}

	public String getPictureReceivedDate() {
		return pictureReceivedDate;
	}

	public void setPictureReceivedDate(String pictureReceivedDate) {
		this.pictureReceivedDate = pictureReceivedDate;
	}

	public String getIdmReadyDate() {
		return idmReadyDate;
	}

	public void setIdmReadyDate(String idmReadyDate) {
		this.idmReadyDate = idmReadyDate;
	}

	public String getPictureReadyDate() {
		return pictureReadyDate;
	}

	public void setPictureReadyDate(String pictureReadyDate) {
		this.pictureReadyDate = pictureReadyDate;
	}

	public String getLdapReadyDate() {
		return ldapReadyDate;
	}

	public void setLdapReadyDate(String ldapReadyDate) {
		this.ldapReadyDate = ldapReadyDate;
	}

	public String getCardSentDate() {
		return cardSentDate;
	}

	public void setCardSentDate(String cardSentDate) {
		this.cardSentDate = cardSentDate;
	}

	public String getTags() {
		return tags;
	}
	
	public boolean getIsVPNRequired() {
		if (tags == null) {
			return false;
		}
		return tags.contains("vpn");
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getVpnReadyDate() {
		return vpnReadyDate;
	}

	public void setVpnReadyDate(String vpnReadyDate) {
		this.vpnReadyDate = vpnReadyDate;
	}
	
}