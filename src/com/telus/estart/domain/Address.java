package com.telus.estart.domain;

import java.io.Serializable;

public class Address implements Serializable {
	private static final long serialVersionUID = -2807264086756206771L;
	private String id;
	private String street;
	private String floor;
	private String city;
	private String province;
	private String postalCode;
	private String company;
	private String phone;
	private String costCentre;
	private String updatedBy;
	
	public Address() {
		
	}
	
	public Address(String id, String floor, String street, String city,
			String province, String postalCode, String company, String phone,
			String costCentre, String updatedBy) {
		super();
		this.id = id;
		this.street = street;
		this.floor = floor;
		this.city = city;
		this.province = province;
		this.postalCode = postalCode;
		this.company = company;
		this.phone = phone;
		this.costCentre = costCentre;
		this.updatedBy = updatedBy;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company; 
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getCostCenter() {
		return costCentre;
	}


	public void setCostCenter(String costCentre) {
		this.costCentre = costCentre;
	}
	
	
}
