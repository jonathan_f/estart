package com.telus.estart.domain;

import java.io.Serializable;
import java.util.List;

public class TeamMember implements Serializable {
	private static final long serialVersionUID = -2175117825824118678L;
	private String PersonnelNumber;
	private String SAPloginID;
	private String FirstName;
	private String LastName;
	private String PreferredFirstName;
	private String CommunicationLanguage;
	private String TELUSemailAddress;
	private String TaleoID;
	private String HireRehireDate;
	private String ServiceAnniversaryDate;
	private String EmployeeGroup;
	private String EmployeeGroupDescription;
	private String EmployeeSubGroup;
	private String EmployeeSubgroupDescription;
	private String PersonnelSubarea;
	private String PersonnelSubareaDescription;
	private String CompanyCode;
	private String PositionCode;
	private String PositionDescription;
	private String JobCode;
	private String OrganizationUnit;
	private String OrganizationUnitDescription;
	private String CostCenter;
	private String CLLI;
	private String CLLIdescription;
	private String Floor;
	private String HouseNoStreet;
	private String City;
	private String Province;
	private String PostalCode;
	private String PhoneNumber;
	private String JobDescription;
	private String WorkStyle;
	private String BusinessIdentifier;
	private String OrgHierarchyCodeDescription;
	private String ManagerPersonnelNumber;
	private String ManagerFirstName;
	private String ManagerLastName;
	private String ManagerEmail;
	private String ManagerPhoneNumber;
	private String NumericalPID;
	private String ContractorSAP;
	private String hireType;
	private Boolean buildingAccess;
	private Boolean systemsAccess;
	private String catsID;
	private String tags;
	private LocalEmployeeRecord employeeRecord;
	private List<LocalEmployeeRecordStatus> history;
	private List<LocalEmployeeRecordComment> comments;
	private List<CATSRecord> catsRecords;

	public String getPersonnelNumber() {
		return this.PersonnelNumber;
	}

	public void setPersonnelNumber(String personnelNumber) {
		this.PersonnelNumber = personnelNumber;
	}

	public String getSAPloginID() {
		return this.SAPloginID;
	}

	public void setSAPloginID(String sAPloginID) {
		this.SAPloginID = sAPloginID;
	}

	public String getFirstName() {
		return this.FirstName;
	}

	public void setFirstName(String firstName) {
		this.FirstName = firstName;
	}

	public String getLastName() {
		return this.LastName;
	}

	public void setLastName(String lastName) {
		this.LastName = lastName;
	}

	public String getPreferredFirstName() {
		return this.PreferredFirstName;
	}

	public void setPreferredFirstName(String preferredFirstName) {
		this.PreferredFirstName = preferredFirstName;
	}

	public String getCommunicationLanguage() {
		return this.CommunicationLanguage;
	}

	public void setCommunicationLanguage(String communicationLanguage) {
		this.CommunicationLanguage = communicationLanguage;
	}

	public String getTELUSemailAddress() {
		return this.TELUSemailAddress;
	}

	public void setTELUSemailAddress(String tELUSemailAddress) {
		this.TELUSemailAddress = tELUSemailAddress;
	}

	public String getTaleoID() {
		return this.TaleoID;
	}

	public void setTaleoID(String taleoID) {
		this.TaleoID = taleoID;
	}

	public String getHireRehireDate() {
		return this.HireRehireDate;
	}

	public void setHireRehireDate(String hireRehireDate) {
		this.HireRehireDate = hireRehireDate;
	}

	public String getServiceAnniversaryDate() {
		return this.ServiceAnniversaryDate;
	}

	public void setServiceAnniversaryDate(String serviceAnniversaryDate) {
		this.ServiceAnniversaryDate = serviceAnniversaryDate;
	}

	public String getEmployeeGroup() {
		return this.EmployeeGroup;
	}

	public void setEmployeeGroup(String employeeGroup) {
		this.EmployeeGroup = employeeGroup;
	}

	public String getEmployeeGroupDescription() {
		return this.EmployeeGroupDescription;
	}

	public void setEmployeeGroupDescription(String employeeGroupDescription) {
		this.EmployeeGroupDescription = employeeGroupDescription;
	}

	public String getEmployeeSubGroup() {
		return this.EmployeeSubGroup;
	}

	public void setEmployeeSubGroup(String employeeSubGroup) {
		this.EmployeeSubGroup = employeeSubGroup;
	}

	public String getEmployeeSubgroupDescription() {
		return this.EmployeeSubgroupDescription;
	}

	public void setEmployeeSubgroupDescription(
			String employeeSubgroupDescription) {
		this.EmployeeSubgroupDescription = employeeSubgroupDescription;
	}

	public String getPersonnelSubarea() {
		return this.PersonnelSubarea;
	}

	public void setPersonnelSubarea(String personnelSubarea) {
		this.PersonnelSubarea = personnelSubarea;
	}

	public String getPersonnelSubareaDescription() {
		return this.PersonnelSubareaDescription;
	}

	public void setPersonnelSubareaDescription(
			String personnelSubareaDescription) {
		this.PersonnelSubareaDescription = personnelSubareaDescription;
	}

	public String getCompanyCode() {
		return this.CompanyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.CompanyCode = companyCode;
	}

	public String getPositionCode() {
		return this.PositionCode;
	}

	public void setPositionCode(String positionCode) {
		this.PositionCode = positionCode;
	}

	public String getPositionDescription() {
		return this.PositionDescription;
	}

	public void setPositionDescription(String positionDescription) {
		this.PositionDescription = positionDescription;
	}

	public String getJobCode() {
		return this.JobCode;
	}

	public void setJobCode(String jobCode) {
		this.JobCode = jobCode;
	}

	public String getOrganizationUnit() {
		return this.OrganizationUnit;
	}

	public void setOrganizationUnit(String organizationUnit) {
		this.OrganizationUnit = organizationUnit;
	}

	public String getOrganizationUnitDescription() {
		return this.OrganizationUnitDescription;
	}

	public void setOrganizationUnitDescription(
			String organizationUnitDescription) {
		this.OrganizationUnitDescription = organizationUnitDescription;
	}

	public String getCostCenter() {
		return this.CostCenter;
	}

	public void setCostCenter(String costCenter) {
		this.CostCenter = costCenter;
	}

	public String getCLLI() {
		return this.CLLI;
	}

	public void setCLLI(String cLLI) {
		this.CLLI = cLLI;
	}

	public String getCLLIdescription() {
		return this.CLLIdescription;
	}

	public void setCLLIdescription(String cLLIdescription) {
		this.CLLIdescription = cLLIdescription;
	}

	public String getFloor() {
		return this.Floor;
	}

	public void setFloor(String floor) {
		this.Floor = floor;
	}

	public String getHouseNoStreet() {
		return this.HouseNoStreet;
	}

	public void setHouseNoStreet(String houseNoStreet) {
		this.HouseNoStreet = houseNoStreet;
	}

	public String getCity() {
		return this.City;
	}

	public void setCity(String city) {
		this.City = city;
	}

	public String getProvince() {
		return this.Province;
	}

	public void setProvince(String province) {
		this.Province = province;
	}

	public String getPostalCode() {
		return this.PostalCode;
	}

	public void setPostalCode(String postalCode) {
		this.PostalCode = postalCode;
	}

	public String getPhoneNumber() {
		return this.PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.PhoneNumber = phoneNumber;
	}

	public String getJobDescription() {
		return this.JobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.JobDescription = jobDescription;
	}

	public String getWorkStyle() {
		return this.WorkStyle;
	}

	public void setWorkStyle(String workStyle) {
		this.WorkStyle = workStyle;
	}

	public String getBusinessIdentifier() {
		return this.BusinessIdentifier;
	}

	public void setBusinessIdentifier(String businessIdentifier) {
		this.BusinessIdentifier = businessIdentifier;
	}

	public String getOrgHierarchyCodeDescription() {
		return this.OrgHierarchyCodeDescription;
	}

	public void setOrgHierarchyCodeDescription(
			String orgHierarchyCodeDescription) {
		this.OrgHierarchyCodeDescription = orgHierarchyCodeDescription;
	}

	public String getManagerPersonnelNumber() {
		return this.ManagerPersonnelNumber;
	}

	public void setManagerPersonnelNumber(String managerPersonneNumber) {
		this.ManagerPersonnelNumber = managerPersonneNumber;
	}

	public String getManagerFirstName() {
		return this.ManagerFirstName;
	}

	public void setManagerFirstName(String managerFirstName) {
		this.ManagerFirstName = managerFirstName;
	}

	public String getManagerLastName() {
		return this.ManagerLastName;
	}

	public void setManagerLastName(String managerLastName) {
		this.ManagerLastName = managerLastName;
	}

	public String getManagerEmail() {
		return this.ManagerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.ManagerEmail = managerEmail;
	}

	public String getManagerPhoneNumber() {
		return this.ManagerPhoneNumber;
	}

	public void setManagerPhoneNumber(String managerPhoneNumber) {
		this.ManagerPhoneNumber = managerPhoneNumber;
	}

	public String getNumericalPID() {
		return this.NumericalPID;
	}

	public void setNumericalPID(String numericalPID) {
		this.NumericalPID = numericalPID;
	}
	
	public String getHireType() {
		return hireType;
	}

	public void setHireType(String hireType) {
		this.hireType = hireType;
	}
	
	public String getContractorSAP() {
		return ContractorSAP;
	}

	public void setContractorSAP(String contractorSAP) {
		ContractorSAP = contractorSAP;
	}

	public Boolean getBuildingAccess() {
		return buildingAccess;
	}

	public void setBuildingAccess(Boolean buildingAccess) {
		this.buildingAccess = buildingAccess;
	}

	public Boolean getSystemsAccess() {
		return systemsAccess;
	}

	public void setSystemsAccess(Boolean systemsAccess) {
		this.systemsAccess = systemsAccess;
	}

	public LocalEmployeeRecord getEmployeeRecord() {
		return this.employeeRecord;
	}

	public void setEmployeeRecord(LocalEmployeeRecord employeeRecord) {
		this.employeeRecord = employeeRecord;
	}

	public List<LocalEmployeeRecordStatus> getHistory() {
		return history;
	}

	public void setHistory(List<LocalEmployeeRecordStatus> history) {
		this.history = history;
	}

	public List<LocalEmployeeRecordComment> getComments() {
		return comments;
	}

	public void setComments(List<LocalEmployeeRecordComment> comments) {
		this.comments = comments;
	}
	
	public List<CATSRecord> getCatsRecords() {
		return catsRecords;
	}

	public void setCatsRecords(List<CATSRecord> catsRecords) {
		this.catsRecords = catsRecords;
	}

	public String getCatsID() {
		return catsID;
	}

	public void setCatsID(String catsID) {
		this.catsID = catsID;
	}
	
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setAddress(Address address) {
		if (address == null) return;
		this.HouseNoStreet = address.getStreet();
		this.Floor = address.getFloor();
		this.City = address.getCity();
		this.Province = address.getProvince();
		this.PostalCode = address.getPostalCode();
		if (isNotNull(address.getCompany())){
			this.CLLIdescription = address.getCompany();
		}
		if (isNotNull(address.getPhone())){
			this.PhoneNumber = address.getPhone();
		}
		if (isNotNull(address.getCostCenter())){
			this.CostCenter = address.getCostCenter();
		}
	}
	
	private Boolean isNotNull(String s) {
		return s != null && !"".equals(s);
	}
	
}