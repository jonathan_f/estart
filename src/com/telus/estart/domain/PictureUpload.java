package com.telus.estart.domain;

import java.sql.Blob;

public class PictureUpload {

	String id;
	byte[] blob;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public byte[] getBlob() {
		return blob;
	}
	public void setBlob(byte[] blob) {
		this.blob = blob;
	}
}
