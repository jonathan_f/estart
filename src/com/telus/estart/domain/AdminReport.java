package com.telus.estart.domain;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdminReport implements Serializable {
	// general
	private int total = 0;
	private int inProcess = 0;
	private int completed = 0;
	private int late = 0;
	private int backDated = 0;
	private int rehire = 0;
	private int navigator = 0;
	private int provisioner = 0;


	// regular full time team member fields
	private int teamMemberTotal = 0;
	private int teamMemberNavigator = 0;
	private int teamMemberProvisioner = 0;
	private int teamMemberInProcess = 0;
	private int teamMemberCompleted = 0;
	private int teamMemberLate = 0;
	private int teamMemberBackDated = 0;
	private int teamMemberRehire = 0;
	private int teamMemberC_E = 0;


	// contractors fields
	private int contractorTotal = 0;
	private int contractorNavigator = 0;
	private int contractorProvisioner = 0;
	private int contractorInProcess = 0;
	private int contractorCompleted = 0;
	private int contractorLate = 0;
	private int contractorBackDated = 0;
	private int contractorRehire = 0;
	private int contractorFieldglass = 0;
	private int contractorRegistration = 0;
	private int contractorVPN = 0;



	private SimpleDateFormat df;
	private Date curDate;

	public AdminReport(List<LocalEmployeeRecord> list) throws ParseException {
		df = new SimpleDateFormat("yyyy-MM-dd");
		curDate = new Date();
		loadEmployeeData(list);
	}

	private void loadEmployeeData(List<LocalEmployeeRecord> records)
			throws ParseException {
		for (LocalEmployeeRecord record : records) {
			checkLateAndBackDatedStatus(record);

			if (record.getType() == EStartConstants.EMPLOYEE_TYPE_FULL_TIME) {
				teamMemberTotal++;
				if (isRecordCompleted(record)) {
					teamMemberCompleted++;
				} else {
					teamMemberInProcess++;
				}
				if (record.getStatus() <= EStartConstants.STATUS_READY_TO_RPOVISION) {
					teamMemberNavigator++;
				} else if (record.getStatus() > EStartConstants.STATUS_READY_TO_RPOVISION && record.getStatus() < EStartConstants.STATUS_COMPLETED) {
					teamMemberProvisioner++;
				}
				if (record.getIsLate()) {
					teamMemberLate++;
				}
				if (record.getIsBackTracked()) {
					teamMemberBackDated++;
				}
				if (record.getHireType().equals("Rehire")) {
					teamMemberRehire++;
				} else if (record.getHireType().equals("C-E")) {
					teamMemberC_E++;
				}
				
			} else { // contractors
				contractorTotal++;
				if (isRecordCompleted(record)) {
					contractorCompleted++;
				} else {
					contractorInProcess++;
				}
				if (record.getStatus() <= EStartConstants.STATUS_READY_TO_RPOVISION) {
					contractorNavigator++;
				} else if (record.getStatus() > EStartConstants.STATUS_READY_TO_RPOVISION && record.getStatus() < EStartConstants.STATUS_COMPLETED) {
					contractorProvisioner++;
				}
				if (record.getIsLate()) {
					contractorLate++;
				}
				if (record.getIsBackTracked()) {
					contractorBackDated++;
				}
				if (record.getTags() != null) {
					String tags = record.getTags();
					if (tags.contains("fieldglass")) {
						contractorFieldglass++;
					}
					if (tags.contains("vpn")) {
						contractorVPN++;
					}
					if (tags.contains("rehire")) {
						contractorRehire++;
					}
				}
		
			}
		}

		contractorRegistration = contractorTotal - contractorFieldglass;
		
		total = teamMemberTotal + contractorTotal;
		completed = teamMemberCompleted + contractorCompleted;
		inProcess = teamMemberInProcess + contractorInProcess;
		late = teamMemberLate + contractorLate;
		backDated = teamMemberBackDated + contractorBackDated;
		rehire = teamMemberRehire + contractorRehire;
		navigator = teamMemberNavigator + contractorNavigator;
		provisioner = teamMemberProvisioner + contractorProvisioner;

		/*
		completedPercent = 100.0 * completed / total;
		latePercent = late / total * 100.0;
		backDatedPercent = backDated / total * 100.0;
		lateOrgManagementPercent = lateOrgManagement / late * 100.0;
		lateFinancePercent = lateFinance / late * 100.0;
		lateRecruitmentPercent = lateRecruitment / late * 100.0;
		lateBusinessPercent = lateBusiness / late * 100.0;
		
		if (teamMemberTotal != 0) {
			teamMemberCompletedPercent = teamMemberCompleted / teamMemberTotal
					* 100;
			teamMemberLatePercent = teamMemberLate / teamMemberTotal * 100;
			teamMemberBackDatedPercent = teamMemberBackDated / teamMemberTotal
					* 100;
		}

		if (teamMemberLate != 0) {
			teamMemberLateOrgManagementPercent = teamMemberLateOrgManagement
					/ teamMemberLate * 100;
			teamMemberLateFinancePercent = teamMemberLateFinance
					/ teamMemberLate * 100;
			teamMemberLateRecruitmentPercent = teamMemberLateRecruitment
					/ teamMemberLate * 100;
			teamMemberLateBusinessPercent = teamMemberLateBusiness
					/ teamMemberLate * 100;
		}

		if (contractorTotal != 0) {
			contractorCompletedPercent = contractorCompleted / contractorTotal
					* 100;
			contractorLatePercent = contractorLate / contractorTotal * 100;
			contractorBackDatedPercent = contractorBackDated / contractorTotal
					* 100;
		}

		if (contractorLate != 0) {
			contractorLateOrgManagementPercent = contractorLateOrgManagement
					/ contractorLate * 100;
			contractorLateFinancePercent = contractorLateFinance
					/ contractorLate * 100;
			contractorLateRecruitmentPercent = contractorLateRecruitment
					/ contractorLate * 100;
			contractorLateBusinessPercent = contractorLateBusiness
					/ contractorLate * 100;
		}
		 */
	}

	private void checkLateAndBackDatedStatus(LocalEmployeeRecord record)
			throws ParseException {
		Date createDate = df.parse(record.getCreateDate());
		Date hireDate = df.parse(record.getHireDate());
		Date completeDate = null;
		if (record.getCompleteDate() != null) {
			completeDate = df.parse(record.getCompleteDate());
		}

		if (!createDate.before(hireDate)) {
			record.setBackTracked(true);
		}
		if ((completeDate == null && curDate.after(hireDate))
				|| (completeDate != null && completeDate.after(hireDate))) {
			record.setLate(true);
		}
	}

	private Boolean isRecordCompleted(LocalEmployeeRecord record) {
		if (record.getStatus() == EStartConstants.STATUS_COMPLETED) {
			return true;
		} else {
			return false;
		}
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getInProcess() {
		return inProcess;
	}

	public void setInProcess(int inProcess) {
		this.inProcess = inProcess;
	}

	public int getCompleted() {
		return completed;
	}

	public void setCompleted(int completed) {
		this.completed = completed;
	}

	public int getLate() {
		return late;
	}

	public void setLate(int late) {
		this.late = late;
	}

	public int getBackDated() {
		return backDated;
	}

	public void setBackDated(int backDated) {
		this.backDated = backDated;
	}

	public int getTeamMemberTotal() {
		return teamMemberTotal;
	}

	public void setTeamMemberTotal(int teamMemberTotal) {
		this.teamMemberTotal = teamMemberTotal;
	}

	public int getTeamMemberInProcess() {
		return teamMemberInProcess;
	}

	public void setTeamMemberInProcess(int teamMemberInProcess) {
		this.teamMemberInProcess = teamMemberInProcess;
	}

	public int getTeamMemberCompleted() {
		return teamMemberCompleted;
	}

	public void setTeamMemberCompleted(int teamMemberCompleted) {
		this.teamMemberCompleted = teamMemberCompleted;
	}

	public int getTeamMemberLate() {
		return teamMemberLate;
	}

	public void setTeamMemberLate(int teamMemberLate) {
		this.teamMemberLate = teamMemberLate;
	}

	public int getTeamMemberBackDated() {
		return teamMemberBackDated;
	}

	public void setTeamMemberBackDated(int teamMemberBackDated) {
		this.teamMemberBackDated = teamMemberBackDated;
	}

	public int getContractorTotal() {
		return contractorTotal;
	}

	public void setContractorTotal(int contractorTotal) {
		this.contractorTotal = contractorTotal;
	}

	public int getContractorInProcess() {
		return contractorInProcess;
	}

	public void setContractorInProcess(int contractorInProcess) {
		this.contractorInProcess = contractorInProcess;
	}

	public int getContractorCompleted() {
		return contractorCompleted;
	}

	public void setContractorCompleted(int contractorCompleted) {
		this.contractorCompleted = contractorCompleted;
	}

	public int getContractorLate() {
		return contractorLate;
	}

	public void setContractorLate(int contractorLate) {
		this.contractorLate = contractorLate;
	}

	public int getContractorBackDated() {
		return contractorBackDated;
	}

	public void setContractorBackDated(int contractorBackDated) {
		this.contractorBackDated = contractorBackDated;
	}

	public int getTeamMemberNavigator() {
		return teamMemberNavigator;
	}

	public void setTeamMemberNavigator(int teamMemberNavigator) {
		this.teamMemberNavigator = teamMemberNavigator;
	}

	public int getTeamMemberProvisioner() {
		return teamMemberProvisioner;
	}

	public void setTeamMemberProvisioner(int teamMemberProvisioner) {
		this.teamMemberProvisioner = teamMemberProvisioner;
	}

	public int getTeamMemberRehire() {
		return teamMemberRehire;
	}

	public void setTeamMemberRehire(int teamMemberRehire) {
		this.teamMemberRehire = teamMemberRehire;
	}

	public int getTeamMemberC_E() {
		return teamMemberC_E;
	}

	public void setTeamMemberC_E(int teamMemberC_E) {
		this.teamMemberC_E = teamMemberC_E;
	}

	public int getContractorNavigator() {
		return contractorNavigator;
	}

	public void setContractorNavigator(int contractorNavigator) {
		this.contractorNavigator = contractorNavigator;
	}

	public int getContractorProvisioner() {
		return contractorProvisioner;
	}

	public void setContractorProvisioner(int contractorProvisioner) {
		this.contractorProvisioner = contractorProvisioner;
	}

	public int getContractorRehire() {
		return contractorRehire;
	}

	public void setContractorRehire(int contractorRehire) {
		this.contractorRehire = contractorRehire;
	}

	public int getContractorFieldglass() {
		return contractorFieldglass;
	}

	public void setContractorFieldglass(int contractorFieldglass) {
		this.contractorFieldglass = contractorFieldglass;
	}

	public int getContractorRegistration() {
		return contractorRegistration;
	}

	public void setContractorRegistration(int contractorRegistration) {
		this.contractorRegistration = contractorRegistration;
	}

	public int getContractorVPN() {
		return contractorVPN;
	}

	public void setContractorVPN(int contractorVPN) {
		this.contractorVPN = contractorVPN;
	}

	public int getRehire() {
		return rehire;
	}

	public void setRehire(int rehire) {
		this.rehire = rehire;
	}

	public int getNavigator() {
		return navigator;
	}

	public void setNavigator(int navigator) {
		this.navigator = navigator;
	}

	public int getProvisioner() {
		return provisioner;
	}

	public void setProvisioner(int provisioner) {
		this.provisioner = provisioner;
	}
	
	
}
