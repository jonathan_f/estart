package com.telus.estart.domain;

import java.io.Serializable;

public class LocalEmployeeRecordComment implements Serializable {

	private String tid;
	private int type;
	private Boolean active;
	private String comment;
	private String createdBy;
	private String createdAt;

	public LocalEmployeeRecordComment() {
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}
	
	
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "LocalEmployeeRecordComment [tid=" + tid + ", comment="
				+ comment + ", createdBy=" + createdBy + ", createdAt="
				+ createdAt + "]";
	}


}