package com.telus.estart.domain;

import java.io.Serializable;
import java.util.Date;

public class CATSRecord implements Serializable {
	
	private String catsID;
	private String tid;
	private String birthday; 
	private String fullName;
	private String managerName;
	private String applicantCity;
	private String applicantProvince;
	private String companyName;

	public CATSRecord() {
		
	}

	public CATSRecord(String catsID, String tid, String birthday,
			String fullName, String managerName, String applicantCity,
			String applicantProvince, String companyName) {
		super();
		this.catsID = catsID;
		this.tid = tid;
		this.birthday = birthday;
		this.fullName = fullName;
		this.managerName = managerName;
		this.applicantCity = applicantCity;
		this.applicantProvince = applicantProvince;
		this.companyName = companyName;
	}



	public String getCatsID() {
		return catsID;
	}

	public void setCatsID(String catsID) {
		this.catsID = catsID;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getApplicantCity() {
		return applicantCity;
	}

	public void setApplicantCity(String applicantCity) {
		this.applicantCity = applicantCity;
	}

	public String getApplicantProvince() {
		return applicantProvince;
	}

	public void setApplicantProvince(String applicantProvince) {
		this.applicantProvince = applicantProvince;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public String toString() {
		return "CATSRecord [catsID=" + catsID + ", tid=" + tid + ", birthday="
				+ birthday + ", fullName=" + fullName + ", managerName="
				+ managerName + ", applicantCity=" + applicantCity
				+ ", applicantProvince=" + applicantProvince + ", companyName="
				+ companyName + "]";
	}
	
	
	

}