package com.telus.estart.domain;

import java.io.Serializable;

public class MailroomLocation implements Serializable {
	private static final long serialVersionUID = -2807264086756206771L;
	private String id;
	private int locationID;

	private String updatedBy;
	
	public MailroomLocation() {
		
	}

	public MailroomLocation(String id, int mailRoomValue, String updatedBy) {
		super();
		this.id = id;
		this.locationID = mailRoomValue;
		this.updatedBy = updatedBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getLocationID() {
		return locationID;
	}

	public void setLocationID(int locationID) {
		this.locationID = locationID;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}
