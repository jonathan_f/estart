package com.telus.estart.domain;

import java.io.Serializable;
import java.util.Date;

public class LocalEmployeeRecordStatus implements Serializable {
	private static final long serialVersionUID = 9007173760406711534L;
	private String tid;
	private Boolean active;
	private int status;
	private Boolean isGoodType;
	private String statusName; 
	private String createdBy;
	private String createdDate;

	public LocalEmployeeRecordStatus() {
	
	}
	
	

	public LocalEmployeeRecordStatus(String tid, int status, String createdBy) {
		super();
		this.tid = tid;
		this.status = status;
		this.createdBy = createdBy;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Boolean getIsGoodType() {
		return isGoodType;
	}

	public void setIsGoodType(Boolean isGoodType) {
		this.isGoodType = isGoodType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
}