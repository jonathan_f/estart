package com.telus.estart.domain;

public class EStartEmailContent {
	/**
	 * Employee email contents
	 */
	
	/* Resubmit Photo*/
	public static final String EMPLOYEE_RESUBMIT_PHOTO_SUBJECT_EN = 
		"Please submit a valid photo";
	public static final String EMPLOYEE_RESUBMIT_PHOTO_BODY_EN = 
		"<p>You recently emailed us a photo for your badge, however it did not meet the criteria listed below. Please send an email to <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> with a new JPEG photo that meets the following requirements:</p>"
		+ "<ul>"
		+ "<li>Recent colour photo (in order to easily identify you)</li>"
		+ "<li>Minimum resolution of 400x600 pixels</li>"
		+ "<li>Directly facing camera from the chest up, of the shoulders and head</li>"
		+ "<li>Plain, light-coloured background without shadows</li>"
		+ "<li>No hat or sunglasses</li>"
		+ "<li>Eyes should be clearly visible and should not appear to be red</li>"
		+ "<li>Head and shoulders in the photo must be at least 1/3 the height of the frame</li>"
		+ "</ul>";
	
	public static final String EMPLOYEE_RESUBMIT_PHOTO_SUBJECT_FR = 
		"Veuillez retransmettre une photo valide";
	public static final String EMPLOYEE_RESUBMIT_PHOTO_BODY_FR = 
		"<p>Vous nous avez r�cemment envoy� une photo par courriel pour votre carte d'identit�. Toutefois, elle ne respectait pas les crit�res indiqu�s ci-dessous. Veuillez transmettre un courriel � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> contenant une nouvelle photo de format JPEG qui satisfait aux exigences suivantes :</p>"
		+ "<ul>"
		+ "<li>Photo r�cente en couleurs (afin de vous identifier facilement)</li>"
		+ "<li>R�solution minimale de 400 x 600 pixels</li>"
		+ "<li>Photo prise de face � partir de la poitrine et montrant les �paules et la t�te</li>"
		+ "<li>Arri�re-plan neutre de couleur claire, sans aucune ombre</li>"
		+ "<li>Aucun chapeau ni lunettes de soleil</li>"
		+ "<li>Les yeux doivent �tre clairement visibles et ne pas appara�tre rouges</li>"
		+ "<li>La t�te et les �paules doivent occuper au moins un tiers de la hauteur du cadre</li>"
		+ "</ul>";
	
	/* Photo not received */
	public static final String EMPLOYEE_PHOTO_NOT_RECEIVED_SUBJECT_EN =
		"Photo Not Received: New Hire Name - %s Employee ID %s  and Taleo ID %s.";
	public static final String EMPLOYEE_PHOTO_NOT_RECEIVED_BODY_EN =	
		"<p>Photo Not Received: New Hire Name - %s Employee id %s and Taleo Id. %s.</p>"
		+ "<p>This employee has been hired and their photo for their Access Card has not been received.</p>"
		+ "<p>Please follow-up with the new hire.</p>";
	
	public static final String EMPLOYEE_PHOTO_NOT_RECEIVED_SUBJECT_FR =
		"Photo non re�ue : nom du nouvel employ� - %s num�ro d'employ� %s et num�ro Taleo %s";
	public static final String EMPLOYEE_PHOTO_NOT_RECEIVED_BODY_FR =	
		"<p>Photo non re�ue : nom du nouvel employ� - %s num�ro d'employ� %s et num�ro Taleo %s</p>"
		+ "<p>Cet employ� a �t� embauch�, et sa photo devant appara�tre sur sa carte intelligente n'a pas �t� re�ue.</p>"
		+ "<p>Veuillez communiquer avec le nouvel employ�.</p>";
	
	
	/* Time to submit new access requests */
	public static final String EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_SUBJECT_EN =
		"ACTION REQUIRED: Time to submit access requests for %s";
	public static final String EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_BODY_EN =
		"<p>Your new hire %s will be starting soon. Please submit Novo requests for any additional access they may require so that they have it for their start date.</p>"
		+ "<p>Novo can be accessed <a href=\"https://novo.tsl.telus.com/aveksa/main\">here</a></p>";
	
	public static final String EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_SUBJECT_FR =
		"ACTION REQUISE : Il est temps de soumettre les demandes d'acc�s pour %s";
	public static final String EMPLOYEE_SUBMIT_NEW_ACCESS_REQUESTS_BODY_FR =
		"<p>Votre nouvel employ�, %s, commencera sous peu. Veuillez soumettre une demande par Novo pour faire en sorte que tout acc�s suppl�mentaire n�cessaire lui soit fourni � sa date d'entr�e en fonction.</p>"
		+ "<p>Vous pouvez acc�der � Novo <a href=\"https://novo.tsl.telus.com/aveksa/main\">ici</a></p>";
		
	/* Courier Delivery Notice to Manager */
	public static final String EMPLOYEE_COURIER_DELIVERY_NOTICE_TO_MANAGER_SUBJECT_EN =
		"Action Required: Access Card for your new team member %s";
	public static final String EMPLOYEE_COURIER_DELIVERY_NOTICE_TO_MANAGER_BODY_EN =
		"<p>An access Card for your new team member %s has been couriered to</p>"
		+ "<p>%s</p>"
		+ "<p>A separate email with the PIN has been sent to the employees email address.</p>"
		+ "<p>As soon as your team member has received the card, please email <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> to confirm.</p>"
		+ "<p>If you have not already done so, please submit any additional requests for access for your new team member <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">here</a></p>";
	
	public static final String EMPLOYEE_COURIER_DELIVERY_NOTICE_TO_MANAGER_SUBJECT_FR =
		"Action requise : Carte intelligente pour %s, votre nouveau membre de l'�quipe";
	public static final String EMPLOYEE_COURIER_DELIVERY_NOTICE_TO_MANAGER_BODY_FR =
		"<p>Une carte intelligente pour %s, votre nouveau membre de l'�quipe, a �t� exp�di�e par service de messagerie � l'adresse suivante</p>"
		+ "<p>%s</p>"
		+ "<p>Le NIP a �t� envoy� dans un courriel distinct � l'adresse de l'employ�.</p>"
		+ "<p>D�s que votre membre de l'�quipe re�oit la carte, veuillez envoyer un courriel � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> pour le confirmer.</p>"
		+ "<p>Si vous ne l'avez pas d�j� fait, veuillez soumettre d'autres demandes d'acc�s, s'il y a lieu, pour votre nouveau membre de l'�quipe <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">ici</a></p>";
		
	/* Mailroom Delivery Advance Notice to Manager. */
	public static final String EMPLOYEE_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_SUBJECT_EN =
		"Action Required: Access Card for your new team member %s";
	public static final String EMPLOYEE_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_BODY_EN =
		"<p>An access Card for your new team member %s has been sent via internal mail to </p>"
		+ "<p>%s</p>"
		+ "<p>Note:  The mailroom will send out an email once the card has arrived. Please wait for the notification.</p>"
		+ "<p>Anyone with a valid TELUS ID can pick up the card on the new hire's behalf by signing for it.</p>"
		+ "<p>Alternatively the new hire can show picture ID such as drivers license and sign for it themselves.   They may need to be accompanied by a TELUS employee to reach the mailroom.</p>"
		+ "<p>A separate email with the PIN has been sent to the employees email address.</p> "
		+ "<p>As soon as your team member has received the card, please email <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> to confirm.</p>"
		+ "<p>If you have not already done so, please submit any additional requests for access for your new team member <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">here</a></p>";
	
	public static final String EMPLOYEE_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_SUBJECT_FR =
		"Action requise : Carte intelligente pour %s, votre nouveau membre de l'�quipe";
	public static final String EMPLOYEE_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_BODY_FR =
		"<p>Une carte intelligente pour %s, votre nouveau membre de l'�quipe, a �t� envoy�e par courrier interne � l�adresse suivante :</p>"
		+ "<p>%s</p>"
		+ "<p>Note : Service de courrier enverra un courriel d�s que la carte est arriv�e. S`il vous pla�t veuillez attendre pour la confirmation.</p>"
		+ "<p>Quiconque muni d'un num�ro d'employ� TELUS valide peut recueillir la carte au nom du nouvel employ� en fournissant sa signature. Le nouvel employ� peut �galement montrer une carte d'identit� avec photo, comme un permis de conduire, et signer lui-m�me. L'accompagnement d'un employ� de TELUS peut �tre requis pour acc�der � la salle de messagerie.</p>"
		+ "<p>Le NIP a �t� envoy� dans un courriel distinct � l'adresse de l'employ�.</p> "
		+ "<p>D�s que votre membre de l'�quipe re�oit sa carte, veuillez envoyer un courriel � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> pour le confirmer.</p>"
		+ "<p>Si vous ne l'avez pas d�j� fait, veuillez soumettre d'autres demandes d'acc�s, s'il y a lieu, pour votre nouveau membre de l'�quipe <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">ici</a></p>";
			
	/* Confirmation of delivery */
	public static final String EMPLOYEE_CONFIRMATION_OF_DELIVERY_SUBJECT_EN =
		"ACTION REQUIRED:  Please confirm receipt of Access Card for %s";
	public static final String EMPLOYEE_CONFIRMATION_OF_DELIVERY_BODY_EN =
		"<p>A Access Card was recently sent to your new hire %s.  Please confirm that your team member has received the card by emailing <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a>.</p>";
	
	public static final String EMPLOYEE_CONFIRMATION_OF_DELIVERY_SUBJECT_FR =
		"ACTION REQUISE : Veuillez confirmer la r�ception de la carte intelligente de %s";
	public static final String EMPLOYEE_CONFIRMATION_OF_DELIVERY_BODY_FR =
		"<p>Une carte intelligente a r�cemment �t� envoy�e � %s, votre nouvel employ�. Veuillez confirmer que votre membre de l'�quipe a re�u la carte en envoyant un courriel � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a>.</p>";
		
	/**
	 * Contractors email contents
	 */
	
	/* Email managers for adequate picture  */
	public static final String CONTRACTOR_RESUBMIT_PHOTO_SUBJECT_EN =
		"ACTION REQUIRED: Resubmit a valid photo for %s";
	public static final String CONTRACTOR_RESUBMIT_PHOTO_BODY_EN =
		"<p>The photo received for your new contractor %s did not meet one or more of the following criteria:</p>"
		+ "<ul>"
		+ "<li>Recent colour photo (in order to easily identify you)</li>"
		+ "<li>Minimum resolution of 400x600 pixels</li>"
		+ "<li>Directly facing camera from the chest up, of the shoulders and head</li>"
		+ "<li>Plain, light-coloured background without shadows</li>"
		+ "<li>No hat or sunglasses</li>"
		+ "<li>Eyes should be clearly visible and should not appear to be red</li>"
		+ "<li>Head and shoulders in the photo must be at least 1/3 the height of the frame</li>"
		+ "</ul>"
		+ "<p>Please email a new photo that meets all criteria to <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> as soon as possible.</p>"
		+ "<p>Please ensure the new contractor's name is in the subject line.</p>";
	
	public static final String CONTRACTOR_RESUBMIT_PHOTO_SUBJECT_FR =
		"ACTION REQUISE : Retransmettre une photo valide pour %s";
	public static final String CONTRACTOR_RESUBMIT_PHOTO_BODY_FR =
		"<p>La photo re�ue pour %s, votre nouveau contractuel, ne respecte pas au  moins un des crit�res suivants:</p>"
		+ "<ul>"
		+ "<li>Photo r�cente en couleurs (afin de vous identifier facilement)</li>"
		+ "<li>R�solution minimale de 400 x 600 pixels</li>"
		+ "<li>Photo prise de face � partir de la poitrine et montrant les �paules et la t�te</li>"
		+ "<li>Arri�re-plan neutre de couleur claire, sans aucune ombre</li>"
		+ "<li>Aucun chapeau ni lunettes de soleil</li>"
		+ "<li>Les yeux doivent �tre clairement visibles et ne pas appara�tre rouges</li>"
		+ "<li>La t�te et les �paules doivent occuper au moins un tiers de la hauteur du cadre</li>"
		+ "</ul>"
		+ "<p>Veuillez envoyer par courriel, dans les plus brefs d�lais, une nouvelle photo qui respecte tous les crit�res � l'adresse <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> as soon as possible.</p>"
		+ "<p>Veuillez inscrire le nom du nouveau contractuel dans l'objet du courriel.</p>";
		
	/* Request a photo for a contractor */
	public static final String CONTRACTOR_SUBMIT_PHOTO_SUBJECT_EN =
		"IMMEDIATE ACTION REQUIRED: Please submit a photo for %s";
	public static final String CONTRACTOR_SUBMIT_PHOTO_BODY_EN = 
		"<p>Your new contractor (%s) was registered as requiring building access, however no photo has been received. Please submit a photo as soon as possible to <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a>. Without a photo, the building access card cannot be provisioned. Please reference the contractor name in the subject line.<p>"
		+ "<ul>"
		+ "<li>Recent colour photo (in order to easily identify you)</li>"
		+ "<li>Minimum resolution of 400x600 pixels</li>"
		+ "<li>Directly facing camera from the chest up, of the shoulders and head</li>"
		+ "<li>Plain, light-coloured background without shadows</li>"
		+ "<li>No hat or sunglasses</li>"
		+ "<li>Eyes should be clearly visible and should not appear to be red</li>"
		+ "<li>Head and shoulders in the photo must be at least 1/3 the height of the frame</li>"
		+ "</ul>";
	
	public static final String CONTRACTOR_SUBMIT_PHOTO_SUBJECT_FR =
		"ACTION IMM�DIATE REQUISE : Veuillez transmettre une photo pour %s";
	public static final String CONTRACTOR_SUBMIT_PHOTO_BODY_FR = 
		"<p>Selon nos dossiers, votre nouveau contractuel, (%s), a demand� un acc�s � l'immeuble. Toutefois, nous n'avons re�u aucune photo. Veuillez transmettre une photo dans les plus brefs d�lais � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a>. Sans photo, la carte d'acc�s � l'immeuble ne peut �tre �mise. Veuillez inscrire le nom du contractuel dans l'objet du courriel. <p>"
		+ "<p>La photo doit respecter les exigences suivantes :</p>"
		+ "<ul>"
		+ "<li>Photo r�cente en couleurs (afin de vous identifier facilement)</li>"
		+ "<li>R�solution minimale de 400 x 600 pixels</li>"
		+ "<li>Photo prise de face � partir de la poitrine et montrant les �paules et la t�te</li>"
		+ "<li>Arri�re-plan neutre de couleur claire, sans aucune ombre</li>"
		+ "<li>Aucun chapeau ni lunettes de soleil</li>"
		+ "<li>Les yeux doivent �tre clairement visibles et ne pas appara�tre rouges</li>"
		+ "<li>La t�te et les �paules doivent occuper au moins un tiers de la hauteur du cadre</li>"
		+ "</ul>";
	
	/* Remind manager to submit Access Requests */
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_SUBJECT_EN = 
		"ACTION REQUIRED: Please submit access requests for your new contractor %s";
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_BODY_EN = 
		"<p>Your new contractor %s will be starting on %s. If building access is required, your contractor will receive a building access card by that date (assuming adequate lead time). You can submit requests through Novo now to ensure %s has other access necessary by start date.<p>"
		+ "<p>Novo can be accessed <a href=\"https://novo.tsl.telus.com/aveksa/main\">here</a></p>";
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_SUBJECT_FR = 
		"ACTION REQUISE : Veuillez soumettre les demandes d'acc�s pour %s, votre nouveau contractuel.";
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_ACCESS_REQUESTS_BODY_FR = 
		"<p>Votre nouveau contractuel, %s, commencera le %s. Si un acc�s � l�immeuble est requis, votre contractuel recevra une carte d�acc�s d�ici � cette date, pourvu que les d�lais pour la demande aient �t� respect�s. Vous pouvez soumettre toute demande d�acc�s additionnelle n�cessaire par Novo maintenant pour faire en sorte que %s ait tous les acc�s voulus d�ici sa date d�entr�e en fonction.</p>"	
		+ "<p>Vous pouvez acc�der � Novo <a href=\"https://novo.tsl.telus.com/aveksa/main\">ici</a></p>";
			
	/* Remind manager to submit clearance record */
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_SUBJECT_EN = 
		"IMMEDIATE ACTION REQUIRED: Please submit a clearance record for %s";
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_BODY_EN = 
		"<p>Our records do not show a recent security clearance in progress or completed for your new contractor <name>.<p>"
		+ "<p>Please access the <a href=\"https://cats1.tsl.telus.com\">CATS</a> system and start a clearance record today.<p>"
		+ "<p>Please email <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> with the CATS number in order to prevent delays in onboarding of your new contractor.<p>"
		+ "<p><b>Note</b>:  a clearance record must be in progress but does not need to be complete to move forward with onboarding.</p>";
	
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_SUBJECT_FR = 
		"ACTION IMM�DIATE REQUISE : Veuillez �tablir un dossier de filtrage de s�curit� pour %s";
	public static final String CONTRACTOR_REMINDER_TO_SUBMIT_CLEARANCE_RECORD_BODY_FR = 
		"<p>Nos dossiers n`indiquent aucun dossier de filtrage de s�curit� en cours ou compl�t� pour <nom>, votre nouveau contractuel.<p>"
		+ "<p>Veuillez acc�der au syst�me <a href=\"https://cats1.tsl.telus.com\">CATS</a> et �tablir un dossier de filtrage de s�curit� d�s aujourd'hui.<p>"
		+ "<p>S`il vous pla�t envoyez un courriel <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> avec le num�ro de CATS afin d`�viter des retards dans la date pr�vue de commencement.<p>"
		+ "<b>Remarque</b> :  Pour que le contractuel puisse commencer � travailler � la date pr�vue, il n'est pas n�cessaire que le   dossier de filtrage soit termin�; il suffit qu'il soit en cours.";
	
	/* Notify manager of couriered access card.  */
	public static final String CONTRACTOR_COURIER_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_SUBJECT_EN =
		"Action Required: Access card for your new contractor %s";
	public static final String CONTRACTOR_COURIER_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_BODY_EN =
		"<p>An access card for your new contractor %s has been couriered to<p>"
		+ "<p>%s</p>"
		+ "<p>via %s. The tracking number is %s.</p>"
		+ "<p>Please email <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> to confirm that your contractor received the card.</p>"
		+ "<p>If you have not already done so, please submit any additional requests for access for your new contractor <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">here</a></p>";
	
	public static final String CONTRACTOR_COURIER_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_SUBJECT_FR =
		"Action requise : Carte d'acc�s � l'attention de %s, votre nouveau contractuel";
	public static final String CONTRACTOR_COURIER_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_BODY_FR =
		"<p>Une carte d'acc�s � l'attention de %s, votre nouveau contractuel, a �t� envoy�e par service de messagerie � l'adresse suivante:<p>"
		+ "<p>%s</p>"
		+ "<p>par %s. Le num�ro de suivi est %s.</p>"
		+ "<p>Veuillez envoyer un courriel � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> pour confirmer la r�ception de la carte par votre contractuel.</p>"
		+ "<p>Si vous ne l'avez pas d�j� fait, veuillez soumettre d'autres demandes d'acc�s, s'il y a lieu, pour votre nouveau contractuel <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">ici</a></p>";

	/* Notify manager of access card sent to mailroom */
	public static final String CONTRACTOR_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_SUBJECT_EN =
		"Action Required: Access card for your new contractor %s";
	public static final String CONTRACTOR_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_BODY_EN =
		"<p>An access card for your new contractor %s has been sent via intercompany mail to</p>"
		+ "<p>%s</p>"
		+ "<p>Note:  The mailroom will send out an email once the card has arrived. Please wait for the notification.</p>"
		+ "<p>Anyone with a valid TELUS ID card may sign for and pick up the card. If the contractor is picking it up themselves, they will need to show picture ID and sign, and may need to be accompanied by a TELUS employee to access the mailroom.</p>"
		+ "<p>Please email <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> to confirm that your contractor received the card.</p>"
		+ "<p>If you have not already done so, please submit any additional requests for access for your new contractor <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">here</a></p>";
	
	public static final String CONTRACTOR_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_SUBJECT_FR =
		"Action requise : Carte d'acc�s � l'attention de %s, votre nouveau contractuel";
	public static final String CONTRACTOR_MAILROOM_DELIVERY_ADVANCE_NOTICE_TO_MANAGER_BODY_FR =
		"<p>Une carte d'acc�s pour %s, votre nouveau contractuel, a �t� envoy�e par courrier interne � l'adresse suivante:</p>"
		+ "<p>%s</p>"
		+ "<p>Note : Service de courrier enverra un courriel d�s que la carte est arriv�e. S`il vous pla�t veuillez attendre pour la confirmation.</p>"
		+ "<p>Quiconque muni d'une carte d'employ� TELUS valide peut recueillir la carte moyennant une signature. Si le contractuel le fait lui-m�me, il doit montrer une carte d'identit� avec photo et fournir sa signature. L'accompagnement d'un employ� de TELUS peut �tre n�cessaire pour acc�der � la salle de messagerie. </p>"
		+ "<p>Veuillez envoyer un courriel � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a> pour confirmer la r�ception de la carte par votre contractuel.</p>"
		+ "<p>Si vous ne l'avez pas d�j� fait, veuillez soumettre d'autres demandes d'acc�s, s'il y a lieu, pour votre nouveau contractuel <a href=\"http://habitat.tmi.telus.com/collaborate/display/corpsec/Access+Requests\">ici</a></p>";
		
	/* Request confirmation that card has been received */
	public static final String CONTRACTOR_CONFIRMATION_OF_DELIVERY_SUBJECT_EN =
		"ACTION REQUIRED:  Please confirm receipt of Access Card for %s";
	public static final String CONTRACTOR_CONFIRMATION_OF_DELIVERY_BODY_EN =
		"<p>An Access Card was recently sent to your new contractor  %s.  Please confirm that your contractor has received the card by emailing <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a>.</p>";
	
	public static final String CONTRACTOR_CONFIRMATION_OF_DELIVERY_SUBJECT_FR =
		"ACTION REQUISE : Veuillez confirmer la r�ception de la carte d'acc�s de %s";
	public static final String CONTRACTOR_CONFIRMATION_OF_DELIVERY_BODY_FR =
		"<p>Une carte d'acc�s a r�cemment �t� envoy�e � %s, votre nouveau contractuel. Veuillez confirmer que votre contractuel a re�u la carte en envoyant un courriel � <a href=\"mailto:BadgePhoto@telus.com\">BadgePhoto@telus.com</a>.</p>";
		
}

