package com.telus.estart.domain;

public class EStartConstants {
	final public static int STATUS_NEW 					= 1;
	final public static int STATUS_PENDING 				= 2;
	final public static int STATUS_READY_TO_RPOVISION 	= 3;
	final public static int STATUS_ASSIGNED 			= 4;
	final public static int STATUS_CARD_SENT 			= 5;
	final public static int STATUS_COMPLETED		 	= 6;
	final public static int STATUS_QUIT_BEFORE_START	= 7;
	final public static int STATUS_PICTURE_UPLOADED		= 8;
	final public static int STATUS_PICTURE_READY	 	= 9;
	final public static int STATUS_LDAP_READY 			= 10;
	final public static int STATUS_CMS_READY 			= 11;
	final public static int STATUS_IDM_READY 			= 12;
	final public static int STATUS_BACK_DATED 			= 13;
	final public static int STATUS_REPORTED_LATE		= 14;
	final public static int STATUS_ESCALTED 			= 15;
	final public static int STATUS_CATS_LINKED			= 16;
	final public static int STATUS_LDAP_NOT_REQUIRED	= 17;
	final public static int STATUS_CMS_NOT_REQUIRED		= 18;
	final public static int STATUS_IDM_NOT_REQUIRED		= 19;
	final public static int STATUS_CARD_RECEIVED 		= 20;	
	final public static int STATUS_SHIP_TO_READY		= 21;	
	final public static int STATUS_VPN_REQUIRED			= 22;	
	final public static int STATUS_VPN_READY			= 23;	
	
	final public static int EMPLOYEE_TYPE_FULL_TIME 	= 1;
	final public static int EMPLOYEE_TYPE_CONTRACTOR 	= 2;
	final public static int EMPLOYEE_TYPE_CONTRACTOR_BUILDING_ACCESS_ONLY 	= 3;
	final public static int EMPLOYEE_TYPE_CONTRACTOR_SYSTEMS_ACCESS_ONLY 	= 4;
	
	final public static int COMMENT_USER_INPUT 					= 1;
	final public static int COMMENT_ESCALATATION_HR				= 2;
	final public static int COMMENT_ESCALATATION_MANAGER		= 3;
	final public static int COMMENT_LATE_ORG_MGM				= 4;
	final public static int COMMENT_LATE_FINANCE				= 5;
	final public static int COMMENT_LATE_RECRUITMENT 			= 6;
	final public static int COMMENT_LATE_BUSINESS				= 7;
	final public static int COMMENT_LATE_SECURITY_CUSTOMER_CARE	= 8;
	final public static int COMMENT_LATE_TECHNICAL_ISSUE		= 9;
	
	final public static int ESCALATATION_EMPLOYEE_NO_PHOTO 			= 1;
	final public static int ESCALATATION_EMPLOYEE_INVALID_PHOTO 	= 2;
	final public static int ESCALATATION_CONTRACTOR_NO_PHOTO		= 1;
	final public static int ESCALATATION_CONTRACTOR_INVALID_PHOTO	= 2;
	final public static int ESCALATATION_CONTRACTOR_ACCESS_REQUEST	= 3;
	final public static int ESCALATATION_CONTRACTOR_CLEARANCE_RECORD= 4;
	
	final public static int SHIPPING_COURIER_PUROLATOR		= 1;
	final public static int SHIPPING_COURIER_DYNAMEX		= 2;
	final public static int SHIPPING_COURIER_FEDEX			= 3;
}
