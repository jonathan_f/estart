package com.telus.estart.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import com.telus.estart.domain.TeamMember;

public class ShippingMailRoomLabelView extends AbstractExcelView
{
  
  public ShippingMailRoomLabelView(){
	  super();
	  //load template from this location
	  setUrl("/template/MailRoomLabel");
  }
  
  protected void buildExcelDocument(Map model, Workbook workbook, HttpServletRequest request, HttpServletResponse response)
    throws Exception
  {
	  TeamMember toTeamMember = (TeamMember) model.get("toTeamMember");
	  TeamMember fromTeamMember = (TeamMember) model.get("fromTeamMember");
	  //main sheet
	  Sheet sheet = workbook.getSheetAt(0); 

	  setText(getCell(sheet, 2, 2), fromTeamMember.getPreferredFirstName() + ' ' + fromTeamMember.getLastName());
	  setText(getCell(sheet, 3, 2), fromTeamMember.getCLLIdescription());
	  setText(getCell(sheet, 4, 2), fromTeamMember.getFloor());
	  setText(getCell(sheet, 5, 2), fromTeamMember.getHouseNoStreet());
	  setText(getCell(sheet, 6, 2), fromTeamMember.getCity());
	  setText(getCell(sheet, 6, 4), fromTeamMember.getProvince());
	  setText(getCell(sheet, 7, 2), fromTeamMember.getPostalCode());
	  
	  setText(getCell(sheet, 11, 6), toTeamMember.getHouseNoStreet());
	  setText(getCell(sheet, 12, 6), toTeamMember.getPreferredFirstName() + ' ' + toTeamMember.getLastName());
	  setText(getCell(sheet, 13, 6), toTeamMember.getTELUSemailAddress());
	  setText(getCell(sheet, 14, 6), toTeamMember.getManagerFirstName() + ' ' + toTeamMember.getManagerLastName());
	  setText(getCell(sheet, 15, 6), toTeamMember.getManagerEmail());
	  setText(getCell(sheet, 16, 6), toTeamMember.getManagerPhoneNumber());
	  
	  Calendar cal= Calendar.getInstance();  
	  cal.add(Calendar.DATE, 10); 
	  DateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
	  setText(getCell(sheet, 17, 6),dfDate.format(cal.getTime()));
  }
  
}