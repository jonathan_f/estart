package com.telus.estart.view;

import com.telus.estart.dao.DAOService;
import com.telus.estart.dao.interfaces.ILocalEmployeeRecordSqlmap;
import com.telus.estart.domain.Address;
import com.telus.estart.domain.LocalEmployeeRecordComment;
import com.telus.estart.domain.TeamMember;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class ExcelAssignedToMeView extends AbstractExcelView
{
	private String shippingAddress;
	private String replyToEmail;
	private String otherUserInput;
	
  protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
    throws Exception
  {
    List<TeamMember> list = (List<TeamMember>)model.get("assignedToMe");
    HSSFSheet sheet = workbook.createSheet("Records assigned to me");
    HSSFRow header = sheet.createRow(0);
    int col = 0;
    header.createCell(col++).setCellValue("Card ID");
    header.createCell(col++).setCellValue("Personnel ID");
    header.createCell(col++).setCellValue("Name");
    header.createCell(col++).setCellValue("Manager ID");
    header.createCell(col++).setCellValue("Shipping address");
    header.createCell(col++).setCellValue("Shipping URL");
    header.createCell(col++).setCellValue("Reply to Email");
    header.createCell(col++).setCellValue("Other comments");
	
    for (int i = 0; i < list.size(); i++) {
      HSSFRow row = sheet.createRow(i + 1);
      col = 0;
      TeamMember tm = (TeamMember)list.get(i);
      String tid = tm.getSAPloginID();
      
      row.createCell(col++).setCellValue("");
      row.createCell(col++).setCellValue(tid);
      row.createCell(col++).setCellValue(tm.getPreferredFirstName() + ' ' + tm.getLastName());
      row.createCell(col++).setCellValue(tm.getManagerPersonnelNumber());
      
      ILocalEmployeeRecordSqlmap localEmployeeSqlMap = DAOService.getLocalEmployeeRecordSqlmap(request);
      int courierType = localEmployeeSqlMap.getEmployeeRecord(tm.getSAPloginID()).getCourier();
      String shippingURL = getShippingURL(courierType, tid);
      
      List<LocalEmployeeRecordComment> comments = localEmployeeSqlMap.getLocalEmployeeRecordComments(tm.getSAPloginID());
      resetRecordedComments();
      processComments(comments);
      row.createCell(col++).setCellValue(shippingAddress);
      row.createCell(col++).setCellValue(shippingURL);
      row.createCell(col++).setCellValue(replyToEmail);
      row.createCell(col++).setCellValue(otherUserInput);
      
    }

  }
  
	private void resetRecordedComments() {
		shippingAddress = "";
		replyToEmail = "";
		otherUserInput = "";
	}

	private String getShippingURL(int courierType, String tid) {
	      String shippingURL = "";
	      if(courierType == -1) return shippingURL; // no shipping info
	      if(courierType == 0) { // mailroom
	    	  shippingURL = "https://security2.tsl.telus.com/estart2/mailroomlabel/" + tid;
	      } else if (courierType >= 1 && courierType <= 3) { // courier
	    	  shippingURL = "https://security2.tsl.telus.com/estart2/shippingexcel/" + tid;
	      }
	      return shippingURL;
	}
	private void processComments(List<LocalEmployeeRecordComment> comments) {
		if (comments != null) {
			for (int i = 0; i < comments.size(); i++) {
				LocalEmployeeRecordComment comment = comments.get(i);
				String commentStr = comment.getComment();

				Pattern emailPattern = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+");
				Matcher m = emailPattern.matcher(commentStr);
				if (m.find()) {
					replyToEmail = m.group();
				} else if (commentStr.contains("Shipped to")) {
					shippingAddress = commentStr;
				} else if (comment.getActive() && comment.getType() == 1) {
					otherUserInput += comment.getComment() + ", ";
				}
			}
		}
	}
  
}