package com.telus.estart.view;

import com.telus.estart.domain.TeamMember;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class ExcelReportView extends AbstractExcelView
{
  private static final String TEAM_MEMBER_LIST_LASTWEEK = "TeamMemberListLastWeek";
  private static final String TEAM_MEMBER_LIST_FUTURE = "TeamMemberListFuture";
  private static final String CONTRACTOR_LIST_LASTWEEK_TO_FUTURE = "ContractorListLastWeekToFuture";

  protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
    throws Exception
  {
    List<TeamMember> teamMemberListLastWeek = (List<TeamMember>)model.get(TEAM_MEMBER_LIST_LASTWEEK);
    List<TeamMember> teamMemberListFuture = (List<TeamMember>)model.get(TEAM_MEMBER_LIST_FUTURE);
    List<TeamMember> contractorListLastWeekToFuture = (List<TeamMember>)model.get(CONTRACTOR_LIST_LASTWEEK_TO_FUTURE);

    generateSheet(workbook, TEAM_MEMBER_LIST_LASTWEEK, teamMemberListLastWeek);
    generateSheet(workbook, TEAM_MEMBER_LIST_FUTURE, teamMemberListFuture);
    generateSheet(workbook, CONTRACTOR_LIST_LASTWEEK_TO_FUTURE, contractorListLastWeekToFuture);
  }

  private void generateSheet(HSSFWorkbook workbook, String title, List<TeamMember> list)
  {
    HSSFSheet sheet = workbook.createSheet(title);
    HSSFRow header = sheet.createRow(0);
    int col = 0;
    header.createCell(col++).setCellValue("PersonnelNumber");
    header.createCell(col++).setCellValue("SAPloginID");
    header.createCell(col++).setCellValue("FirstName");
    header.createCell(col++).setCellValue("LastName");
    header.createCell(col++).setCellValue("PreferredFirstName");
    header.createCell(col++).setCellValue("CommunicationLanguage");
    header.createCell(col++).setCellValue("TELUSemailAddress");
    header.createCell(col++).setCellValue("TaleoID");
    header.createCell(col++).setCellValue("HireRehireDate");
    header.createCell(col++).setCellValue("ServiceAnniversaryDate");
    header.createCell(col++).setCellValue("EmployeeGroup");
    header.createCell(col++).setCellValue("EmployeeGroupDescription");
    header.createCell(col++).setCellValue("EmployeeSubGroup");
    header.createCell(col++).setCellValue("EmployeeSubgroupDescription");
    header.createCell(col++).setCellValue("PersonnelSubarea");
    header.createCell(col++).setCellValue("PersonnelSubareaDescription");
    header.createCell(col++).setCellValue("CompanyCode");
    header.createCell(col++).setCellValue("PositionCode");
    header.createCell(col++).setCellValue("PositionDescription");
    header.createCell(col++).setCellValue("JobCode");
    header.createCell(col++).setCellValue("OrganizationUnit");
    header.createCell(col++).setCellValue("OrganizationUnitDescription");
    header.createCell(col++).setCellValue("CostCenter");
    header.createCell(col++).setCellValue("CLLI");
    header.createCell(col++).setCellValue("CLLIdescription");
    header.createCell(col++).setCellValue("Floor");
    header.createCell(col++).setCellValue("HouseNoStreet");
    header.createCell(col++).setCellValue("Street");
    header.createCell(col++).setCellValue("City");
    header.createCell(col++).setCellValue("Province");
    header.createCell(col++).setCellValue("PostalCode");
    header.createCell(col++).setCellValue("PhoneNumber");
    header.createCell(col++).setCellValue("JobDescription");
    header.createCell(col++).setCellValue("WorkStyle");
    header.createCell(col++).setCellValue("BusinessIdentifier");
    header.createCell(col++).setCellValue("OrgHierarchyCodeDescription");
    header.createCell(col++).setCellValue("ManagerPersonneNumber");
    header.createCell(col++).setCellValue("ManagerFirstName");
    header.createCell(col++).setCellValue("ManagerLastName");
    header.createCell(col++).setCellValue("ManagerEmail");
    header.createCell(col++).setCellValue("ManagerPhoneNumber");
    header.createCell(col++).setCellValue("NumericalPID");
    header.createCell(col++).setCellValue("DistinguishedName");
    header.createCell(col++).setCellValue("UserPrincipalName");
    header.createCell(col++).setCellValue("PSDTokenAccountID");
    header.createCell(col++).setCellValue("PSDStatus");
    header.createCell(col++).setCellValue("PSDStatusReason");
    header.createCell(col++).setCellValue("SmartCard");

    for (int i = 0; i < list.size(); i++) {
      HSSFRow row = sheet.createRow(i + 1);
      col = 0;
      TeamMember tm = (TeamMember)list.get(i);
      row.createCell(col++).setCellValue(tm.getPersonnelNumber());
      row.createCell(col++).setCellValue(tm.getSAPloginID());
      row.createCell(col++).setCellValue(tm.getFirstName());
      row.createCell(col++).setCellValue(tm.getLastName());
      row.createCell(col++).setCellValue(tm.getPreferredFirstName());
      row.createCell(col++).setCellValue(tm.getCommunicationLanguage());
      row.createCell(col++).setCellValue(tm.getTELUSemailAddress());
      row.createCell(col++).setCellValue(tm.getTaleoID());
      row.createCell(col++).setCellValue(tm.getHireRehireDate());
      row.createCell(col++).setCellValue(tm.getServiceAnniversaryDate());
      row.createCell(col++).setCellValue(tm.getEmployeeGroup());
      row.createCell(col++).setCellValue(tm.getEmployeeGroupDescription());
      row.createCell(col++).setCellValue(tm.getEmployeeSubGroup());
      row.createCell(col++).setCellValue(tm.getEmployeeSubgroupDescription());
      row.createCell(col++).setCellValue(tm.getPersonnelSubarea());
      row.createCell(col++).setCellValue(tm.getPersonnelSubareaDescription());
      row.createCell(col++).setCellValue(tm.getCompanyCode());
      row.createCell(col++).setCellValue(tm.getPositionCode());
      row.createCell(col++).setCellValue(tm.getPositionDescription());
      row.createCell(col++).setCellValue(tm.getJobCode());
      row.createCell(col++).setCellValue(tm.getOrganizationUnit());
      row.createCell(col++).setCellValue(tm.getOrganizationUnitDescription());
      row.createCell(col++).setCellValue(tm.getCostCenter());
      row.createCell(col++).setCellValue(tm.getCLLI());
      row.createCell(col++).setCellValue(tm.getCLLIdescription());
      row.createCell(col++).setCellValue(tm.getFloor());
      row.createCell(col++).setCellValue(tm.getHouseNoStreet());
      row.createCell(col++).setCellValue(tm.getCity());
      row.createCell(col++).setCellValue(tm.getProvince());
      row.createCell(col++).setCellValue(tm.getPostalCode());
      row.createCell(col++).setCellValue(tm.getPhoneNumber());
      row.createCell(col++).setCellValue(tm.getJobDescription());
      row.createCell(col++).setCellValue(tm.getWorkStyle());
      row.createCell(col++).setCellValue(tm.getBusinessIdentifier());
      row.createCell(col++).setCellValue(tm.getOrgHierarchyCodeDescription());
      row.createCell(col++).setCellValue(tm.getManagerPersonnelNumber());
      row.createCell(col++).setCellValue(tm.getManagerFirstName());
      row.createCell(col++).setCellValue(tm.getManagerLastName());
      row.createCell(col++).setCellValue(tm.getManagerEmail());
      row.createCell(col++).setCellValue(tm.getManagerPhoneNumber());
      row.createCell(col++).setCellValue(tm.getNumericalPID());

    }
  }
}