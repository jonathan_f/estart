package com.telus.estart.view;

import com.telus.estart.dao.DAOService;
import com.telus.estart.dao.interfaces.ILocalEmployeeRecordSqlmap;
import com.telus.estart.domain.TeamMember;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class ExcelCardSentTodayToMailRoomView extends AbstractExcelView
{

  protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
    throws Exception
  {
    List<TeamMember> list = (List<TeamMember>)model.get("teamMemberList");

    HSSFSheet sheet = workbook.createSheet("Records Card Sent to Mailroom today");
    HSSFRow header = sheet.createRow(0);
    int col = 0;
    header.createCell(col++).setCellValue("Card Sent Date");
    header.createCell(col++).setCellValue("Card Sent From");
    header.createCell(col++).setCellValue("Recipient ID");
    header.createCell(col++).setCellValue("Recipient Name");
    header.createCell(col++).setCellValue("Recipient Email");
    header.createCell(col++).setCellValue("Recipient Manager");
    header.createCell(col++).setCellValue("Recipient Manager Email");
    header.createCell(col++).setCellValue("Recipient Manager Phone");
    header.createCell(col++).setCellValue("Location");
    header.createCell(col++).setCellValue("Card Expiry Date");

    	
    java.util.Date now = new java.util.Date();
	DateFormat dfDate = new SimpleDateFormat("M/d/yyyy");
	String curDate = dfDate.format(now); 
	Calendar cal= Calendar.getInstance();  
	cal.add(Calendar.DATE, 10); 
	String expiryDate = dfDate.format(cal.getTime());
	
	ILocalEmployeeRecordSqlmap sqlMap = DAOService.getLocalEmployeeRecordSqlmap(request);

    for (int i = 0; i < list.size(); i++) {
      HSSFRow row = sheet.createRow(i + 1);
      col = 0;
      TeamMember tm = (TeamMember)list.get(i);
      row.createCell(col++).setCellValue(curDate);
      row.createCell(col++).setCellValue("Edmonton");
      row.createCell(col++).setCellValue(tm.getSAPloginID());
      row.createCell(col++).setCellValue(tm.getPreferredFirstName() + ' ' + tm.getLastName());
      row.createCell(col++).setCellValue(tm.getTELUSemailAddress());
      row.createCell(col++).setCellValue(tm.getManagerFirstName() + ' ' + tm.getManagerLastName());
      row.createCell(col++).setCellValue(tm.getManagerEmail());
      row.createCell(col++).setCellValue(tm.getManagerPhoneNumber());
      row.createCell(col++).setCellValue(sqlMap.getShippingMailRoomAddress(tm.getSAPloginID()));
      row.createCell(col++).setCellValue(expiryDate);
      
    }

  }
}