package com.telus.estart.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.telus.estart.domain.AdminReport;
import com.telus.estart.domain.LocalEmployeeRecord;

public class AdminExcelReportView extends AbstractExcelView {
	private static final String ADMIN_REPORT_SUMMARY = "Summary";
	private static final String ADMIN_REPORT_DETAILS = "Details";
	private static final String ADMIN_REPORT_HOLIDAYS = "Holidays";
	
	
	public AdminExcelReportView(){
		  super();
		  //load template from this location
		  setUrl("/template/adminReportTemplate");
	  }
	
	protected void buildExcelDocument(Map model, Workbook workbook,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		//AdminReport aReport = (AdminReport) model.get("AdminReport");
		List<LocalEmployeeRecord> employeeList = (List<LocalEmployeeRecord>) model
				.get("TeamMemberList");
		//generateSummarySheet(workbook, ADMIN_REPORT_SUMMARY, aReport);
		generateDetailsSheet(workbook, ADMIN_REPORT_DETAILS, employeeList);
		//workbook.createSheet(ADMIN_REPORT_HOLIDAYS);
	}

	private void generateDetailsSheet(Workbook workbook, String title,
			List<LocalEmployeeRecord> list) {
		Sheet sheet = workbook.getSheetAt(0);//get details sheet
		Row header = sheet.createRow(0);
		int col = 0;
		
		setText(getCell(sheet, 0, col++), "Type");
		setText(getCell(sheet, 0, col++), "ID");
		setText(getCell(sheet, 0, col++), "Full Name");
		setText(getCell(sheet, 0, col++), "Status");
		setText(getCell(sheet, 0, col++), "Hire Type");
		char colStartDate = letters[col];
		setText(getCell(sheet, 0, col++), "Start Date");
		char colSAPDate = letters[col];
		setText(getCell(sheet, 0, col++), "SAP Entry Date");
		char colPhotoDate = letters[col];
		setText(getCell(sheet, 0, col++), "Photo Received Date");
		char colProvisionDate = letters[col];
		setText(getCell(sheet, 0, col++), "Provision Date");
		char colDueDate = letters[col];
		setText(getCell(sheet, 0, col++), "Due Date");
		char colCompleteDate = letters[col];
		setText(getCell(sheet, 0, col++), "Complete Date");
		setText(getCell(sheet, 0, col++), "Enough HR Lead Time?");
		setText(getCell(sheet, 0, col++), "Enough Photo Lead Time?");
		setText(getCell(sheet, 0, col++), "Provisioned by Start Date?");
		setText(getCell(sheet, 0, col++), "Distribution Delay?");
		setText(getCell(sheet, 0, col++), "Back Dated?");
		setText(getCell(sheet, 0, col++), "Due Date met?");
		setText(getCell(sheet, 0, col++), "Late Reasons");
		setText(getCell(sheet, 0, col++), "Tags");


		for (int i = 0; i < list.size(); i++) {
			int rowNum = i + 2;
			Row row = sheet.createRow(i+1);
			col = 0;
			LocalEmployeeRecord record = (LocalEmployeeRecord) list.get(i);
			String employeeType;
			if (record.getType() == 1) {
				employeeType = "Team Member";
			} else {
				employeeType = "Contractor";
			}
			row.createCell(col++).setCellValue(employeeType);
			row.createCell(col++).setCellValue(record.getTid());
			row.createCell(col++).setCellValue(record.getFullName());
			row.createCell(col++).setCellValue(record.getStatusName());
			row.createCell(col++).setCellValue(record.getHireType());
			row.createCell(col++).setCellValue(record.getHireDate());
			row.createCell(col++).setCellValue(record.getCreateDate());
			row.createCell(col++).setCellValue(record.getPictureReceivedDate());
			row.createCell(col++).setCellValue(record.getCardSentDate());
			Cell dueDateCell =  row.createCell(col++);
			dueDateCell.setCellType(Cell.CELL_TYPE_FORMULA);
			dueDateCell.setCellFormula("IFERROR(IF(NETWORKDAYS($"+ colSAPDate + rowNum + "," + colStartDate + rowNum + ",Constants!D$2:D$14) >= Constants!B$5, " + colStartDate + rowNum + ", TEXT("+ colSAPDate + rowNum + " + Constants!B$2, \"YYYY-MM-DD\")),\"n/a\")");
			
			row.createCell(col++).setCellValue(record.getCompleteDate());
			
//			Cell c1a = row.createCell(col++);
//			c1a.setCellType(Cell.CELL_TYPE_FORMULA);
//			c1a.setCellFormula("NETWORKDAYS($"+ colSAPDate + rowNum + "," + colStartDate + rowNum + ")");
			
			Cell c1 = row.createCell(col++);
			c1.setCellType(Cell.CELL_TYPE_FORMULA);
			c1.setCellFormula("IFERROR(IF(NETWORKDAYS($"+ colSAPDate + rowNum + "," + colStartDate + rowNum + ",Constants!D2:D14)>=Constants!B2,\"y\",\"n\"),\"n/a\")");
			
//			Cell c2a = row.createCell(col++);
//			c2a.setCellType(Cell.CELL_TYPE_FORMULA);
//			c2a.setCellFormula("NETWORKDAYS($"+ colPhotoDate + rowNum + "," + colStartDate + rowNum + ")");
			
			Cell c2 = row.createCell(col++);
			c2.setCellType(Cell.CELL_TYPE_FORMULA);
			c2.setCellFormula("IFERROR(IF(" + colPhotoDate + rowNum + "=\"\",\"n/a\", IF(NETWORKDAYS($" + colPhotoDate + rowNum + "," + colStartDate + rowNum + ",Constants!D2:D14)>=Constants!B3,\"y\",\"n\")),\"n/a\")");
			
			Cell c3 = row.createCell(col++);
			c3.setCellType(Cell.CELL_TYPE_FORMULA);
			//c3.setCellFormula("IFERROR(IF(NETWORKDAYS($" + colProvisionDate + rowNum + "," + colStartDate + rowNum + ")>=2,\"y\",\"n\"),\"n/a\")");
			c3.setCellFormula("IFERROR(IF(" + colProvisionDate + rowNum + "=\"\",\"n/a\", IF(NETWORKDAYS($" + colProvisionDate + rowNum + "," + colStartDate + rowNum + ",Constants!D2:D14)>=Constants!B4,\"y\",\"n\")),\"n/a\")");
			
			Cell c4 = row.createCell(col++);
			c4.setCellType(Cell.CELL_TYPE_FORMULA);
			//c4.setCellFormula("IFERROR(IF(" + colCompleteDate + rowNum + ">=" +  colStartDate + rowNum + ",\"y\",\"n\"),\"n/a\")");
			c4.setCellFormula("IFERROR(IF(" + colCompleteDate + rowNum + "=\"\",\"n/a\", IF(" + colCompleteDate + rowNum + ">=" +  colStartDate + rowNum + ",\"y\",\"n\")),\"n/a\")");
			
			Cell c5 = row.createCell(col++);
			c5.setCellType(Cell.CELL_TYPE_FORMULA);
			c5.setCellFormula("IFERROR(IF(" + colSAPDate + rowNum + ">=" +  colStartDate + rowNum + ",\"y\",\"n\"),\"n/a\")");
			
			Cell dueDateMetCell = row.createCell(col++);
			dueDateMetCell.setCellType(Cell.CELL_TYPE_FORMULA);
			dueDateMetCell.setCellFormula("IFERROR(IF(" + colProvisionDate + rowNum + "=\"\",\"n/a\", IF(NETWORKDAYS($" + colProvisionDate + rowNum + "," + colDueDate + rowNum + ",Constants!D$2:D$14)>=Constants!B$4,\"y\",\"n\")),\"n/a\")");
			
			row.createCell(col++).setCellValue(generateLateReasonString(record));
			row.createCell(col++).setCellValue(record.getTags());
		}
		Sheet summary = workbook.getSheetAt(1);//get details sheet
		//summary table formulas
		for (int j = 1; j < 9; j ++) {
			for ( int k = 2; k < 34; k++) {
				setCellFormula(summary, k,j);
			}
		}
	}
	
	//helper function to load formulas from excel sheet and make it work on the generated excel
	private void setCellFormula(Sheet sheet, int row, int col) {
		Cell cell = getCell(sheet, row, col);
		try {
			String s = cell.getCellFormula();
			
			cell.setCellType(Cell.CELL_TYPE_FORMULA);
			cell.setCellFormula(s);
			
		} catch(IllegalStateException e) {
			//ignore empty cell without formula
		}
		
	}
	
	private String generateLateReasonString(LocalEmployeeRecord record) {
		String lateReason = "";
		boolean first = true;
		if(record.isLateReasonOrgManagement()) {
			if(first) {
				first = false;
			} else {
				lateReason += "/";
			}
			lateReason += "Org-Management";
		}
		if(record.isLateReasonFinance()) {
			if(first) {
				first = false;
			} else {
				lateReason += "/";
			}
			lateReason += "Finance";
		}
		if(record.isLateReasonRecruitment()) {
			if(first) {
				first = false;
			} else {
				lateReason += "/";
			}
			lateReason += "Recruitment";
		}
		if(record.isLateReasonBusiness()) {
			if(first) {
				first = false;
			} else {
				lateReason += "/";
			}
			lateReason += "Business";
		}
		if(record.isLateReasonTechnical()) {
			if(first) {
				first = false;
			} else {
				lateReason += "/";
			}
			lateReason += "Technical";
		}
		return lateReason;
	}
}