package com.telus.estart.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import com.telus.estart.domain.TeamMember;

public class ShippingCourierView extends AbstractExcelView
{
  
  public ShippingCourierView(){
	  super();
	  //load template from this location
	  setUrl("/template/Online_Distribution_ver_19");
  }
  
  protected void buildExcelDocument(Map model, Workbook workbook, HttpServletRequest request, HttpServletResponse response)
    throws Exception
  {
	  TeamMember toTeamMember = (TeamMember) model.get("toTeamMember");
	  TeamMember fromTeamMember = (TeamMember) model.get("fromTeamMember");
	  //main sheet
	  Sheet sheet = workbook.getSheetAt(0); 
	  int col = 3; //column D
	  setText(getCell(sheet, 2, col), fromTeamMember.getPreferredFirstName() + ' ' + fromTeamMember.getLastName());
	  setText(getCell(sheet, 3, col), fromTeamMember.getPhoneNumber());
	  setText(getCell(sheet, 4, col), fromTeamMember.getCLLIdescription());
	  setText(getCell(sheet, 5, col), fromTeamMember.getOrganizationUnitDescription());
	  setText(getCell(sheet, 6, col), fromTeamMember.getHouseNoStreet());
	  setText(getCell(sheet, 7, col), fromTeamMember.getFloor());
	  setText(getCell(sheet, 9, col), fromTeamMember.getCity());
	  setText(getCell(sheet, 10, col), fromTeamMember.getProvince());
	  setText(getCell(sheet, 11, col), fromTeamMember.getPostalCode());
	  
	  setText(getCell(sheet, 12, col), toTeamMember.getPreferredFirstName() + ' ' + toTeamMember.getLastName());
	  setText(getCell(sheet, 13, col), toTeamMember.getPhoneNumber());
	  setText(getCell(sheet, 14, col), toTeamMember.getCLLIdescription());
	  setText(getCell(sheet, 15, col), toTeamMember.getOrganizationUnitDescription());
	  setText(getCell(sheet, 16, col), toTeamMember.getHouseNoStreet());
	  setText(getCell(sheet, 17, col), toTeamMember.getFloor());
	  setText(getCell(sheet, 19, col), toTeamMember.getCity());
	  setText(getCell(sheet, 20, col), toTeamMember.getProvince());
	  setText(getCell(sheet, 21, col), toTeamMember.getPostalCode());
	  setText(getCell(sheet, 26, col), toTeamMember.getCostCenter());
	  java.util.Date now = new java.util.Date();
	  DateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
	  DateFormat dfTime = new SimpleDateFormat("HH:mm zzz");
	  setText(getCell(sheet, 3, 7), dfDate.format(now));
	  setText(getCell(sheet, 4, 7), dfTime.format(now));
	  // label sheet
	  sheet = workbook.getSheetAt(1); 
	  setCellFormula(sheet, 2, 2);
	  setCellFormula(sheet, 3, 2);
	  setCellFormula(sheet, 4, 2);
	  setCellFormula(sheet, 5, 2);
	  setCellFormula(sheet, 6, 2);
	  setCellFormula(sheet, 6, 4);
	  setCellFormula(sheet, 7, 2);
	  
	  setCellFormula(sheet, 11, 6);
	  setCellFormula(sheet, 12, 6);
	  setCellFormula(sheet, 13, 6);
	  setCellFormula(sheet, 14, 6);
	  setCellFormula(sheet, 15, 6);
	  setCellFormula(sheet, 15, 7);
	  setCellFormula(sheet, 16, 6);
	  setCellFormula(sheet, 17, 6);
	  
	  /*
	  setText(getCell(sheet, 2, 2), fromTeamMember.getPreferredFirstName() + ' ' + fromTeamMember.getLastName());
	  setText(getCell(sheet, 3, 2), fromTeamMember.getCLLIdescription());
	  setText(getCell(sheet, 4, 2), fromTeamMember.getFloor());
	  setText(getCell(sheet, 5, 2), fromTeamMember.getHouseNoStreet());
	  setText(getCell(sheet, 6, 2), fromTeamMember.getCity());
	  setText(getCell(sheet, 6, 4), fromTeamMember.getProvince());
	  setText(getCell(sheet, 7, 2), fromTeamMember.getPostalCode());
	  
	  setText(getCell(sheet, 11, 6), toTeamMember.getPreferredFirstName() + ' ' + toTeamMember.getLastName());
	  setText(getCell(sheet, 12, 6), toTeamMember.getCLLIdescription());
	  setText(getCell(sheet, 13, 6), toTeamMember.getFloor());
	  setText(getCell(sheet, 14, 6), toTeamMember.getHouseNoStreet());
	  setText(getCell(sheet, 15, 6), toTeamMember.getCity());
	  setText(getCell(sheet, 15, 7), toTeamMember.getProvince());
	  setText(getCell(sheet, 16, 6), toTeamMember.getPostalCode());
	  setText(getCell(sheet, 17, 6), toTeamMember.getPhoneNumber()); */
  }
  
	//helper function to load formulas from excel sheet and make it work on the generated excel
	private void setCellFormula(Sheet sheet, int row, int col) {
		Cell cell = getCell(sheet, row, col);
		String s = cell.getCellFormula();
		cell.setCellType(Cell.CELL_TYPE_FORMULA);
		cell.setCellFormula(s);
	}
	
  
}