package com.telus.estart.dao.impl;
import java.sql.SQLException;
import java.util.HashMap;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.telus.estart.dao.interfaces.ILenelIDMadapterSqlmap;
import com.telus.estart.domain.PictureUpload;

public class LenelIDMadapterSqlmap extends SqlMapClientDaoSupport implements ILenelIDMadapterSqlmap {

	private static final String UPLOAD_PICTURE = "LenelIDMadapter.uploadPicture";
	
	public void uploadPicture(String id, byte[] picture) throws SQLException {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("tid", id);
		//System.out.println(picture);
		//Blob blob=new SerialBlob(picture); 
		//org.springframework.orm.ibatis.support.BlobByteArrayTypeHandler;
//		Blob blob = oracle.sql.BLOB.getEmptyBLOB();
//		byte[] test = {-47, 1, 16, 84, 2, 101, 110, 83, 111, 109, 101, 32, 78, 70, 67, 32, 68, 97, 116, 97};
//		blob.setBytes(1, test);
		
		//blob.setBytes(test);
		//Blob blob=new SerialBlob(picture); 
		//blob.setBytes(0, picture);
		
		params.put("picture", picture);
		//System.out.println(test);
		//System.out.println(picture.length);
		PictureUpload pic = new PictureUpload();
		pic.setId(id);
		pic.setBlob(picture);
		super.getSqlMapClient().insert(UPLOAD_PICTURE, pic);
	}
}
