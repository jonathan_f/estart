package com.telus.estart.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.telus.estart.dao.interfaces.ICATSSqlmap;


public class CATSSqlmap extends SqlMapClientDaoSupport implements ICATSSqlmap {

	private static final String UPDATE_CATS_RECORD = "cats.updateEmployeeID";
	
	public void updateCATSEmployeeID(String id, String catsID) throws SQLException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", id);
		params.put("catsID", catsID);
		super.getSqlMapClient().update(UPDATE_CATS_RECORD, params);
	}
}
