package com.telus.estart.dao.impl;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.telus.estart.dao.interfaces.ITeamMemberSqlmap;
import com.telus.estart.domain.TeamMember;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.ObjectNotFoundException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class TeamMemberSqlmap extends SqlMapClientDaoSupport implements
		ITeamMemberSqlmap {
	private static final String GET_NEW_TEAM_MEMBERS = "eSAM.newTeamMembers";
	private static final String GET_TEAM_MEMBERS_STARTED_LAST_WEEK = "eSAM.teamMembersStartedLastWeek";
	private static final String GET_TEAM_MEMBER = "eSAM.getTeamMemberByID";
	private static final String GET_TEAM_MEMBER_FROM_CED = "eSAM.getTeamMemberByIDFromCED";
	
	private static final String GET_CONTRACTORS = "eSAM.newContractors";
	private static final String GET_CONTRACTOR = "eSAM.getContractorByID";
	private static final String UPDATE_CATS_ID = "eSAM.updateESamCATSID";

	public List<TeamMember> getNewTeamMembers() throws ObjectNotFoundException,
			SQLException {
		List<TeamMember> list = super.getSqlMapClient().queryForList(
				GET_NEW_TEAM_MEMBERS);
		return list;
	}

	public List<TeamMember> getTeamMembersStartedLastWeek(List<String> incompleteRecordIDs)
			throws ObjectNotFoundException, SQLException {
		List<TeamMember> list;
		if (incompleteRecordIDs.isEmpty()) {
			//System.out.println("Empty incomplete");
			list = super.getSqlMapClient().queryForList(GET_TEAM_MEMBERS_STARTED_LAST_WEEK);
		} else {
			//also get all incomplete records
			//System.out.println("incompleteRecordIDs: " + incompleteRecordIDs);
			list = super.getSqlMapClient().queryForList(GET_TEAM_MEMBERS_STARTED_LAST_WEEK, incompleteRecordIDs);
		}
		return list;
	}

	public TeamMember getTeamMember(String id) throws ObjectNotFoundException,
			SQLException {
		return (TeamMember) super.getSqlMapClient().queryForObject(
				GET_TEAM_MEMBER, id);
	}
	
	public TeamMember getTeamMemberFromCED(String id)
			throws ObjectNotFoundException, SQLException {
		return (TeamMember) super.getSqlMapClient().queryForObject(
				GET_TEAM_MEMBER_FROM_CED, id);
	}
	
	
	public List<TeamMember> getContractorsFromLastWeekToFuture(List<String> incompleteRecordIDs)
			throws ObjectNotFoundException, SQLException {
		List<TeamMember> list;
		//convert xid to numerical format
		List<String> convertedIDs = new LinkedList<String>();
		for(String s : incompleteRecordIDs) {
			convertedIDs.add(s.substring(1));//remove first char 'X'
		}
		
		if (incompleteRecordIDs.isEmpty()) {
			list = super.getSqlMapClient().queryForList(GET_CONTRACTORS);
		} else {
			//System.out.println(convertedIDs);
			list = super.getSqlMapClient().queryForList(GET_CONTRACTORS, convertedIDs);
		}
		return list;
	}
	
	public TeamMember getContractor(String id) throws ObjectNotFoundException,
		SQLException {
		return (TeamMember) super.getSqlMapClient().queryForObject(
				GET_CONTRACTOR, id);
	}
	
	public void updateESamCATSID(String id, String catsID) throws SQLException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", id);
		params.put("catsID", catsID);
		super.getSqlMapClient().update(UPDATE_CATS_ID, params);
	}
}