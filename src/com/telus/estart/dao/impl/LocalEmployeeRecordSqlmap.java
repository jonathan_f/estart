package com.telus.estart.dao.impl;


import com.telus.estart.dao.interfaces.ILocalEmployeeRecordSqlmap;
import com.telus.estart.domain.Address;
import com.telus.estart.domain.CATSRecord;
import com.telus.estart.domain.LocalEmployeeRecord;
import com.telus.estart.domain.LocalEmployeeRecordComment;
import com.telus.estart.domain.LocalEmployeeRecordStatus;
import com.telus.estart.domain.MailroomLocation;
import com.telus.estart.domain.TeamMember;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.ObjectNotFoundException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class LocalEmployeeRecordSqlmap extends SqlMapClientDaoSupport implements
		ILocalEmployeeRecordSqlmap {
	private static final String GET_EMPLOYEE_RECORDS = "employee.getEmployeeRecords";
	private static final String GET_EMPLOYEE_RECORDS_BY_DATE = "employee.getEmployeeRecordsByDate";
	
	private static final String GET_EMPLOYEE_RECORD = "employee.getEmployeeRecordByID";
	
	private static final String ADD_EMPLOYEE_RECORD = "employee.addEmployeeRecord";
	private static final String REMOVE_EMPLOYEE_RECORD = "employee.removeEmployeeRecord";
	
	private static final String EDIT_EMPLOYEE_RECORD = "employee.updateEmployeeRecord";

	private static final String IS_EMPLOYEE_RECORD_EXIST = "employee.isEmployeeRecordExist";
	private static final String GET_INCOMPLETE_EMPLOYEE_RECORDS = "employee.getIncompleteTeamMemberEmployeeRecords";
	private static final String GET_INCOMPLETE_CONTRACTOR_RECORDS = "employee.getIncompleteContractorEmployeeRecords";
	private static final String GET_CARD_SENT_RECORDS_TODAY = "employee.getCardSentRecordsToday";
	private static final String GET_ASSIGNED_TO_ME_RECORD_IDS = "employee.getAssignedToMeRecordIDs";
	
	private static final String GET_EMPLOYEE_RECORD_STATUS_HISTORY = "employee.getEmployeeRecordsStatusHistory";
	private static final String ADD_EMPLOYEE_RECORD_STATUS = "employee.addEmployeeRecordStatusHistory";
	private static final String HAS_STATUS_IN_HISTORY = "employee.hasStatusInHistory";
	
	private static final String GET_EMPLOYEE_RECORD_COMMENTS = "employee.getEmployeeRecordsComments";
	private static final String ADD_EMPLOYEE_RECORD_COMMENT = "employee.addEmployeeRecordComment";
	
	private static final String GET_CATS_RECORDS_BY_ID = "employee.get_cats_records_by_id";
	
	private static final String DEACTIVATE_PREVIOUS_EMPLOYEE_RECORD_COMMENTS = "employee.deactivatePreviousEmployeeRecordComments";
	private static final String DEACTIVATE_PREVIOUS_EMPLOYEE_RECORD_HISTORY = "employee.deactivatePreviousEmployeeRecordHistory";
	
	private static final String ADD_SHIPPING_COURRIER_ADDRESS = "employee.add_shipping_courier_address";
	private static final String GET_SHIPPING_COURRIER_ADDRESS_BY_ID = "employee.get_shipping_courier_address_by_id";
	
	private static final String GET_SHIPPING_MAILROOM_LOCATIONS = "employee.get_shipping_mailroom_locations";
	private static final String ADD_SHIPPING_MAILROOM_ADDRESS = "employee.add_shipping_mailroom_address";
	private static final String GET_SHIPPING_MAILROOM_ADDRESS_BY_ID = "employee.get_shipping_mailroom_address_by_id";

	
	/**
	 * updateEmployeeRecord
	 * updateLateEmployeeRecordDates
	 * updateLateEmployeeRecordEHireDates
	 * deactivatePreviousEmployeeRecordComments
	 * deactivatePreviousEmployeeRecordHistory
	 * 
	 */
	
	@SuppressWarnings("unchecked")
	public List<LocalEmployeeRecord> getLocalEmployeeRecords(List<TeamMember> teamMembers) 
			throws ObjectNotFoundException, SQLException {
		List<LocalEmployeeRecord> list = super.getSqlMapClient().queryForList(
				GET_EMPLOYEE_RECORDS, generateIDListString(teamMembers));
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<LocalEmployeeRecord> getEmployeeRecordsByDate(Date startDate, Date endDate) 
			throws ObjectNotFoundException, SQLException {
		HashMap<String, String> map = new HashMap<String, String>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		map.put("startDate", df.format(startDate));
		map.put("endDate", df.format(endDate));
		List<LocalEmployeeRecord> list = super.getSqlMapClient().queryForList(
				GET_EMPLOYEE_RECORDS_BY_DATE, map);
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getIncompleteEmployeeRecords() throws ObjectNotFoundException,
			SQLException {
		List<String> list = super.getSqlMapClient().queryForList(
				GET_INCOMPLETE_EMPLOYEE_RECORDS);
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getIncompleteContractorRecords() throws ObjectNotFoundException,
			SQLException {
		List<String> list = super.getSqlMapClient().queryForList(
				GET_INCOMPLETE_CONTRACTOR_RECORDS);
		//System.out.println(list);
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getCardSentRecordsToday() throws ObjectNotFoundException, SQLException {
		List<String> list = super.getSqlMapClient().queryForList(
				GET_CARD_SENT_RECORDS_TODAY);
		//System.out.println(list);
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getAssignedToMeRecordIDs(String agentName) throws ObjectNotFoundException, SQLException {
		List<String> list = super.getSqlMapClient().queryForList(
				GET_ASSIGNED_TO_ME_RECORD_IDS, agentName);
		return list;
	}

	public void addLocalEmployeeRecord(LocalEmployeeRecord e)
			throws ObjectNotFoundException, SQLException {
		super.getSqlMapClient().insert(ADD_EMPLOYEE_RECORD, e);
	}
	
	public void removeLocalEmployeeRecord(String id) throws ObjectNotFoundException, SQLException {
		super.getSqlMapClient().delete(REMOVE_EMPLOYEE_RECORD, id);
	}
	
	
	public void editEmployeeRecord(LocalEmployeeRecord e)
			throws ObjectNotFoundException, SQLException {
		//System.out.println("Status : " + e.getStatus());
		super.getSqlMapClient().update(EDIT_EMPLOYEE_RECORD, e);
	}
	
	
	public void deactivatePreviousEmployeeRecord(String id)
			throws ObjectNotFoundException, SQLException {
		super.getSqlMapClient().update(DEACTIVATE_PREVIOUS_EMPLOYEE_RECORD_HISTORY, id);
		super.getSqlMapClient().update(DEACTIVATE_PREVIOUS_EMPLOYEE_RECORD_COMMENTS, id);
	}
	

	@SuppressWarnings("unchecked")
	public Boolean isLocalEmployeeRecordExist(String id)
			throws ObjectNotFoundException, SQLException {
		List<String> list = super.getSqlMapClient().queryForList(
				IS_EMPLOYEE_RECORD_EXIST, id);
		if (list.size() > 0)
			return Boolean.valueOf(true);
		return Boolean.valueOf(false);
	}
	
	public LocalEmployeeRecord getEmployeeRecord(String id) throws SQLException {
		return  (LocalEmployeeRecord) super.getSqlMapClient().queryForObject(
				GET_EMPLOYEE_RECORD, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<LocalEmployeeRecordStatus> getLocalEmployeeRecordStatusHistory(String tid)  
			throws ObjectNotFoundException, SQLException {
		List<LocalEmployeeRecordStatus> list = super.getSqlMapClient().queryForList(
				GET_EMPLOYEE_RECORD_STATUS_HISTORY, tid);
		return list;
	}
	
	public void addLocalEmployeeRecordStatusToHistory(LocalEmployeeRecordStatus s)
			throws ObjectNotFoundException, SQLException {
		super.getSqlMapClient().insert(ADD_EMPLOYEE_RECORD_STATUS, s);
	}
	
	public Boolean hasStatusInHistory(LocalEmployeeRecordStatus s) 
			throws ObjectNotFoundException, SQLException {
		@SuppressWarnings("unchecked")
		List<String> list = super.getSqlMapClient().queryForList(
				HAS_STATUS_IN_HISTORY, s);
		if (list.size() > 0)
			return Boolean.valueOf(true);
		return Boolean.valueOf(false);
	}
	
	@SuppressWarnings("unchecked")
	public List<LocalEmployeeRecordComment> getLocalEmployeeRecordComments(String tid)
		throws ObjectNotFoundException, SQLException {
		List<LocalEmployeeRecordComment> list = super.getSqlMapClient().queryForList(
				GET_EMPLOYEE_RECORD_COMMENTS, tid);
		return list;
	}
	
	public void addLocalEmployeeRecordComment(LocalEmployeeRecordComment c)
			throws ObjectNotFoundException, SQLException {
		super.getSqlMapClient().insert(ADD_EMPLOYEE_RECORD_COMMENT, c);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CATSRecord> getCATSRecordsByID(String id)  
			throws ObjectNotFoundException, SQLException {
		String xid = id.toLowerCase();
		List<CATSRecord> list = super.getSqlMapClient().queryForList(
				GET_CATS_RECORDS_BY_ID, xid);
		return list;
	}
	
	public void addShippingCourierAddress(
			Address address) throws ObjectNotFoundException,
			SQLException {
		super.getSqlMapClient().insert(ADD_SHIPPING_COURRIER_ADDRESS, address);
	}
	
	public  Address getShippingCourierAddressByID(String id)  
			throws ObjectNotFoundException, SQLException {
		return  (Address) super.getSqlMapClient().queryForObject(
				GET_SHIPPING_COURRIER_ADDRESS_BY_ID, id);
	}
	
	public List<String> getMailRoomLocations() throws ObjectNotFoundException, SQLException {
		@SuppressWarnings("unchecked")
		List<String> list = super.getSqlMapClient().queryForList(
				GET_SHIPPING_MAILROOM_LOCATIONS);
		return list;
	}
	
	public void addShippingMailRoomAddress(MailroomLocation m) throws ObjectNotFoundException, SQLException {
		super.getSqlMapClient().insert(ADD_SHIPPING_MAILROOM_ADDRESS, m);
	}
	
	public String getShippingMailRoomAddress(String id) throws ObjectNotFoundException, SQLException {
		return  (String) super.getSqlMapClient().queryForObject(
				GET_SHIPPING_MAILROOM_ADDRESS_BY_ID, id);
	}
	
	private List<String> generateIDListString(List<TeamMember> list) {
		if (list.isEmpty()) {
			return null;
		}
		List<String> strings = new LinkedList<String>();
		for (TeamMember t : list) {
			strings.add(t.getSAPloginID());
		}
		return strings;
	}
}