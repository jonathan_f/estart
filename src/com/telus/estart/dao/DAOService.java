package com.telus.estart.dao;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.telus.estart.dao.interfaces.ICATSSqlmap;
import com.telus.estart.dao.interfaces.ILenelIDMadapterSqlmap;
import com.telus.estart.dao.interfaces.ILocalEmployeeRecordSqlmap;
import com.telus.estart.dao.interfaces.ITeamMemberSqlmap;
import com.telus.framework.dao.ITeamMemberDao;

import javax.servlet.http.HttpServletRequest;

import oracle.jdbc.driver.OracleConnection;
import oracle.sql.BLOB;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbcp.DelegatingConnection;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class DAOService {
	private static OracleConnection oracleConnection = null;
	
	//eStart team member object map
	public static ITeamMemberSqlmap getTeamMemberSqlMap(
			HttpServletRequest request) {
		ApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(request.getSession()
						.getServletContext());
		return (ITeamMemberSqlmap) context.getBean("eSAMDao");
	}
	//framework team member object map
	public static ITeamMemberDao getTeamMemberDao(
			HttpServletRequest request) {
		ApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(request.getSession()
						.getServletContext());
		return (ITeamMemberDao) context.getBean("TeamMemberDao");
	}
	

	public static ILocalEmployeeRecordSqlmap getLocalEmployeeRecordSqlmap(
			HttpServletRequest request) {
		ApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(request.getSession()
						.getServletContext());
		
		return (ILocalEmployeeRecordSqlmap) context
				.getBean("localEmployeeRecordDao");
	}

	public static ICATSSqlmap getCATSSqlmap(HttpServletRequest request) {
		ApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(request.getSession()
						.getServletContext());
		return (ICATSSqlmap) context.getBean("CATSDao");
	}
	
	public static FileInputStream getShippingExcelTemplate(HttpServletRequest request) {
		ApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(request.getSession()
						.getServletContext());
		return (FileInputStream) context.getBean("shippingExcelTemplate");
	}
  
	@SuppressWarnings({ "finally", "deprecation" })
	public static boolean uploadPictureToIDMAdapter(HttpServletRequest request,
			String id, byte[] picture) throws SQLException {
		if (oracleConnection == null) {
			BasicDataSource dataSource = (BasicDataSource) WebApplicationContextUtils
					.getWebApplicationContext(
							request.getSession().getServletContext()).getBean(
							"dataSourceLenelIDMadapter");
			DelegatingConnection del = new DelegatingConnection(dataSource.getConnection());
			oracleConnection = (OracleConnection) del.getInnermostDelegate();
		}
		PreparedStatement createStatement = null;
		PreparedStatement updateStatement = null;
		boolean success = false;
		//insert the record with a empty blob to hold the place
		try {
			createStatement = oracleConnection
					.prepareStatement("INSERT INTO TRANSACT_MYPHOTOID_PICS (EMP_ID,PICTURE) VALUES(?,?)");
			createStatement.setString(1, id);
			createStatement.setBlob(2, oracle.sql.BLOB.empty_lob());
			createStatement.executeUpdate();
			//System.out.println("#################### sql update start");
			updateStatement = oracleConnection
					.prepareStatement("SELECT PICTURE FROM TRANSACT_MYPHOTOID_PICS WHERE EMP_ID = " + id + " FOR UPDATE");
			oracleConnection.setAutoCommit(false);
			ResultSet rset = updateStatement.executeQuery();
			rset.next();
			BLOB pictureBLOB = (BLOB) rset.getBlob("PICTURE");
			OutputStream outstream = pictureBLOB.getBinaryOutputStream();
			outstream.write(picture);
			outstream.flush();
			outstream.close();
			oracleConnection.commit();
			//System.out.println("################## blob is written ");
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} // JBoss wrapped connection
		finally {
			if(createStatement != null) createStatement.close();
			//if(oracleConnection != null) oracleConnection.close();
			return success;
		}
	
  }
  
}