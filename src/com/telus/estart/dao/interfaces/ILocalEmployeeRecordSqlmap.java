package com.telus.estart.dao.interfaces;

import com.telus.estart.domain.Address;
import com.telus.estart.domain.CATSRecord;
import com.telus.estart.domain.LocalEmployeeRecord;
import com.telus.estart.domain.LocalEmployeeRecordComment;
import com.telus.estart.domain.LocalEmployeeRecordStatus;
import com.telus.estart.domain.MailroomLocation;
import com.telus.estart.domain.TeamMember;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.ejb.ObjectNotFoundException;

public abstract interface ILocalEmployeeRecordSqlmap {
	public abstract List<LocalEmployeeRecord> getLocalEmployeeRecords(
			List<TeamMember> paramList) throws ObjectNotFoundException,
			SQLException;
	public abstract List<LocalEmployeeRecord> getEmployeeRecordsByDate(Date startDate, Date endDate) 
			throws ObjectNotFoundException, SQLException;

	public abstract List<String> getIncompleteEmployeeRecords()
			throws ObjectNotFoundException, SQLException;
	
	public abstract List<String> getIncompleteContractorRecords()
			throws ObjectNotFoundException, SQLException;
	
	public abstract List<String> getCardSentRecordsToday() throws ObjectNotFoundException, SQLException;

	public abstract List<String> getAssignedToMeRecordIDs(String agentName) throws ObjectNotFoundException, SQLException;

	public abstract void addLocalEmployeeRecord(
			LocalEmployeeRecord paramLocalEmployeeRecord)
			throws ObjectNotFoundException, SQLException;
	
	public abstract void removeLocalEmployeeRecord(String id) throws ObjectNotFoundException, SQLException;
	
	public abstract void editEmployeeRecord(LocalEmployeeRecord e)
			throws ObjectNotFoundException, SQLException;
	
	public abstract void deactivatePreviousEmployeeRecord(String id)
			throws ObjectNotFoundException, SQLException;
	
	public abstract Boolean isLocalEmployeeRecordExist(String paramString)
			throws ObjectNotFoundException, SQLException;

	public abstract LocalEmployeeRecord getEmployeeRecord(String id)
			throws SQLException;

	public abstract List<LocalEmployeeRecordStatus> getLocalEmployeeRecordStatusHistory(
			String tid) throws ObjectNotFoundException, SQLException;

	public abstract void addLocalEmployeeRecordStatusToHistory(
			LocalEmployeeRecordStatus s) throws ObjectNotFoundException,
			SQLException;
	
	public abstract Boolean hasStatusInHistory(LocalEmployeeRecordStatus s) throws ObjectNotFoundException,
	SQLException;

	public abstract List<LocalEmployeeRecordComment> getLocalEmployeeRecordComments(
			String tid) throws ObjectNotFoundException, SQLException;

	public abstract void addLocalEmployeeRecordComment(
			LocalEmployeeRecordComment c) throws ObjectNotFoundException, SQLException;

	public abstract List<CATSRecord> getCATSRecordsByID(String id)
			throws ObjectNotFoundException, SQLException;

	public abstract void addShippingCourierAddress(Address address)
			throws ObjectNotFoundException, SQLException;

	public abstract Address getShippingCourierAddressByID(String id)
			throws ObjectNotFoundException, SQLException;
	
	public abstract List<String> getMailRoomLocations() throws ObjectNotFoundException, SQLException;
	
	public abstract void addShippingMailRoomAddress(MailroomLocation m) throws ObjectNotFoundException, SQLException;
	
	public abstract String getShippingMailRoomAddress(String id) throws ObjectNotFoundException, SQLException;
	
	
}