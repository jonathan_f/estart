package com.telus.estart.dao.interfaces;

import java.sql.SQLException;

public abstract interface ICATSSqlmap {
	public abstract void updateCATSEmployeeID(String id, String catsID) throws SQLException;
}