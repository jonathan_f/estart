package com.telus.estart.dao.interfaces;

import java.sql.Blob;
import java.sql.SQLException;

public abstract interface ILenelIDMadapterSqlmap {
	public abstract void uploadPicture(String id, byte[] bs) throws SQLException;
}