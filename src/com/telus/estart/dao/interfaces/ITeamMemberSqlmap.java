package com.telus.estart.dao.interfaces;

import com.telus.estart.domain.TeamMember;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.ObjectNotFoundException;

public abstract interface ITeamMemberSqlmap {
	public abstract List<TeamMember> getNewTeamMembers()
			throws ObjectNotFoundException, SQLException;

	public abstract List<TeamMember> getTeamMembersStartedLastWeek(
			List<String> incompleteRecordIDs) throws ObjectNotFoundException,
			SQLException;

	public abstract TeamMember getTeamMember(String id)
			throws ObjectNotFoundException, SQLException;
	
	public abstract TeamMember getTeamMemberFromCED(String id)
			throws ObjectNotFoundException, SQLException;

	public abstract List<TeamMember> getContractorsFromLastWeekToFuture(List<String> incompleteRecordIDs)
			throws ObjectNotFoundException, SQLException;

	public abstract TeamMember getContractor(String id)
			throws ObjectNotFoundException, SQLException;
	
	public abstract void updateESamCATSID(String id, String catsID) throws SQLException;
}