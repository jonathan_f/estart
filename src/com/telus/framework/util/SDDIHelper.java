package com.telus.framework.util;

import java.sql.SQLException;

import javax.ejb.ObjectNotFoundException;
import javax.servlet.http.HttpServletRequest;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.telus.framework.dao.TeamMemberDao;
import com.telus.framework.dbobject.ITeamMember;

public class SDDIHelper {

     public static ITeamMember getTeamMemberByNumericalPID(String numericalPID) throws ObjectNotFoundException, SQLException {
          TeamMemberDao teamMemberDao = (TeamMemberDao) SDDIHelper.getApplicationContext().getBean("TeamMemberDao");
          return teamMemberDao.getTeamMemberByNumericalPID(numericalPID);
     }

     public static int getCurrentTeamMemberNumericalPIDFromRequest(HttpServletRequest request) {
    	 //String uid = (String) request.getSession().getAttribute("customSSOFilter_uid");
    	 
    	TeamMemberDao teamMemberDao = (TeamMemberDao) SDDIHelper.getApplicationContext().getBean("TeamMemberDao");
     	String uid = (String) request.getSession().getAttribute("customSSOFilter_email");
  		ITeamMember tm;
 		try {
 			
 			tm = teamMemberDao.getTeamMemberByEmail(uid);
 			//System.out.println(uid + tm.getPreferredFirstName() + " " + tm.getLastName());
 			
 			if( tm == null) { // can't find the user's email. maybe due to email address changes in LDAP: the user needs to fix his email address by contacting spoc
 				uid = (String) request.getSession().getAttribute("customSSOFilter_uid");
 			}
 			
 			
 			return Integer.parseInt(tm.getNumericalPID());
 		} catch (ObjectNotFoundException e) {
 			// TODO Auto-generated catch block
 			
 			e.printStackTrace();
 			return 0;
 		} catch (SQLException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 			return 0;
 		}

     }

     public static String getCurrentTeamMemberTIDFromRequest(HttpServletRequest request) {
    	 String uid = (String) request.getSession().getAttribute("customSSOFilter_uid");
          return uid;

     }
     
     public static String getCurrentTeamMemberNameFromRequest(HttpServletRequest request) {
    	TeamMemberDao teamMemberDao = (TeamMemberDao) SDDIHelper.getApplicationContext().getBean("TeamMemberDao");
    	String uid = (String) request.getSession().getAttribute("customSSOFilter_email");
 		ITeamMember tm;
		try {
			
			tm = teamMemberDao.getTeamMemberByEmail(uid);
			//System.out.println(uid + tm.getPreferredFirstName() + " " + tm.getLastName());
			return tm.getPreferredFirstName() + " " + tm.getLastName();
		} catch (ObjectNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			return "";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
 		
     }
     
     public static String getUserEmail(HttpServletRequest request) {
 		String email = (String) request.getSession().getAttribute("customSSOFilter_email");
 		return email.toLowerCase();
 	}

     public static Object getSpringBean(String beanName) {
          return SDDIHelper.getApplicationContext().getBean(beanName);
     }

     public static ApplicationContext getApplicationContext() {
          return ApplicationContextProvider.getApplicationContext();
     }

     public static String sanitize(String dirty) {
          return StringSanitizer.sanitize(dirty);
     }

     public static String TIdToNumericalPID(String id) {
    	 
    	 String TId = id.toLowerCase();
    	 if (!(TId.startsWith("t") || TId.startsWith("x"))){
    		 return "";
    	 }
    	 
         while (TId.startsWith("t") || TId.startsWith("x") || TId.startsWith("0")) {
               TId = TId.substring(1, TId.length());
         }
         return TId;
     }

}
