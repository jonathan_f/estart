package com.telus.framework.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * You should use SDDIHelper to obtain the Application Context instead of this class
 * A static way to expose Spring Application Context.  AppContext Variable is populated via Spring Injection.
 * @author Kalun Ho (t855054)
 *
 */
public class ApplicationContextProvider implements ApplicationContextAware {

	private static ApplicationContext applicationContext = null;
	
	@Override
	public void setApplicationContext(ApplicationContext appContext)
			throws BeansException {
			applicationContext = appContext;
	}
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

}
