package com.telus.framework.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * You should use SDDIHelper to sanitize String instead of this class
 * @author Copied from TFA Framework - Kalun Ho (t855054)
 *
 */
public class StringSanitizer {

	/**
	 * This Pattern includes all possible new line characters
	 */
	public static final Pattern ANY_NEWLINE = Pattern.compile("\r|\n|\r\n|\u0085|\u2028|\u2029");

	/**
	 * This Pattern allows simple HTML tags that cannot be exploited for cross
	 * site scripting.
	 */
	public static final Pattern SIMPLE_TAGS = Pattern.compile("<\\s*/?\\s*(" +
		
			"[Bb]|" +		// <B>
			"[Ii]|" +		// <I>
			"[Uu]|" +		// <U>
			"[Pp]|" +		// <P>
			"[Hh]\\d|" +	// <H#>, eg. <H1>
			"[Bb][Rr]\\s*/?|" +	// <BR>
			"[Hh][Rr]\\s*/?|" +	// <HR>
			"[Aa](\\s+[Hh][Rr][Ee][Ff]\\s*=\\s*[\"'][^\"']*[\"'])?|" +		// Links
			"[Ii][Mm][Gg]\\s+[Ss][Rr][Cc]\\s*=\\s*[\"'][^\"']*[\"']\\s*/" +	// Images
		
		")\\s*>");
			
	/**
	 * Unvalidated input String
	 */
	private String input;
	
	/**
	 * Booleans matching the chars in the string to mark whether or not it has
	 * been checked
	 */
	private boolean[] checkedChars;
	
	public StringSanitizer(String input) {
		this.input = input;
		this.checkedChars = new boolean[input.length()];
	}
	
	/**
	 * Allow the &lt;B&gt; and &lt;/B&gt; tags
	 */
	public void checkTagB() {
		checkTag(Pattern.compile("<\\s*/?\\s*[Bb]\\s*>"));
	}
	
	/**
	 * Allow the &lt;I&gt; and &lt;/I&gt; tags
	 */
	public void checkTagI() {
		checkTag(Pattern.compile("<\\s*/?\\s*[Ii]\\s*>"));
	}
	
	/**
	 * Allow the &lt;BR&gt; tag
	 */
	public void checkTagBR() {
		checkTag(Pattern.compile("<\\s*/?\\s*[Bb][Rr]\\s*>"));
	}
	
	/**
	 * Allow the &lt;H#&gt; to &lt;/H#&gt; tags (&lt;H0&gt to &lt;H9&gt)
	 */
	public void checkTagH() {
		checkTag(Pattern.compile("<\\s*/?\\s*[Hh]\\d\\s*>"));
	}
	
	/**
	 * Mark indexes in the input String as checked. Regions that match the
	 * specified Pattern are deemed checked.
	 * 
	 * @param p The Pattern used to mark checked tags
	 */
	public void checkTag(Pattern p) {
		Matcher m = p.matcher(input);
		
		while (m.find()) {			// Find next match
			int idx = m.start();

			while (idx < m.end()) {	// Mark all chars in region as checked
				checkedChars[idx++] = true;
			}
		}
	}
	
	/**
	 * Construct a String that sanitizes potentially malicious characters.
	 * Tags that were checked and deemed safe with be left untouched.
	 * 
	 * @return An escaped version of the string (leaving tags that were checked)
	 */
	public String getSanitizedString() {
		StringBuilder sb = new StringBuilder();
		
		char[] chars = input.toCharArray();
		
		for (int i = 0; i < chars.length; i++) {
			char c = chars[i];
			
			if (checkedChars[i])	// Already checked, no need to sanitize
				sb.append(c);
			
			else if (c == '<')
				sb.append("&lt;");
			
			else if (c == '>')
				sb.append("&gt;");
			
			else if (c == '\'')
				sb.append("&#39;");	// The entity number is used instead of the name for IE compatibility
			
			else if (c == '"')
				sb.append("&quot;");
			
			else
				sb.append(c);		// Char is safe, no need to sanitize
		}
		
		return sb.toString();
	}
	
	/**
	 * Sanitize input of all potentially malicious tags. Tags specified in
	 * SIMPLE_TAGS are allowed.
	 * 
	 * @param input
	 * @return The input with all potentially malicious tags escaped
	 */
	public static String sanitize(String input) {
		StringSanitizer ss = new StringSanitizer(input);
		
		ss.checkTag(SIMPLE_TAGS);
		
		return ss.getSanitizedString();
	}
}
