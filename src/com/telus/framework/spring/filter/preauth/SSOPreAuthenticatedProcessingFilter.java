package com.telus.framework.spring.filter.preauth;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

/**
* CustomSSOFilter Pre-auth authenticator - This skips password check and use SSO for authentication
* @author Kalun Ho (t855054)
*
*/
public class SSOPreAuthenticatedProcessingFilter extends RequestHeaderAuthenticationFilter {

	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {

		return request.getSession().getAttribute("customSSOFilter_email");

	}

}

