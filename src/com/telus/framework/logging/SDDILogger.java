package com.telus.framework.logging;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.telus.framework.dao.ILoggerDao;
import com.telus.framework.util.SDDIHelper;
import com.telus.framework.util.StringSanitizer;

/**
 * This is the central logger for SDDI application. All application and admin
 * logging should go through this class.
 * 
 * @author Kalun Ho (t855054), modified from TFA Framework
 * 
 */
public class SDDILogger {

	static Log myLogger = LogFactory.getLog(SDDILogger.class);

	public static enum EVENT_CODE {
		DEBUG("Debug"), // Use if you wish to write information that may only be
						// interesting for debugging purposes
		INFO("Info"), // Intended to be used for auditing purposes--any actions
						// that permanently change some data state should use
						// this
		WARN("Warning"), // Can be used when something unexpected occurred but
							// may not be an error
		ERROR("Error"),
		
		WORKLOG("Work Log"); // Errors, Exceptions
		
		private String eventCode;

		EVENT_CODE(String eventCode) {
			this.eventCode = eventCode;
		}

		public String toString() {
			return eventCode;
		}

	};

	/**
	 * This is the standard log method that most people will use.
	 * 
	 * @param message
	 * @param level
	 * @param category
	 * @param url
	 * @param userid
	 * @param log_file
	 * @param log_db
	 */
	public static Integer log(String message, SDDILogger.EVENT_CODE level,
			String url, String userid,
			boolean log_file, boolean log_db) {
		message = cleanMessage(message);

		if (log_file) {
			if (level.toString().equals(EVENT_CODE.DEBUG.toString())) {
				myLogger.debug("url: " + url 
						+ " userid " + userid + " message " + message);

			}

			else if (level.toString().equals(EVENT_CODE.INFO.toString())) {
				myLogger.info("url: " + url 
						+ " userid " + userid + " message " + message);
			}

			else if (level.toString().equals(EVENT_CODE.WARN.toString())) {
				myLogger.warn("url: " + url 
						+ " userid " + userid + " message " + message);
			}

			else if (level.toString().equals(EVENT_CODE.ERROR.toString())) {
				myLogger.error("url: " + url 
						+ " userid " + userid + " message " + message);
			}
			else if (level.toString().equals(EVENT_CODE.WORKLOG.toString())) {
				myLogger.info("url: " + url 
						+ " userid " + userid + " message " + message);
			}
		}

		if (log_db) {
			if (level.toString().equals(EVENT_CODE.DEBUG.toString())) {
				return writeDatabase(cleanMessage(message), level,
						url, userid);
			}

			else if (level.toString().equals(EVENT_CODE.INFO.toString())) {
				return writeDatabase(cleanMessage(message), level,
						url, userid);
			}

			else if (level.toString().equals(EVENT_CODE.WARN.toString())) {
				return writeDatabase(cleanMessage(message), level,
						url, userid);
			}

			else if (level.toString().equals(EVENT_CODE.ERROR.toString())) {
				return writeDatabase(cleanMessage(message), level,
						url, userid);
			}
			
			else if (level.toString().equals(EVENT_CODE.WORKLOG.toString())) {
				return writeDatabase(cleanMessage(message), level,
						url, userid);
			}
		}
		return -1;
	}

	/**
	 * Log Exception to Database - Should use this to log most exceptions
	 * 
	 * @param exception
	 *            pass in the thrown exception
	 * @param request
	 *            HttpSerlvetRequest
	 * @return Log ID number
	 */
	public static Integer logSimpleException(Throwable exception,
			HttpServletRequest request) {
		return SDDILogger
				.logException(exception, request.getRequestURI(), SDDIHelper
						.getCurrentTeamMemberNumericalPIDFromRequest(request)+"",
						true, true);
	}
	
	/**
	 * Log WorkLog to Database - Should use this to log most exceptions
	 * 
	 * @param exception
	 *            pass in the thrown exception
	 * @param request
	 *            HttpSerlvetRequest
	 * @return Log ID number
	 */
	public static Integer logSimpleWorkLog(String workLogDescription,
			HttpServletRequest request) {
		return SDDILogger
				.logWorklog(workLogDescription, request.getRequestURI(), SDDIHelper
						.getCurrentTeamMemberNumericalPIDFromRequest(request)+"",
						true, true);
	}

	/**
	 * Log Info to Database - Should use this to log most exceptions
	 * 
	 * @param exception
	 *            pass in the thrown exception
	 * @param request
	 *            HttpSerlvetRequest
	 * @return Log ID number
	 */
	public static Integer logSimpleInfo(String workLogDescription,
			HttpServletRequest request) {
		return SDDILogger
				.logInfo(workLogDescription, request.getRequestURI(), SDDIHelper
						.getCurrentTeamMemberNumericalPIDFromRequest(request)+"",
						true, true);
	}
	

	/**
	 * This is a convenient method for logging any throwable.
	 * 
	 * @param exception
	 * @param exceptionUrl
	 * @param log_file
	 * @param log_db
	 * @param email_error
	 */
	public static Integer logException(Throwable exception,
			String exceptionUrl, String userid, boolean log_file, boolean log_db) {
		if (log_file && exception != null) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(outputStream);
			exception.printStackTrace((PrintStream) ps);
			String stackTrace = new String(outputStream.toByteArray());
			myLogger.error(exception.getMessage() + " " + stackTrace);
			return SDDILogger.log(stackTrace, SDDILogger.EVENT_CODE.ERROR,
					exceptionUrl, userid,
					log_file, log_db);
		}
		return -1;
	}
	
	/**
	 * This is a convenient method for logging any worklog.
	 * 
	 * @param worklogDescription
	 * @param worklogURL
	 * @param log_file
	 * @param log_db
	 * @param email_error
	 */
	public static Integer logWorklog(String workLogDescription,
			String workLogUrl, String userid, boolean log_file, boolean log_db) {
		if (log_file && workLogDescription != null) {
			myLogger.error(workLogDescription);
			return SDDILogger.log(workLogDescription, SDDILogger.EVENT_CODE.WORKLOG,
					workLogUrl, userid,
					log_file, log_db);
		}
		return -1;
	}
	
	/**
	 * This is a convenient method for logging any info messages.
	 * 
	 * @param worklogDescription
	 * @param worklogURL
	 * @param log_file
	 * @param log_db
	 * @param email_error
	 */
	public static Integer logInfo(String workLogDescription,
			String workLogUrl, String userid, boolean log_file, boolean log_db) {
		if (log_file && workLogDescription != null) {
			myLogger.info(workLogDescription);
			return SDDILogger.log(workLogDescription, SDDILogger.EVENT_CODE.INFO,
					workLogUrl, userid,
					log_file, log_db);
		}
		return -1;
	}


	/**
	 * Add a tab to any newline characters in the message. This prevents log
	 * forging by explicitly identifying all newlines in one log entry.
	 * 
	 * @param message
	 * @return Original String with tabs prepended to any newlines
	 */
	public static String cleanMessage(String message) {
		if (message == null) {
			return "";
		}

		Matcher m = StringSanitizer.ANY_NEWLINE.matcher(message);

		StringBuffer sb = new StringBuffer(message.length());

		while (m.find()) { // Find next match
			m.appendReplacement(sb, ""); // Move append position to next match
			sb.append(m.group() + "\t"); // Insert original text appended with a
											// tab
		}
		m.appendTail(sb);

		return sb.toString();
	}

	private static Integer writeDatabase(String message,
			SDDILogger.EVENT_CODE level,
			String url, String userid) {
		Integer logKey = -1;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("EventExecutionStep", url);
		paramMap.put("EventDescription", message);
		paramMap.put("EventCode", level.toString());
		paramMap.put("EventSource", userid);

		try {
			ILoggerDao dao = (ILoggerDao) SDDIHelper.getApplicationContext()
					.getBean("LoggerDao");
			logKey = dao.insert(paramMap);

		} catch (Exception e) {
			e.printStackTrace();
			myLogger.info("failed to write to db");
		}

		return logKey;
	}
	
}