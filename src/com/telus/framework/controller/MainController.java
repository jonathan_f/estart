package com.telus.framework.controller;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.ObjectNotFoundException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.telus.framework.dao.IUserDao;
import com.telus.framework.dbobject.IUser;

/**
 * MainController
 * 
 * Serves up the requested page in WEB-INF/jsp
 * 
 * @author Kalun Ho (t855054)
 */
@Controller
public class MainController {

	/**
	 * Forward all page request automatically
	 * 
	 * @param URI
	 * @return lookup string in tiles.xml under WEB-INF
	 */
	@RequestMapping("/{page}")
	public String redirect(@PathVariable(value = "page") String page) {
		return page;
	}
	
	@RequestMapping("/error-403")
	public String display(Model model, HttpServletRequest request)
			throws SQLException {
		return "error-403";
	}

}
