package com.telus.framework.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.telus.framework.dao.LoggerDao;
import com.telus.framework.dbobject.ILog;
import com.telus.framework.util.SDDIHelper;

@Controller
public class LogController {

	/**
	 * Return logs in JSON format
	 * 
	 * @param request
	 *            An instance of HttpSerlvetRequest
	 * @return Response code for JQuery Datatable Object
	 * @throws SQLException
	 */
	@RequestMapping(value = "/log/getLog", method = RequestMethod.GET)
	public @ResponseBody String getLog(HttpServletRequest request, HttpServletResponse response) {
		
		int start = Integer.parseInt(request.getParameter("iDisplayStart"));
		int length = Integer.parseInt(request.getParameter("iDisplayLength"));
		String sortingCol = request.getParameter("iSortCol_0");
		String sortingDir = request.getParameter("sSortDir_0");
		String searchTerm = request.getParameter("sSearch");
		LoggerDao loggerDao =(LoggerDao) SDDIHelper.getSpringBean("LoggerDao");
		
		
		List<ILog> logs = loggerDao.getLog(start, length, sortingCol, sortingDir, searchTerm);
		Integer totalRecords = loggerDao.getAllLogCount();
		Integer totalDisplayRecords = 0;
		if (searchTerm.isEmpty()) {
			totalDisplayRecords = totalRecords;
		} else {
			totalDisplayRecords = loggerDao.getLogCount(searchTerm);
		}
		
		
		Gson gson = new Gson();
		StringBuffer jsonResponse = new StringBuffer();
		jsonResponse.append("{ \"iTotalRecords\" : " + totalRecords);
		jsonResponse.append(", \"iTotalDisplayRecords\" : " + totalDisplayRecords);
		jsonResponse.append(", \"aaData\" : [");
		for (ILog log: logs) {
			jsonResponse.append(gson.toJson(log).replace("{", "{ \"DT_RowID\": \"" + log.getId() + "\", "));
			jsonResponse.append(", ");
		}
		if (logs.size() > 0) {
			jsonResponse.deleteCharAt(jsonResponse.lastIndexOf(","));
		}
		jsonResponse.append(" ] }");
		return jsonResponse.toString();
		
	}
	
}
