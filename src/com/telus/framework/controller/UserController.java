package com.telus.framework.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.ObjectNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.Gson;
import com.telus.framework.dao.ITeamMemberDao;
import com.telus.framework.dao.IUserDao;
import com.telus.framework.dbobject.ITeamMember;
import com.telus.framework.dbobject.IUser;
import com.telus.framework.dbobject.TeamMember;
import com.telus.framework.dbobject.User;
import com.telus.framework.util.SDDIHelper;

/**
 * A Controller for the Admin console
 * 
 * @author Kalun Ho (t855054)
 * 
 */
@Controller
public class UserController {

	/**
	 * Display All Users of the application
	 * 
	 * @param model
	 *            An instance of org.springframework.ui.Model
	 * @param request
	 *            An instance of HttpSerlvetRequest
	 * @return String that maps to a specific page
	 * @throws SQLException
	 */
	@RequestMapping("/users")
	public String display(Model model, HttpServletRequest request)
			throws SQLException {

		IUserDao userDao = this.getUserDao(request);

		try {
			List<IUser> users = userDao.getAllUsers();
			List<String> permissions = userDao.getAllPermissions();
			model.addAttribute("Users", users);
			model.addAttribute("Permissions", permissions);
		} catch (ObjectNotFoundException e) {
			// log error
			e.printStackTrace();
		}
		return "users";
	}

	/**
	 * Delete a user of the application
	 * 
	 * @param request
	 *            An instance of HttpSerlvetRequest
	 * @return Response code for JQuery Datatable Object
	 * @throws SQLException
	 */
	@RequestMapping(value = "/users/delete", method = RequestMethod.POST)
	public @ResponseBody
	String deleteUser(HttpServletRequest request, HttpServletResponse response) {
		String email = request.getParameter("id");
		IUserDao userDao = this.getUserDao(request);
		IUser user = (IUser)SDDIHelper.getSpringBean("User");
		user.setEmail(email);
		try {
			userDao.deleteUser(user);
			return "ok";
		} catch (ConcurrencyFailureException e) {
			response.setStatus(HttpStatus.SC_EXPECTATION_FAILED);
			return "Error - Delete Failed";
		} catch (SQLException e) {
			response.setStatus(HttpStatus.SC_EXPECTATION_FAILED);
			return "Error - Delete Failed";
		}
	}

	/**
	 * Insert a user of the application
	 * 
	 * @param request
	 *            An instance of HttpSerlvetRequest
	 * @return Response code for JQuery Datatable Object
	 * @throws SQLException
	 */
	@RequestMapping(value = "/users/insert", method = RequestMethod.POST)
	public @ResponseBody
	String addUser(HttpServletRequest request, HttpServletResponse response) {

		List<String> permissions = new ArrayList<String>();
		String email = request.getParameter("email");
		boolean enabled = Boolean.parseBoolean(request.getParameter("enabled"));
		String permissionsArray = request.getParameter("permissions");
		Collections.addAll(permissions, permissionsArray.split(";"));

		IUser user = (IUser)SDDIHelper.getSpringBean("User");
		user.setEmail(email);
		user.setEnabled(enabled);
		user.setPermissions(permissions);

		IUserDao userDao = this.getUserDao(request);

		try {
			userDao.addUser(user);
			return user.getEmail();
		} catch (ConcurrencyFailureException e) {
			response.setStatus(HttpStatus.SC_EXPECTATION_FAILED);
			return "Error - Insert Failed";
		} catch (DuplicateKeyException e) {
			response.setStatus(HttpStatus.SC_CONFLICT);
			return "Error - User Already Exist";
		} catch (SQLException e) {
			response.setStatus(HttpStatus.SC_CONFLICT);
			return "Error - User Already Exist";
		}
	}

	/**
	 * Update a user of the application
	 * 
	 * @param request
	 *            An instance of HttpSerlvetRequest
	 * @return Response code for JQuery Datatable Object
	 * @throws SQLException
	 */
	@RequestMapping(value = "/users/update", method = RequestMethod.POST)
	public @ResponseBody
	String updateUser(HttpServletRequest request, HttpServletResponse response) {

		String column = request.getParameter("columnId");
		String email = request.getParameter("id");
		String value = request.getParameter("value");
		IUserDao userDao = this.getUserDao(request);
		IUser user = (IUser)SDDIHelper.getSpringBean("User");
		user.setEmail(email);

		try {

			if (Integer.parseInt(column) == 1) {
				user.setEnabled(Boolean.parseBoolean(value));
				userDao.updateUserEnabled(user);
			} else {
				List<String> permissions = new ArrayList<String>();
				Collections.addAll(permissions, value.split(";"));
				user.setPermissions(permissions);
				userDao.updateUserPermissions(user);

			}

		} catch (DuplicateKeyException e) {
			response.setStatus(HttpStatus.SC_CONFLICT);
			return "Error - User already exist";
		} catch (ConcurrencyFailureException e) {
			response.setStatus(HttpStatus.SC_EXPECTATION_FAILED);
			return "Error - Update Failed";
		} catch (SQLException e) {
			response.setStatus(HttpStatus.SC_CONFLICT);
			return "Error - Update Failed";
		}

		return value;

	}

	/**
	 * Suggest a list of employee given their ID
	 * 
	 * @param request
	 * @return JSON response string
	 * @throws SQLException
	 */
	@RequestMapping(value = "/users/autocomplete", method = RequestMethod.POST)
	public @ResponseBody
	String autocompleteUser(HttpServletRequest request) throws SQLException {
		ApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(request.getSession()
						.getServletContext());
		ITeamMemberDao teamMemberDao = (ITeamMemberDao) context
				.getBean("TeamMemberDao");
		String searchString = request.getParameter("name_startsWith");
		Gson gson = new Gson();
		try {
			List<ITeamMember> teamMembers = teamMemberDao
					.autoComplete(searchString);
			return gson.toJson(teamMembers);
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
		}

		return "";
	}

	private IUserDao getUserDao(HttpServletRequest request) {
		ApplicationContext context = WebApplicationContextUtils
				.getWebApplicationContext(request.getSession()
						.getServletContext());
		return (IUserDao) context.getBean("UserDao");
	}

}
