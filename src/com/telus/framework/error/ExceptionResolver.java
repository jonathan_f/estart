package com.telus.framework.error;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.telus.framework.logging.SDDILogger;
import com.telus.framework.util.SDDIHelper;

/**
 * Catches all exception thrown by the controller (hence, you don't need to use try / catch statement to handle exceptions)
 * Handles any exception except for error 404 and 500.
 * @author Kalun Ho (t855054)
 *
 */
public class ExceptionResolver implements HandlerExceptionResolver {

	/**
	 * Log error events and redirect to error page
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object object, Exception exception) {
		
		String eventSource = SDDIHelper.getCurrentTeamMemberNumericalPIDFromRequest(request)+"";
		
		if (eventSource == null) {
			eventSource = "system";
		}
		
		Integer errorTicketNumber = SDDILogger.logException(exception, request.getRequestURI(), eventSource, true, true);
			
		Map<String, Object> param = new HashMap<String, Object>();
		
		param.put("errorTicketNumber", errorTicketNumber);
		
		return new ModelAndView("error", param);
	}

}
