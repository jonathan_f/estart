package com.telus.framework.error;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;

import com.telus.framework.logging.SDDILogger;

/**
 * Handles all 403, access denied error
 * @author Kalun Ho (t855054)
 *
 */
public class SDDIAccessDeniedHandler implements org.springframework.security.web.access.AccessDeniedHandler {

	private String accessDeniedUrl;
	
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException exception) throws IOException, ServletException {
		SDDILogger.logSimpleException(exception, request);
		response.sendRedirect(accessDeniedUrl);
	}

    public String getAccessDeniedUrl() {
        return accessDeniedUrl;
    }

    public void setAccessDeniedUrl(String accessDeniedUrl) {
        this.accessDeniedUrl = accessDeniedUrl;
    }
	
}
