package com.telus.framework.dbobject;

import java.util.Date;

public interface ILog {

	public abstract Integer getId();

	public abstract void setId(Integer id);

	public abstract String getExecutionStep();

	public abstract void setExecutionStep(String executionStep);

	public abstract String getDescription();

	public abstract void setDescription(String description);

	public abstract String getSource();

	public abstract void setSource(String source);

	public abstract String getCode();

	public abstract void setCode(String code);

	public abstract Date getDateTime();

	public abstract void setDateTime(Date dateTime);

}