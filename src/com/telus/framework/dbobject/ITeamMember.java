package com.telus.framework.dbobject;

import java.sql.Timestamp;

public interface ITeamMember {

	public abstract int getIdCEDdata();

	public abstract void setIdCEDdata(int idCEDdata);

	public abstract String getOperation();

	public abstract void setOperation(String operation);

	public abstract Timestamp getOperationDate();

	public abstract void setOperationDate(Timestamp operationDate);

	public abstract String getDataSourceCode();

	public abstract void setDataSourceCode(String dataSourceCode);

	public abstract String getPersonnelNumber();

	public abstract void setPersonnelNumber(String personnelNumber);

	public abstract String getSapUserID();

	public abstract void setSapUserID(String sapUserID);

	public abstract String getLastName();

	public abstract void setLastName(String lastName);

	public abstract String getFirstName();

	public abstract void setFirstName(String firstName);

	public abstract String getPreferredFirstName();

	public abstract void setPreferredFirstName(String preferredFirstName);

	public abstract String getCompanyCode();

	public abstract void setCompanyCode(String companyCode);

	public abstract String getCompanyDescription();

	public abstract void setCompanyDescription(String companyDescription);

	public abstract String getPositionCode();

	public abstract void setPositionCode(String positionCode);

	public abstract String getPositionCodeTitle();

	public abstract void setPositionCodeTitle(String positionCodeTitle);

	public abstract String getCostCentre();

	public abstract void setCostCentre(String costCentre);

	public abstract String getOrganizationUnitCode();

	public abstract void setOrganizationUnitCode(String organizationUnitCode);

	public abstract String getOrganizationUnitTitle();

	public abstract void setOrganizationUnitTitle(String organizationUnitTitle);

	public abstract String getRoomNumber();

	public abstract void setRoomNumber(String roomNumber);

	public abstract String getBuildingCLLI();

	public abstract void setBuildingCLLI(String buildingCLLI);

	public abstract String getWorkAddressBuildingName();

	public abstract void setWorkAddressBuildingName(
			String workAddressBuildingName);

	public abstract String getWorkAddressStreet();

	public abstract void setWorkAddressStreet(String workAddressStreet);

	public abstract String getWorkAddressPostalCode();

	public abstract void setWorkAddressPostalCode(String workAddressPostalCode);

	public abstract String getWorkAddressCity();

	public abstract void setWorkAddressCity(String workAddressCity);

	public abstract String getWorkAddressProvince();

	public abstract void setWorkAddressProvince(String workAddressProvince);

	public abstract String getWorkAddressCountry();

	public abstract void setWorkAddressCountry(String workAddressCountry);

	public abstract String getWorkAddressPhoneNumber();

	public abstract void setWorkAddressPhoneNumber(String workAddressPhoneNumber);

	public abstract String getAlternatePhone();

	public abstract void setAlternatePhone(String alternatePhone);

	public abstract String getFax();

	public abstract void setFax(String fax);

	public abstract String getPager();

	public abstract void setPager(String pager);

	public abstract String getCell();

	public abstract void setCell(String cell);

	public abstract String getHomePhone();

	public abstract void setHomePhone(String homePhone);

	public abstract String getManagerSourceProvince();

	public abstract void setManagerSourceProvince(String managerSourceProvince);

	public abstract String getManagerPersonnelNumber();

	public abstract void setManagerPersonnelNumber(String managerPersonnelNumber);

	public abstract String getManagerName();

	public abstract void setManagerName(String managerName);

	public abstract String getEmailAddress();

	public abstract void setEmailAddress(String emailAddress);

	public abstract String getNtUserID();

	public abstract void setNtUserID(String ntUserID);

	public abstract String getRacfID();

	public abstract void setRacfID(String racfID);

	public abstract String getSapReleaseCode();

	public abstract void setSapReleaseCode(String sapReleaseCode);

	public abstract String getContractor();

	public abstract void setContractor(String contractor);

	public abstract String getCrisID();

	public abstract void setCrisID(String crisID);

	public abstract String getEvpOrgCode();

	public abstract void setEvpOrgCode(String evpOrgCode);

	public abstract String getManagementProfessional();

	public abstract void setManagementProfessional(String managementProfessional);

	public abstract String getExternalToTELUS();

	public abstract void setExternalToTELUS(String externalToTELUS);

	public abstract String getNumericalPID();

	public abstract void setNumericalPID(String numericalPID);

	public abstract String getNumericalSAPID();

	public abstract void setNumericalSAPID(String numericalSAPID);

}