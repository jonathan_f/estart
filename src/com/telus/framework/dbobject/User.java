package com.telus.framework.dbobject;

import java.util.List;

/**
 * User for this particular application.
 * @author Kalun Ho (t855054)
 *
 */
public class User implements IUser {

	private String email;
	private boolean enabled;
	private List<String> permissions;
	

	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.IUser#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return enabled;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.IUser#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.IUser#getPermissions()
	 */
	@Override
	public List<String> getPermissions() {
		return permissions;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.IUser#setPermissions(java.util.List)
	 */
	@Override
	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}
	@Override
	public String getEmail() {
		// TODO Auto-generated method stub
		return email.toLowerCase();
	}
	@Override
	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email = email;
	}
	
	
}
