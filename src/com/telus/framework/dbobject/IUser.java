package com.telus.framework.dbobject;

import java.util.List;

public interface IUser {

	public abstract String getEmail();

	public abstract void setEmail(String email);
		
	public abstract boolean isEnabled();

	public abstract void setEnabled(boolean enabled);

	public abstract List<String> getPermissions();

	public abstract void setPermissions(List<String> permissions);

}