package com.telus.framework.dbobject;

import java.util.Date;

public class Log implements ILog {

	private Integer id;
	private String executionStep;
	private String description;
	private String source;
	private String code;
	private Date dateTime;
	
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#setId(java.lang.Integer)
	 */
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#getExecutionStep()
	 */
	@Override
	public String getExecutionStep() {
		return executionStep;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#setExecutionStep(java.lang.String)
	 */
	@Override
	public void setExecutionStep(String executionStep) {
		this.executionStep = executionStep;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#getSource()
	 */
	@Override
	public String getSource() {
		return source;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#setSource(java.lang.String)
	 */
	@Override
	public void setSource(String source) {
		this.source = source;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#getCode()
	 */
	@Override
	public String getCode() {
		return code;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#getDateTime()
	 */
	@Override
	public Date getDateTime() {
		return dateTime;
	}
	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ILog#setDateTime(java.util.Date)
	 */
	@Override
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
}
