package com.telus.framework.dbobject;

import java.sql.Timestamp;

/**
 * A class to hold CED records. The idea here is *not* to accurately type the different variables (almost all are String). 
 * We get this data from a flat text file, we correlate existing records in AccessRecords by matching a single field 
 * (PersonnelID) and we export the data back to a text file - so really, we do not care about variable types.
 * 
 * @author t805959
 *
 */
public class TeamMember implements ITeamMember {

	public enum searchBy { 	byAccessProdID, byRecordTypeID, byPersonnelID, bySAPID, byLastName, byMgrPersonnelID, byContractInitiative,
		byMgrSAPID, byProvinceID,byContractNumber,byContractYear,byVendorName,byLastModified};
		
	/*
	 * Fields as per CEDdata table
	 */
	private int idCEDdata; 
	private String operation;			
	private Timestamp operationDate;		
	private String dataSourceCode;
	private String personnelNumber;
	private String sapUserID;
	private String lastName;
	private String firstName;
	private String preferredFirstName;
	private String companyCode;
	private String companyDescription;
	private String positionCode;
	private String positionCodeTitle;
	private String costCentre;
	private String organizationUnitCode;
	private String organizationUnitTitle;
	private String roomNumber;
	private String buildingCLLI;
	private String workAddressBuildingName;
	private String workAddressStreet;
	private String workAddressPostalCode;
	private String workAddressCity;
	private String workAddressProvince;
	private String workAddressCountry;
	private String workAddressPhoneNumber;
	private String alternatePhone;
	private String fax;
	private String pager;
	private String cell;
	private String homePhone;
	private String managerSourceProvince;
	private String managerPersonnelNumber;
	private String managerName;
	private String emailAddress;
	private String ntUserID;
	private String racfID;
	private String sapReleaseCode;
	private String contractor;
	private String crisID;
	private String evpOrgCode;
	private String managementProfessional;
	private String externalToTELUS;
	private String numericalPID;
	private String numericalSAPID;
	

	/**
	 * The main constructor for this class
	 * @param myidCEDdata
	 * @param myOperation
	 * @param myOperationDate
	 * @param myDataSourceCode
	 * @param myPersonnelNumber
	 * @param mySAPUserID
	 * @param myLastName
	 * @param myFirstName
	 * @param myPreferredFirstName
	 * @param myCompanyCode
	 * @param myCompanyDescription
	 * @param myPositionCode
	 * @param myPositionCodeTitle
	 * @param myCostCentre
	 * @param myOrganizationUnitCode
	 * @param myOrganizationUnitTitle
	 * @param myRoomNumber
	 * @param myBuildingCLLI
	 * @param myWorkAddressBuildingName
	 * @param myWorkAddressStreet
	 * @param myWorkAddressPostalCode
	 * @param myWorkAddressCity
	 * @param myWorkAddressProvince
	 * @param myWorkAddressCountry
	 * @param myWorkAddressPhoneNumber
	 * @param myAlternatePhone
	 * @param myFax
	 * @param myPager
	 * @param myCell
	 * @param myHomePhone
	 * @param myManagerSourceProvince
	 * @param myManagerPersonnelNumber
	 * @param myManagerName
	 * @param myEmailAddress
	 * @param myNTUserID
	 * @param myRACFID
	 * @param mySAPReleaseCode
	 * @param myContractor
	 * @param myCRISID
	 * @param myEVPOrgCode
	 * @param myManagementProfessional
	 * @param myExternalToTELUS
	 */
	public TeamMember ( int myidCEDdata, String myOperation, Timestamp myOperationDate, String myDataSourceCode,
			String myPersonnelNumber,String mySAPUserID,String myLastName,String myFirstName,String myPreferredFirstName,
			String myCompanyCode,String myCompanyDescription,String myPositionCode,String myPositionCodeTitle,
			String myCostCentre,String myOrganizationUnitCode,String myOrganizationUnitTitle,String myRoomNumber,
			String myBuildingCLLI,String myWorkAddressBuildingName,String myWorkAddressStreet,String myWorkAddressPostalCode,
			String myWorkAddressCity,String myWorkAddressProvince,String myWorkAddressCountry,String myWorkAddressPhoneNumber,
			String myAlternatePhone,String myFax,String myPager,String myCell,String myHomePhone,String myManagerSourceProvince,
			String myManagerPersonnelNumber,String myManagerName,String myEmailAddress,String myNTUserID,String myRACFID,
			String mySAPReleaseCode,String myContractor,String myCRISID,String myEVPOrgCode,String myManagementProfessional,
			String myExternalToTELUS, String myNumericalPID, String myNumericalSAPID) {
			
	idCEDdata = myidCEDdata;
	operation = myOperation;
	operationDate = myOperationDate;
	dataSourceCode = myDataSourceCode;
	personnelNumber = myPersonnelNumber;
	sapUserID = mySAPUserID;
	lastName = myLastName;
	firstName = myFirstName;
	preferredFirstName = myPreferredFirstName;
	companyCode = myCompanyCode;
	companyDescription = myCompanyDescription;
	positionCode = myPositionCode;
	positionCodeTitle = myPositionCodeTitle;
	costCentre = myCostCentre;
	organizationUnitCode = myOrganizationUnitCode;
	organizationUnitTitle = myOrganizationUnitTitle;
	roomNumber = myRoomNumber;
	buildingCLLI = myBuildingCLLI;
	workAddressBuildingName = myWorkAddressBuildingName;
	workAddressStreet = myWorkAddressStreet;
	workAddressPostalCode = myWorkAddressPostalCode;
	workAddressCity = myWorkAddressCity;
	workAddressProvince = myWorkAddressProvince;
	workAddressCountry = myWorkAddressCountry;
	workAddressPhoneNumber = myWorkAddressPhoneNumber;
	alternatePhone = myAlternatePhone;
	fax = myFax;
	pager = myPager;
	cell = myCell;
	homePhone = myHomePhone;
	managerSourceProvince = myManagerSourceProvince;
	managerPersonnelNumber = myManagerPersonnelNumber;
	managerName = myManagerName;
	emailAddress = myEmailAddress;
	ntUserID = myNTUserID;
	racfID = myRACFID;
	sapReleaseCode = mySAPReleaseCode;
	contractor = myContractor;
	crisID = myCRISID;
	evpOrgCode = myEVPOrgCode;
	managementProfessional = myManagementProfessional;
	externalToTELUS = myExternalToTELUS;
	numericalPID = myNumericalPID;
	numericalSAPID = myNumericalSAPID;
}
	
	
	/**
	 * A default constructor that creates a totally blank record
	 */
	public TeamMember() {
		idCEDdata					= -1;
		operation					= "";
		operationDate				= null;
		dataSourceCode				= "";
		personnelNumber				= "";
		sapUserID					= "";
		lastName					= "";
		firstName					= "";
		preferredFirstName			= "";
		companyCode					= "";
		companyDescription			= "";
		positionCode				= "";
		positionCodeTitle			= "";
		costCentre					= "";
		organizationUnitCode		= "";
		organizationUnitTitle		= "";
		roomNumber					= "";
		buildingCLLI				= "";
		workAddressBuildingName		= "";
		workAddressStreet			= "";
		workAddressPostalCode		= "";
		workAddressCity				= "";
		workAddressProvince			= "";
		workAddressCountry			= "";
		workAddressPhoneNumber		= "";
		alternatePhone				= "";
		fax							= "";
		pager						= "";
		cell						= "";
		homePhone					= "";
		managerSourceProvince		= "";
		managerPersonnelNumber		= "";
		managerName					= "";
		emailAddress				= "";
		ntUserID					= "";
		racfID						= "";
		sapReleaseCode				= "";
		contractor					= "";
		crisID						= "";
		evpOrgCode					= "";
		managementProfessional		= "";
		externalToTELUS				= "";
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getIdCEDdata()
	 */
	@Override
	public int getIdCEDdata() {
		return idCEDdata;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setIdCEDdata(int)
	 */
	@Override
	public void setIdCEDdata(int idCEDdata) {
		this.idCEDdata = idCEDdata;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getOperation()
	 */
	@Override
	public String getOperation() {
		return operation;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setOperation(java.lang.String)
	 */
	@Override
	public void setOperation(String operation) {
		this.operation = operation;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getOperationDate()
	 */
	@Override
	public Timestamp getOperationDate() {
		return operationDate;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setOperationDate(java.sql.Timestamp)
	 */
	@Override
	public void setOperationDate(Timestamp operationDate) {
		this.operationDate = operationDate;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getDataSourceCode()
	 */
	@Override
	public String getDataSourceCode() {
		return dataSourceCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setDataSourceCode(java.lang.String)
	 */
	@Override
	public void setDataSourceCode(String dataSourceCode) {
		this.dataSourceCode = dataSourceCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getPersonnelNumber()
	 */
	@Override
	public String getPersonnelNumber() {
		return personnelNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setPersonnelNumber(java.lang.String)
	 */
	@Override
	public void setPersonnelNumber(String personnelNumber) {
		this.personnelNumber = personnelNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getSapUserID()
	 */
	@Override
	public String getSapUserID() {
		return sapUserID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setSapUserID(java.lang.String)
	 */
	@Override
	public void setSapUserID(String sapUserID) {
		this.sapUserID = sapUserID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getLastName()
	 */
	@Override
	public String getLastName() {
		return lastName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setLastName(java.lang.String)
	 */
	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getFirstName()
	 */
	@Override
	public String getFirstName() {
		return firstName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setFirstName(java.lang.String)
	 */
	@Override
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getPreferredFirstName()
	 */
	@Override
	public String getPreferredFirstName() {
		return preferredFirstName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setPreferredFirstName(java.lang.String)
	 */
	@Override
	public void setPreferredFirstName(String preferredFirstName) {
		this.preferredFirstName = preferredFirstName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getCompanyCode()
	 */
	@Override
	public String getCompanyCode() {
		return companyCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setCompanyCode(java.lang.String)
	 */
	@Override
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getCompanyDescription()
	 */
	@Override
	public String getCompanyDescription() {
		return companyDescription;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setCompanyDescription(java.lang.String)
	 */
	@Override
	public void setCompanyDescription(String companyDescription) {
		this.companyDescription = companyDescription;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getPositionCode()
	 */
	@Override
	public String getPositionCode() {
		return positionCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setPositionCode(java.lang.String)
	 */
	@Override
	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getPositionCodeTitle()
	 */
	@Override
	public String getPositionCodeTitle() {
		return positionCodeTitle;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setPositionCodeTitle(java.lang.String)
	 */
	@Override
	public void setPositionCodeTitle(String positionCodeTitle) {
		this.positionCodeTitle = positionCodeTitle;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getCostCentre()
	 */
	@Override
	public String getCostCentre() {
		return costCentre;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setCostCentre(java.lang.String)
	 */
	@Override
	public void setCostCentre(String costCentre) {
		this.costCentre = costCentre;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getOrganizationUnitCode()
	 */
	@Override
	public String getOrganizationUnitCode() {
		return organizationUnitCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setOrganizationUnitCode(java.lang.String)
	 */
	@Override
	public void setOrganizationUnitCode(String organizationUnitCode) {
		this.organizationUnitCode = organizationUnitCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getOrganizationUnitTitle()
	 */
	@Override
	public String getOrganizationUnitTitle() {
		return organizationUnitTitle;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setOrganizationUnitTitle(java.lang.String)
	 */
	@Override
	public void setOrganizationUnitTitle(String organizationUnitTitle) {
		this.organizationUnitTitle = organizationUnitTitle;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getRoomNumber()
	 */
	@Override
	public String getRoomNumber() {
		return roomNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setRoomNumber(java.lang.String)
	 */
	@Override
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getBuildingCLLI()
	 */
	@Override
	public String getBuildingCLLI() {
		return buildingCLLI;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setBuildingCLLI(java.lang.String)
	 */
	@Override
	public void setBuildingCLLI(String buildingCLLI) {
		this.buildingCLLI = buildingCLLI;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getWorkAddressBuildingName()
	 */
	@Override
	public String getWorkAddressBuildingName() {
		return workAddressBuildingName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setWorkAddressBuildingName(java.lang.String)
	 */
	@Override
	public void setWorkAddressBuildingName(String workAddressBuildingName) {
		this.workAddressBuildingName = workAddressBuildingName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getWorkAddressStreet()
	 */
	@Override
	public String getWorkAddressStreet() {
		return workAddressStreet;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setWorkAddressStreet(java.lang.String)
	 */
	@Override
	public void setWorkAddressStreet(String workAddressStreet) {
		this.workAddressStreet = workAddressStreet;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getWorkAddressPostalCode()
	 */
	@Override
	public String getWorkAddressPostalCode() {
		return workAddressPostalCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setWorkAddressPostalCode(java.lang.String)
	 */
	@Override
	public void setWorkAddressPostalCode(String workAddressPostalCode) {
		this.workAddressPostalCode = workAddressPostalCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getWorkAddressCity()
	 */
	@Override
	public String getWorkAddressCity() {
		return workAddressCity;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setWorkAddressCity(java.lang.String)
	 */
	@Override
	public void setWorkAddressCity(String workAddressCity) {
		this.workAddressCity = workAddressCity;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getWorkAddressProvince()
	 */
	@Override
	public String getWorkAddressProvince() {
		return workAddressProvince;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setWorkAddressProvince(java.lang.String)
	 */
	@Override
	public void setWorkAddressProvince(String workAddressProvince) {
		this.workAddressProvince = workAddressProvince;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getWorkAddressCountry()
	 */
	@Override
	public String getWorkAddressCountry() {
		return workAddressCountry;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setWorkAddressCountry(java.lang.String)
	 */
	@Override
	public void setWorkAddressCountry(String workAddressCountry) {
		this.workAddressCountry = workAddressCountry;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getWorkAddressPhoneNumber()
	 */
	@Override
	public String getWorkAddressPhoneNumber() {
		return workAddressPhoneNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setWorkAddressPhoneNumber(java.lang.String)
	 */
	@Override
	public void setWorkAddressPhoneNumber(String workAddressPhoneNumber) {
		this.workAddressPhoneNumber = workAddressPhoneNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getAlternatePhone()
	 */
	@Override
	public String getAlternatePhone() {
		return alternatePhone;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setAlternatePhone(java.lang.String)
	 */
	@Override
	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getFax()
	 */
	@Override
	public String getFax() {
		return fax;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setFax(java.lang.String)
	 */
	@Override
	public void setFax(String fax) {
		this.fax = fax;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getPager()
	 */
	@Override
	public String getPager() {
		return pager;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setPager(java.lang.String)
	 */
	@Override
	public void setPager(String pager) {
		this.pager = pager;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getCell()
	 */
	@Override
	public String getCell() {
		return cell;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setCell(java.lang.String)
	 */
	@Override
	public void setCell(String cell) {
		this.cell = cell;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getHomePhone()
	 */
	@Override
	public String getHomePhone() {
		return homePhone;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setHomePhone(java.lang.String)
	 */
	@Override
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getManagerSourceProvince()
	 */
	@Override
	public String getManagerSourceProvince() {
		return managerSourceProvince;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setManagerSourceProvince(java.lang.String)
	 */
	@Override
	public void setManagerSourceProvince(String managerSourceProvince) {
		this.managerSourceProvince = managerSourceProvince;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getManagerPersonnelNumber()
	 */
	@Override
	public String getManagerPersonnelNumber() {
		return managerPersonnelNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setManagerPersonnelNumber(java.lang.String)
	 */
	@Override
	public void setManagerPersonnelNumber(String managerPersonnelNumber) {
		this.managerPersonnelNumber = managerPersonnelNumber;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getManagerName()
	 */
	@Override
	public String getManagerName() {
		return managerName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setManagerName(java.lang.String)
	 */
	@Override
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getEmailAddress()
	 */
	@Override
	public String getEmailAddress() {
		return emailAddress.toLowerCase();
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setEmailAddress(java.lang.String)
	 */
	@Override
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getNtUserID()
	 */
	@Override
	public String getNtUserID() {
		return ntUserID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setNtUserID(java.lang.String)
	 */
	@Override
	public void setNtUserID(String ntUserID) {
		this.ntUserID = ntUserID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getRacfID()
	 */
	@Override
	public String getRacfID() {
		return racfID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setRacfID(java.lang.String)
	 */
	@Override
	public void setRacfID(String racfID) {
		this.racfID = racfID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getSapReleaseCode()
	 */
	@Override
	public String getSapReleaseCode() {
		return sapReleaseCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setSapReleaseCode(java.lang.String)
	 */
	@Override
	public void setSapReleaseCode(String sapReleaseCode) {
		this.sapReleaseCode = sapReleaseCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getContractor()
	 */
	@Override
	public String getContractor() {
		return contractor;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setContractor(java.lang.String)
	 */
	@Override
	public void setContractor(String contractor) {
		this.contractor = contractor;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getCrisID()
	 */
	@Override
	public String getCrisID() {
		return crisID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setCrisID(java.lang.String)
	 */
	@Override
	public void setCrisID(String crisID) {
		this.crisID = crisID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getEvpOrgCode()
	 */
	@Override
	public String getEvpOrgCode() {
		return evpOrgCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setEvpOrgCode(java.lang.String)
	 */
	@Override
	public void setEvpOrgCode(String evpOrgCode) {
		this.evpOrgCode = evpOrgCode;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getManagementProfessional()
	 */
	@Override
	public String getManagementProfessional() {
		return managementProfessional;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setManagementProfessional(java.lang.String)
	 */
	@Override
	public void setManagementProfessional(String managementProfessional) {
		this.managementProfessional = managementProfessional;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getExternalToTELUS()
	 */
	@Override
	public String getExternalToTELUS() {
		return externalToTELUS;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setExternalToTELUS(java.lang.String)
	 */
	@Override
	public void setExternalToTELUS(String externalToTELUS) {
		this.externalToTELUS = externalToTELUS;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getNumericalPID()
	 */
	@Override
	public String getNumericalPID() {
		return numericalPID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setNumericalPID(int)
	 */
	@Override
	public void setNumericalPID(String numericalPID) {
		this.numericalPID = numericalPID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#getNumericalSAPID()
	 */
	@Override
	public String getNumericalSAPID() {
		return numericalSAPID;
	}


	/* (non-Javadoc)
	 * @see com.telus.framework.dbobject.ITeamMember#setNumericalSAPID(int)
	 */
	@Override
	public void setNumericalSAPID(String numericalSAPID) {
		this.numericalSAPID = numericalSAPID;
	}
		
	
}
