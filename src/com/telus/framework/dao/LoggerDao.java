package com.telus.framework.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.telus.framework.dbobject.ILog;

/**
 * LoggerDao Implementation Class
 * 
 * @author Kalun Ho (t855054)
 * 
 */
public class LoggerDao extends SqlMapClientDaoSupport implements ILoggerDao {

	public static String ADD_LOG = "Logging.addLog";
	public static String GET_LOG = "Logging.getLog";
	public static String GET_LOG_COUNT = "Logging.getLogCount";
	public static String GET_ALL_LOG_COUNT = "Logging.getLogCount";

	@Override
	public Integer insert(Map<String, Object> param)
			throws DuplicateKeyException, SQLException {
		return (Integer) super.getSqlMapClient().insert(ADD_LOG, param);
	}
	
	public List<ILog> getLog(int start, int length, String orderBy, String orderDir, String searchTerm) {
		
		Map params = new HashMap();
		params.put("start", start);
		params.put("length", length);
		params.put("orderBy", orderBy);
		params.put("orderDir", orderDir);
		params.put("searchTerm", searchTerm);

		return super.getSqlMapClientTemplate().queryForList(GET_LOG, params);
		
	}
	
	public Integer getLogCount(String searchTerm) {
		
		Map params = new HashMap();
		params.put("searchTerm", searchTerm);
		
		return (Integer) super.getSqlMapClientTemplate().queryForObject(GET_LOG_COUNT, params);
	}
	
	public Integer getAllLogCount() {
		
		return (Integer) super.getSqlMapClientTemplate().queryForObject(GET_ALL_LOG_COUNT, null);
	}

}
