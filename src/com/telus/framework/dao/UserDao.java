package com.telus.framework.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.ObjectNotFoundException;

import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.telus.framework.dbobject.IUser;
import com.telus.framework.dbobject.User;

/**
 * Implementation of UserDao
 * @author Kalun Ho (t855054)
 *
 */
public class UserDao extends SqlMapClientDaoSupport implements IUserDao {

	private static final String GET_ALL_USERS = "User.getAllUsers";
	private static final String GET_ALL_PERMISSIONS = "User.getAllPermissions";
	private static final String DELETE_USER = "User.deleteUser";
	private static final String ADD_USER = "User.addUser";
	private static final String ADD_USER_PERMISSION = "User.addUserPermission";
	private static final String UPDATE_USER_ENABLED = "User.updateUserEnabled";
	private static final String DELETE_USER_PERMISSION = "User.deleteUserPermission";
	
	@Override
	public List<IUser> getAllUsers() throws ObjectNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return super.getSqlMapClient().queryForList( GET_ALL_USERS,  "");
	}

	@Override
	public void addUser(IUser user) throws ConcurrencyFailureException, DuplicateKeyException, SQLException {
		super.getSqlMapClient().insert(ADD_USER, user);
		this.addUserPermissions(user);
	}

	@Override
	public void deleteUser(IUser user) throws ConcurrencyFailureException, SQLException {
		// TODO Auto-generated method stub
		super.getSqlMapClient().delete(DELETE_USER, user);
	}

	@Override
	public List<String> getAllPermissions() throws ObjectNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return super.getSqlMapClient().queryForList( GET_ALL_PERMISSIONS,  "");
	}

	@Override
	public void updateUserEnabled(IUser user) throws ConcurrencyFailureException, SQLException {
		// TODO Auto-generated method stub
		super.getSqlMapClient().update( UPDATE_USER_ENABLED, user);
	}

	@Override
	public void updateUserPermissions(IUser user) throws ConcurrencyFailureException, DuplicateKeyException, SQLException {
		try {
			super.getSqlMapClient().delete( DELETE_USER_PERMISSION, user);
		} catch (ConcurrencyFailureException e) {
			
		}
		this.addUserPermissions(user);
	}
	
	private void addUserPermissions(IUser user) throws ConcurrencyFailureException, DuplicateKeyException, SQLException {
		Map<String, String> map = new HashMap<String, String>();
		for (String permission: user.getPermissions()) {
			map.put("email", user.getEmail());
			map.put("permission", permission.toLowerCase().trim());
			super.getSqlMapClient().insert(ADD_USER_PERMISSION, map);
			map.clear();
		}
	}

}

