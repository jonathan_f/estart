package com.telus.framework.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.ejb.ObjectNotFoundException;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.telus.framework.dbobject.ITeamMember;
import com.telus.framework.dbobject.TeamMember;

/**
 * Implementation class for TeamMemberDao
 * @author Kalun Ho (t855054)
 *
 */
public class TeamMemberDao extends SqlMapClientDaoSupport implements ITeamMemberDao {

	private static final String AUTO_COMPLETE = "TeamMembers.autoComplete";
	private static final String GET_TEAM_MEMBER_BY_NUMERICAL_PID = "TeamMembers.getTeamMemberByNumericalPID";
	private static final String GET_TEAM_MEMBER_BY_EMAIL = "TeamMembers.getTeamMemberByEmail";
	
	public List<ITeamMember> autoComplete(String searchString) throws ObjectNotFoundException, SQLException {
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("searchString", searchString);
		return super.getSqlMapClient().queryForList(AUTO_COMPLETE, map);
		
	}


	@Override
	public ITeamMember getTeamMemberByNumericalPID(String numericalPID) throws ObjectNotFoundException, SQLException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("searchString", numericalPID);
		return (TeamMember) super.getSqlMapClient().queryForObject(GET_TEAM_MEMBER_BY_NUMERICAL_PID, map);
	}
	
	@Override
	public ITeamMember getTeamMemberByEmail(String email) throws ObjectNotFoundException, SQLException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("searchString", email);
		return (TeamMember) super.getSqlMapClient().queryForObject(GET_TEAM_MEMBER_BY_EMAIL, map);
	}

}
	
	
