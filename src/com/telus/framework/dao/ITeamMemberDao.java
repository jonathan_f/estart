package com.telus.framework.dao;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.ObjectNotFoundException;

import com.telus.framework.dbobject.ITeamMember;
import com.telus.framework.dbobject.TeamMember;

/**
 * Interface for Team Member Dao.  Use to query team members
 * @author Kalun Ho (t855054)
 *
 */
public interface ITeamMemberDao {
	
	/**
	 * Search for a certain number of team members with numerical PID matches searchString
	 * @param searchString
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws SQLException 
	 */
	public List<ITeamMember> autoComplete (String searchString) throws ObjectNotFoundException, SQLException;

	/**
	 * Get one particular user with numerical PID
	 * @param numericalPID
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws SQLException 
	 */
	public ITeamMember getTeamMemberByNumericalPID(String numericalPID) throws ObjectNotFoundException, SQLException;
	
	/**
	 * Get one particular user with Email
	 * @param numericalPID
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws SQLException 
	 */
	public ITeamMember getTeamMemberByEmail(String email) throws ObjectNotFoundException, SQLException;
	
}
