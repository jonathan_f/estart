package com.telus.framework.dao;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.ObjectNotFoundException;

import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.DuplicateKeyException;

import com.telus.framework.dbobject.IUser;
import com.telus.framework.dbobject.User;

/**
 * Interface for User Dao - Use for add, delete, and update application user permissions
 * @author Kalun Ho (t855054)
 *
 */
public interface IUserDao {

	/**
	 * Query all application users
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws SQLException 
	 */
	public List<IUser> getAllUsers() throws ObjectNotFoundException, SQLException;

	/**
	 * Query all permission levels 
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws SQLException 
	 */
	public List<String> getAllPermissions()  throws ObjectNotFoundException, SQLException;
	
	/**
	 * Add user to application
	 * @param user
	 * @throws ConcurrencyFailureException
	 * @throws DuplicateKeyException
	 * @throws SQLException 
	 */
	public void addUser(IUser user) throws ConcurrencyFailureException, DuplicateKeyException, SQLException;
	
	/**
	 * Delete user from application
	 * @param user
	 * @throws ConcurrencyFailureException
	 * @throws SQLException 
	 */
	public void deleteUser(IUser user) throws ConcurrencyFailureException, SQLException;
	
	/**
	 * Enable or Disable user
	 * @param user
	 * @throws ConcurrencyFailureException
	 * @throws SQLException 
	 */
	public void updateUserEnabled(IUser user) throws ConcurrencyFailureException, SQLException;

	/**
	 * Update user permissions
	 * @param user
	 * @throws ConcurrencyFailureException
	 * @throws DuplicateKeyException
	 * @throws SQLException 
	 */
	public void updateUserPermissions(IUser user) throws ConcurrencyFailureException, DuplicateKeyException, SQLException;
}
