package com.telus.framework.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;

import com.telus.framework.dbobject.ILog;

/**
 * Interface for LoggerDao - Use to insert log into file and database
 * @author Kalun Ho (t855054)
 *
 */
public interface ILoggerDao {
	
	/**
	 * Insert log messages into database
	 * @param param
	 * @return
	 * @throws DuplicateKeyException
	 * @throws SQLException 
	 */
	public Integer insert(Map<String, Object> param) throws DuplicateKeyException, SQLException;
	
	public List<ILog> getLog(int start, int length, String orderBy, String orderDir, String searchTerm);
	
	public Integer getLogCount(String searchTerm);	
	
	public Integer getAllLogCount();
	
}
